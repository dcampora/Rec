#ifndef MUONTIMECOR_H
#define MUONTIMECOR_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDAQ/IMuonRawBuffer.h"
#include "MuonInterfaces/IMuonFastHWTool.h"
#include "MuonInterfaces/IMuonTimeCor.h"            // Interface


/** @class MuonTimeCor MuonTimeCor.h
 *
 *
 *  @author Alessia Satta
 *  @date   2009-12-22
 */
class MuonTimeCor final : public GaudiTool, virtual public IMuonTimeCor {
public:
  /// Standard constructor
  MuonTimeCor( const std::string& type,
               const std::string& name,
               const IInterface* parent);

  StatusCode initialize() override;
  StatusCode getCorrection(LHCb::MuonTileID tile,int & cor) override;
  StatusCode getOutCorrection(LHCb::MuonTileID tile,int & cor) override;
  StatusCode setOutCorrection(LHCb::MuonTileID tile,int  cor) override;
  StatusCode writeOutCorrection() override;
  StatusCode writeCorrection() override;

private:
  int getIndex(LHCb::MuonTileID tile) const;
  int getLayer(LHCb::MuonTileID tile) const;
  virtual StatusCode setCorrection(LHCb::MuonTileID tile,int  cor);


  std::vector<std::string> m_correctionFiles;
  std::string m_correctionFileOut;
  std::vector<int> m_correctionSign;
  std::vector<int> m_correction[20][2];
  std::vector<int> m_Outcorrection[20][2];
  DeMuonDetector* m_muonDetector;
  IMuonRawBuffer* m_muonBuffer;
  IMuonFastHWTool* m_muonHWTool;


};
#endif // MUONTIMECOR_H
