// ============================================================================
// Include files
// ============================================================================
// STD & STL 
// ============================================================================
#include <algorithm>
#include <functional>
#include <memory>
// ============================================================================
#include "GaudiKernel/SystemOfUnits.h"
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "CaloUtils/CaloDataFunctor.h"
#include "Event/CellID.h"
#include "CaloInterfaces/ICaloClusterSelector.h"
#include "CaloInterfaces/ICaloHypoTool.h"
#include  "CaloUtils/CaloAlgUtils.h"
#include "CaloSinglePhotonAlg.h"
#include "CaloUtils/CaloMomentum.h"
#include "boost/lexical_cast.hpp"
// ============================================================================
/** @file
 * 
 *  Implementation file for class : CaloSinglePhotonAlg
 *  The implementation is based on F.Machefert's codes.
 *  @see CaloSinglePhotonAlg
 *
 *  @author Frederic Machefert machefer@in2p3.fr
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 31/03/2002 
 */
// ============================================================================
namespace {
    template <typename Container, typename Arg, typename Parent>
    bool apply(const Container& c, Arg& arg, const char* errmsg, const Parent& parent) {
        bool r = std::all_of( std::begin(c), std::end(c),
                            [&](typename Container::const_reference elem)
                            { return (*elem)(&arg).isSuccess(); } );
        if (UNLIKELY(!r)) parent.Error( errmsg, StatusCode::FAILURE ).ignore();
        return r;
    }
}
DECLARE_ALGORITHM_FACTORY( CaloSinglePhotonAlg )
// ============================================================================
/*  Standard constructor
 *  @param name algorithm name 
 *  @param pSvc service locator 
 */
// ============================================================================
CaloSinglePhotonAlg::CaloSinglePhotonAlg
( const std::string& name ,
  ISvcLocator*       pSvc )
  : GaudiAlgorithm ( name , pSvc )  
  //
  , m_eTcut ( 0. )
  //
  , m_inputData    ( LHCb::CaloClusterLocation::Ecal   ) 
  , m_outputData   ( LHCb::CaloHypoLocation::Photons   ) 
  , m_detData      ( DeCalorimeterLocation:: Ecal      )
  , m_det(0)
{
  m_selectorsTypeNames.push_back( "CaloSelectCluster/PhotonCluster"                   ) ;
  m_selectorsTypeNames.push_back( "CaloSelectNeutralClusterWithTracks/NeutralCluster" ) ;
  declareProperty( "SelectionTools"  , m_selectorsTypeNames    ,"List of tools for selection of clusters") ;
  declareProperty( "CorrectionTools" , m_correctionsTypeNames  ,"List of tools for primary corrections") ;  
  declareProperty ( "HypoTools" , m_hypotoolsTypeNames  , "List of generic Hypo-tools to apply to newly created hypos") ;

  m_hypotoolsTypeNames .push_back ( "CaloExtraDigits/SpdPrsExtraG" ) ;  
  m_correctionsTypeNames2.push_back ( "CaloECorrection/ECorrection" ) ;
  m_correctionsTypeNames2.push_back ( "CaloSCorrection/SCorrection" ) ;
  m_correctionsTypeNames2.push_back ( "CaloLCorrection/LCorrection" ) ;
  
  declareProperty ( "CorrectionTools2" , m_correctionsTypeNames2 , "List of tools for 'fine-corrections' " ) ;  
  declareProperty ( "HypoTools2" , m_hypotoolsTypeNames2 , "List of generic Hypo-tools to apply to corrected hypos") ;
  declareProperty ( "EtCut"   , m_eTcut , "Threshold on cluster & hypo ET" ) ;
  //
  declareProperty ( "InputData"         , m_inputData     ) ;  
  declareProperty ( "OutputData"        , m_outputData     ) ;  
  declareProperty ( "Detector"          , m_detData      ) ;  
  

 // Default context-dependent locations
  m_inputData = LHCb::CaloAlgUtils::CaloClusterLocation( "Ecal", context() );
  m_outputData= LHCb::CaloAlgUtils::CaloHypoLocation(  "Photons", context() );
  m_detData   = LHCb::CaloAlgUtils::DeCaloLocation("Ecal");

  setProperty ( "PropertiesPrint" , true ) ;
  
}
// ============================================================================
/*   standard Algorithm initialization
 *   @return status code 
 */
// ============================================================================
StatusCode CaloSinglePhotonAlg::initialize() 
{
  StatusCode sc = GaudiAlgorithm::initialize();
  if( sc.isFailure() ) 
  { return Error("Could not initialize the base class GaudiAlgorithm!",sc);}

  // check the geometry information 
  m_det = getDet<DeCalorimeter>( m_detData ) ; 
  if( 0 == m_det )return Error("Detector information is not available!");
  
  // locate selector tools
  std::transform( m_selectorsTypeNames.begin(), m_selectorsTypeNames.end(),
                  std::back_inserter(m_selectors),
                  [&](const std::string& name) { return this->tool<ICaloClusterSelector>( name, this ); } );
  if ( m_selectors.empty() )info() << "No Cluster Selection tools are specified!" << endmsg ;
  // locate correction tools
  std::transform( m_correctionsTypeNames.begin(), m_correctionsTypeNames.end(),
                  std::back_inserter(m_corrections),
                  [&](const std::string& name) { return this->tool<ICaloHypoTool>( name , this ); } );
  if ( m_corrections.empty() )info() << "No Hypo    Correction(1) tools are specified!" << endmsg ;
  // locate other hypo  tools
  std::transform( m_hypotoolsTypeNames.begin(), m_hypotoolsTypeNames.end(),
                  std::back_inserter(m_hypotools),
                  [&](const std::string& name) { return this->tool<ICaloHypoTool>( name, this ); } );
  if ( m_hypotools.empty() )info() << "No Hypo    Processing(1) tools are specified!" << endmsg ;
  // locate correction tools
  std::transform( m_correctionsTypeNames2.begin(), m_correctionsTypeNames2.end(),
                  std::back_inserter(m_corrections2),
                  [&](const std::string& name) { return this->tool<ICaloHypoTool>( name , this ); } );
  if ( m_corrections2.empty() )info() << "No Hypo    Correction(2) tools are specified!" << endmsg ;
  // locate other hypo  tools
  std::transform( m_hypotoolsTypeNames2.begin(), m_hypotoolsTypeNames2.end(),
                  std::back_inserter(m_hypotools2),
                  [&](const std::string& name) { return this->tool<ICaloHypoTool>(name,this); } );
  if ( m_hypotools2.empty() )info () << "No Hypo    Processing(2) tools are specified!" << endmsg ;
  ///
  counterStat = tool<ICounterLevel>("CounterLevel");
  return StatusCode::SUCCESS;
}
// ============================================================================
/*   standard Algorithm finalization
 *   @return status code 
 */
// ============================================================================
StatusCode 
CaloSinglePhotonAlg::finalize() 
{
  // clear containers
  m_selectors             .clear () ;
  m_corrections           .clear () ;
  m_hypotools             .clear () ;
  m_corrections2          .clear () ;
  m_hypotools2            .clear () ;
  m_selectorsTypeNames    .clear () ;
  m_correctionsTypeNames  .clear () ;
  m_hypotoolsTypeNames    .clear () ;
  m_correctionsTypeNames2 .clear () ;
  m_hypotoolsTypeNames2   .clear () ;
  // finalize the base class 
  return GaudiAlgorithm::finalize() ; 
}
// ============================================================================
/*   standard Algorithm execution
 *   @return status code 
 */
// ============================================================================
StatusCode 
CaloSinglePhotonAlg::execute(){
  using namespace  LHCb::CaloDataFunctor;
  // avoid long names 
  typedef LHCb::CaloClusters             Clusters ;
  typedef LHCb::CaloHypos                Hypos    ;
  typedef Clusters::iterator       iterator ;
  
  // get input clusters
  Clusters* clusters = get<Clusters>( m_inputData );
  
  // create and the output container of hypotheses and put in to ETS  
  Hypos*    hypos = new Hypos() ;
  put( hypos , m_outputData );
  
  LHCb::CaloDataFunctor::EnergyTransverse<const DeCalorimeter*> eT ( m_det ) ;
  
  // loop over clusters 
  for( iterator cluster = clusters->begin() ;clusters->end() != cluster ; ++cluster ){

    // loop over all selectors
    bool select = ( eT(*cluster)>=m_eTcut ) &&
                    std::all_of( m_selectors.begin(), m_selectors.end(),
                                 [&](const auto& selector)
                                 { return (*selector)(*cluster); } );
    // cluster to be selected?
    if ( !select ) { continue ; }
    
    // create "Hypo"/"Photon" object
    auto hypo = std::make_unique<LHCb::CaloHypo>();
    
    // set parameters of newly created hypo 
    hypo -> setHypothesis ( LHCb::CaloHypo::Photon );      
    hypo -> addToClusters ( *cluster );
    hypo -> setPosition   ( new LHCb::CaloPosition((*cluster)->position()) );    
    
    bool ok = apply( m_corrections, *hypo, "Error from Correction Tool, skip the cluster  " , *this) // loop over all corrections and apply corrections  
           && apply( m_hypotools, *hypo, "Error from Other Hypo Tool, skip the cluster  " , *this) // loop over other hypo tools (e.g. add extra digits)
           && apply( m_corrections2, *hypo, "Error from Correction Tool 2 skip the cluster" , *this) // loop over all corrections and apply corrections  
           && apply( m_hypotools2, *hypo,  "Error from Other Hypo Tool 2, skip the cluster" , *this ); // loop over other hypo tools (e.g. add extra digits)
    if (!ok) continue;        // CONTINUE

    // check momentum after all corrections :
    if( LHCb::CaloMomentum( hypo.get() ).pt() >= m_eTcut )
      hypos->insert ( hypo.release() ) ;  /// add the hypo into container of hypos 
    
  } // end of the loop over all clusters
  
  if(UNLIKELY(msgLevel(MSG::DEBUG)))debug() << " # of created Photon  Hypos is  " << hypos->size()  << "/" << clusters->size()<< endmsg ;
  if(counterStat->isQuiet())counter ( m_inputData + "=>" + m_outputData  ) += hypos->size() ;  
  return StatusCode::SUCCESS;
}




