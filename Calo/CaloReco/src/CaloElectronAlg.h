#ifndef CALORECO_CALOElectronALG_H 
#define CALORECO_CALOElectronALG_H 1
// Include files
// from STL
#include <string>
#include <memory>
// from GaudiAlg
#include "CaloDet/DeCalorimeter.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "CaloInterfaces/ICounterLevel.h"
// forward delcarations 
struct ICaloClusterSelector ;
struct ICaloHypoTool        ;

/** @class CaloElectronAlg CaloElectronAlg.h
 *  
 *  The simplest algorithm of reconstruction of 
 *  electrons in electromagnetic calorimeter.
 *
 *  @author Vanya Belyaev      Ivan.Belyaev@itep.ru
 *  @date   31/03/2002
 */
class CaloElectronAlg : public GaudiAlgorithm
{  
  /// friend factory for instantiation 
  friend class AlgFactory<CaloElectronAlg>; 
public:
  
  /// container of names
  typedef std::vector<std::string>           Names       ;
  /// container of selector tools 
  typedef std::vector<ICaloClusterSelector*> Selectors   ;
  /// containers of hypo tools 
  typedef std::vector<ICaloHypoTool*>        HypoTools   ;
  /// container of correction tools (S-,L-,...)
  typedef HypoTools                          Corrections ;
  
public:

  /**  standard Algorithm initialization
   *   @see CaloAlgorithm
   *   @see     Algorithm
   *   @see    IAlgorithm
   *   @return status code 
   */
  virtual StatusCode initialize ();  
  
  /**  standard Algorithm execution
   *   @see     Algorithm
   *   @see    IAlgorithm
   *   @return status code 
   */
  virtual StatusCode execute    ();  
  
  /**  standard Algorithm finalization
   *   @see CaloAlgorithm
   *   @see     Algorithm
   *   @see    IAlgorithm
   *   @return status code 
   */
  virtual StatusCode finalize   ();
  
protected:
  
  /** Standard constructor
   *  @param name algorithm name 
   *  @param pSvc service locator 
   */
  CaloElectronAlg
  ( const std::string&  name , 
    ISvcLocator*        pSvc );
  
private:
  
  // cluster selectors 
  Names        m_selectorsTypeNames    ;
  Selectors    m_selectors             ;
  
  // corrections
  Names        m_correctionsTypeNames  ;
  Corrections  m_corrections           ;
  
  // other hypo tools 
  Names        m_hypotoolsTypeNames    ;
  HypoTools    m_hypotools             ;

  // corrections
  Names        m_correctionsTypeNames2 ;
  Corrections  m_corrections2          ;
  
  // other hypo tools 
  Names        m_hypotoolsTypeNames2   ;
  HypoTools    m_hypotools2            ;

  std::string m_inputData ;
  std::string m_outputData;
  std::string m_detData   ;
  const DeCalorimeter*  m_det = nullptr;
  double m_eTcut = 0;
  ICounterLevel* counterStat = nullptr;
};

// ============================================================================
#endif // CALOElectronALG_H
