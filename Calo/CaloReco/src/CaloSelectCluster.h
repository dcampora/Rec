//
// ============================================================================
#ifndef CALORECO_CALOSELECTCLUSTER_H
#define CALORECO_CALOSELECTCLUSTER_H 1
// ============================================================================
#include <string>
#include "GaudiAlg/GaudiTool.h"
#include "CaloInterfaces/ICaloClusterSelector.h"
#include "CaloInterfaces/ICounterLevel.h"
// ============================================================================

class CaloSelectCluster :
  public virtual ICaloClusterSelector ,
  public          GaudiTool
{
  /// friend factory for instantiation
  friend struct ToolFactory<CaloSelectCluster>;

public:

  bool select( const LHCb::CaloCluster* cluster ) const override;
  virtual bool operator()( const LHCb::CaloCluster* cluster ) const override;
  StatusCode initialize () override;


  CaloSelectCluster() = delete;
  CaloSelectCluster(const CaloSelectCluster& ) = delete;
  CaloSelectCluster& operator=(const CaloSelectCluster& ) = delete;

protected:

  CaloSelectCluster( const std::string& type   ,
                     const std::string& name   ,
                     const IInterface*  parent );

private:
  double m_cut;
  double m_etCut;
  int m_mult;
  int m_multMin;
  ICounterLevel* counterStat;
};
#endif // CALORECO_CALOSELECTCLUSTER_H
