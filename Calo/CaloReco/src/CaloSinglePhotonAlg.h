// ============================================================================
#ifndef CALOALGS_CALOSINGLEPHOTONALG_H 
#define CALOALGS_CALOSINGLEPHOTONALG_H 1
// ============================================================================
#include <string>
#include "GaudiAlg/GaudiAlgorithm.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloInterfaces/ICounterLevel.h"
struct ICaloClusterSelector ;
struct ICaloHypoTool        ;

/** @class CaloSinglePhotonAlg CaloSinglePhotonAlg.h
 *  
 *  The simplest algorithm of reconstruction of 
 *  single photon in electromagnetic calorimeter.
 *  The implementation is based on F.Machefert's codes.
 *
 *  @author Frederic Machefert machefer@in2p3.fr
 *  @author Vanya Belyaev      Ivan.Belyaev@itep.ru
 *  @date   31/03/2002
 */
class CaloSinglePhotonAlg : 
  public GaudiAlgorithm
{  
  /// friend factory for instantiation 
  friend class AlgFactory<CaloSinglePhotonAlg>; 
public:
  
  /// container of names
  typedef std::vector<std::string>           Names       ;
  /// container of selector tools 
  typedef std::vector<ICaloClusterSelector*> Selectors   ;
  /// containers of hypo tools 
  typedef std::vector<ICaloHypoTool*>        HypoTools   ;
  /// container of correction tools (S-,L-,...)
  typedef HypoTools                          Corrections ;
  
public:

  virtual StatusCode initialize ();  
  virtual StatusCode execute    ();  
  virtual StatusCode finalize   ();
  
protected:
  
  CaloSinglePhotonAlg( const std::string&  name , ISvcLocator*        pSvc );  
  
private:
  
  double       m_eTcut                     ;

  // cluster selectors 
  Names        m_selectorsTypeNames        ;
  Selectors    m_selectors                 ;
  
  // corrections
  Names        m_correctionsTypeNames       ;
  Corrections  m_corrections                ;
  
  // other hypo tools 
  Names        m_hypotoolsTypeNames         ;
  HypoTools    m_hypotools                  ;

  // corrections
  Names        m_correctionsTypeNames2      ;
  Corrections  m_corrections2               ;
  
  // other hypo tools 
  Names        m_hypotoolsTypeNames2        ;
  HypoTools    m_hypotools2                 ;

  std::string m_inputData ;
  std::string m_outputData;
  std::string m_detData   ;
  const DeCalorimeter*  m_det;
  ICounterLevel* counterStat;
};
// ============================================================================
#endif // CALOSINGLEPHOTONALG_H
