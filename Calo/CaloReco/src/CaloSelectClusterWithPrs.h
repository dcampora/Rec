// ============================================================================
#ifndef CALORECO_CALOSELECTCLUSTERWITHPRS_H
#define CALORECO_CALOSELECTCLUSTERWITHPRS_H 1
// ============================================================================
#include <string>
#include "GaudiAlg/GaudiTool.h"
#include "CaloInterfaces/ICaloClusterSelector.h"
#include "CaloInterfaces/ICaloHypo2Calo.h"
#include "CaloInterfaces/ICounterLevel.h"
// ============================================================================

class CaloSelectClusterWithPrs : 
  public virtual ICaloClusterSelector ,
  public          GaudiTool
{
  /// friend factory for instantiation
  friend struct ToolFactory<CaloSelectClusterWithPrs>;
public:
  
  virtual bool select( const LHCb::CaloCluster* cluster ) const ;  
  virtual bool operator()( const LHCb::CaloCluster* cluster ) const ;
  virtual StatusCode initialize () ;
  
protected:
  
  CaloSelectClusterWithPrs( const std::string& type   , 
                            const std::string& name   ,
                            const IInterface*  parent );  

private:
  double m_cut;
  double m_mult;
  ICaloHypo2Calo* m_toPrs;
  std::string m_det;
  ICounterLevel* counterStat;
};
#endif // CALORECO_CALOSELECTCLUSTERWITHPRS_H
