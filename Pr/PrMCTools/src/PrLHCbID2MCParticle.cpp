// Include files 

#include "Event/MCParticle.h"

// includes from DigiEvent
#include "Event/VeloCluster.h"
#include "Event/VeloLiteCluster.h"
#include "Event/VPCluster.h"
#include "Event/STCluster.h"
#include "Event/OTTime.h"
#include "Event/FTCluster.h"
#include "Event/FTLiteCluster.h"

// local
#include "PrLHCbID2MCParticle.h"

namespace {
  static const std::vector<std::string> s_stClusterNames = { LHCb::STClusterLocation::TTClusters,
                                                             LHCb::STClusterLocation::ITClusters,
                                                             LHCb::STClusterLocation::UTClusters };
  static const std::vector<std::string> s_stLiteClusterNames = { LHCb::STLiteClusterLocation::TTClusters,
                                                                 LHCb::STLiteClusterLocation::ITClusters,
                                                                 LHCb::STLiteClusterLocation::UTClusters };
}
//-----------------------------------------------------------------------------
// Implementation file for class : PrLHCbID2MCParticle
//
// 2010-03-22 Victor Coco
//-----------------------------------------------------------------------------

DECLARE_ALGORITHM_FACTORY( PrLHCbID2MCParticle )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
  PrLHCbID2MCParticle::PrLHCbID2MCParticle( const std::string& name,
                                              ISvcLocator* pSvcLocator)
    : GaudiAlgorithm ( name , pSvcLocator ),
      m_otHitCreator("Tf::OTHitCreator/OTHitCreator")
{
  declareProperty( "TargetName",  m_targetName = "Pr/LHCbID" );
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode PrLHCbID2MCParticle::execute() {

  if (msgLevel(MSG::DEBUG)) debug() << "==> Execute" << endmsg;

  LinkerWithKey<LHCb::MCParticle> lhcbLink( evtSvc(), msgSvc(), m_targetName );

  //== Velo

  const auto* v_clusters = getIfExists<LHCb::VeloClusters>(LHCb::VeloClusterLocation::Default);
  if ( v_clusters ) {
    LinkedTo<LHCb::MCParticle> veloLink( evtSvc(), msgSvc(), LHCb::VeloClusterLocation::Default );
    for(const auto& clus : *v_clusters) {
      LHCb::LHCbID myId = LHCb::LHCbID( clus->channelID() );
      linkAll( veloLink, lhcbLink, myId, myId.veloID(), clus->pseudoSize() );
    }
  } else {
    const auto* vl_clusters = getIfExists<LHCb::VeloLiteCluster::VeloLiteClusters>(LHCb::VeloLiteClusterLocation::Default);
    if ( vl_clusters ) {
      LinkedTo<LHCb::MCParticle> veloLink( evtSvc(), msgSvc(), LHCb::VeloClusterLocation::Default );
      for(const auto& clus : *vl_clusters) {
        LHCb::LHCbID myId = LHCb::LHCbID( clus.channelID() );
        linkAll( veloLink, lhcbLink, myId, myId.veloID(), clus.pseudoSize() );
      }
    }
  }

  //== VP

  const auto* vp_clusters = getIfExists<LHCb::VPClusters>(LHCb::VPClusterLocation::Default);
  if ( vp_clusters ) {
    LinkedTo<LHCb::MCParticle> vpLink(evtSvc(), msgSvc(), LHCb::VPClusterLocation::Default);
    for (const auto& clus : *vp_clusters) {
      LHCb::LHCbID myId = clus->channelID();
      linkAll( vpLink, lhcbLink, myId, myId.vpID());
    }
  }

  //== TT, IT, UT (All ST-like detectors)

  for ( unsigned int kk = 0; s_stClusterNames.size() > kk; ++kk ) {
    std::string clusterName = s_stClusterNames[kk];
    std::string liteName    = s_stLiteClusterNames[kk];
    const auto* cont = getIfExists<LHCb::STCluster::Container>( clusterName );
    if ( cont ) {
      LinkedTo<LHCb::MCParticle> ttLink( evtSvc(), msgSvc(), clusterName );
      for(  const auto& clus : *cont ) {
        LHCb::LHCbID myId = LHCb::LHCbID(clus->channelID());
        linkAll( ttLink, lhcbLink,  myId, myId.stID(), clus->size() );
      }
    } else {
      //FIXME: this does not work in the future branch!!!!
      const auto* clusters = getIfExists<LHCb::STLiteCluster::STLiteClusters>( liteName );
      if ( clusters ) {
        LinkedTo<LHCb::MCParticle> ttLink( evtSvc(), msgSvc(), clusterName );
        for(const auto& clus : *clusters) {
          LHCb::LHCbID myId = LHCb::LHCbID( clus.channelID() );
          linkAll( ttLink, lhcbLink,  myId, myId.stID(), clus.pseudoSize() );
        }
      }
    }
  }

  //== OT coordinates
  if ( exist<LHCb::LinksByKey>( "Link/" + LHCb::OTTimeLocation::Default ) ) {
    LinkedTo<LHCb::MCParticle> otLink( evtSvc(), msgSvc(),LHCb::OTTimeLocation::Default );
    if ( !otLink.notFound() ) {
      if ( !m_otReady ) {
        m_otHitCreator.retrieve().ignore();
        m_otReady = true;
      }
      for (const auto& hit: m_otHitCreator->hits() ) {
        LHCb::LHCbID myId = hit->lhcbID();
        linkAll( otLink, lhcbLink,  myId, myId.otID() );
      }
    }
  }
  
  //== FT
  const auto* ft_clusters = getIfExists<FastClusterContainer<LHCb::FTLiteCluster,int>>( LHCb::FTLiteClusterLocation::Default );
  if ( ft_clusters ) {
    LinkedTo<LHCb::MCParticle> ftLink( evtSvc(), msgSvc(),LHCb::FTClusterLocation::Default );
    if ( !ftLink.notFound() ) {
      for(const auto& cluster : *ft_clusters) {
        LHCb::LHCbID myId = LHCb::LHCbID( cluster.channelID() );
        linkAll( ftLink, lhcbLink, myId, myId.ftID() );
      }
    }
  }
  return StatusCode::SUCCESS;
}

//=========================================================================
//  link all particles to the specified id
//=========================================================================
void PrLHCbID2MCParticle::linkAll( LinkedTo<LHCb::MCParticle>& ilink, LinkerWithKey<LHCb::MCParticle>& olink, 
                                   LHCb::LHCbID myId, int firstId, int size ) {
  std::vector<const LHCb::MCParticle*> partList; partList.reserve(size);
  while( size > 0 ) {
    for (const LHCb::MCParticle* part = ilink.first( firstId );
         part!=nullptr; part = ilink.next()) {
      partList.push_back( part );
    }
    ++firstId;
    --size;
  }
  // remove any possible duplicates
  std::sort(partList.begin(), partList.end());
  partList.erase( std::unique(partList.begin(),partList.end()),
                    partList.end() );
  if ( msgLevel(MSG::DEBUG) ) info() << format( "For ID %8x  MC: ", myId.lhcbID() );
  for ( const auto& part : partList ) {
    if ( msgLevel(MSG::DEBUG) ) info() << part->key() << " ";
    olink.link( myId.lhcbID(), part );
  }
  if ( msgLevel(MSG::DEBUG) ) info() << endmsg;
}
//=============================================================================
