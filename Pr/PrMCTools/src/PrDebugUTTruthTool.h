// $Id: PrDebugUTTruthTool.h,v 1.4 2008-12-04 09:05:07 cattanem Exp $
#ifndef PRDEBUGTTTRUTHTOOL_H 
#define PRDEBUGTTTRUTHTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTupleTool.h"
#include "PrKernel/IPrDebugUTTool.h"            // Interface
#include <TfKernel/TTStationHitManager.h>
#include "MCInterfaces/IIdealStateCreator.h"
class DeSTDetector;

/** @class PrDebugUTTruthTool PrDebugUTTruthTool.h
 *  
 *  Class to extract MC information for Upgrade Tracking.
 *  Updated to have information for PrLongLivedTracking.
 *  @author Olivier Callot
 *  @date   2007-10-22
 *
 *  @author Adam Davis
 *  @date   2016-04-10
 */
class PrDebugUTTruthTool : public GaudiTupleTool, virtual public IPrDebugUTTool {
public: 
  /// Standard constructor
  PrDebugUTTruthTool( const std::string& type, 
                       const std::string& name,
                       const IInterface* parent);

  virtual ~PrDebugUTTruthTool( ); ///< Destructor

  virtual StatusCode initialize(); /// initialize

  virtual void debugUTClusterOnTrack( const LHCb::Track* track, 
                                      const PrUTHits::const_iterator beginCoord,
                                      const PrUTHits::const_iterator endCoord   );
  
  virtual void debugUTCluster( MsgStream& msg, const PrUTHit* hit );

  virtual bool isTrueHit( const LHCb::Track* track, const PrUTHit* hit);

  virtual double fracGoodHits( const LHCb::Track* track, const PrUTHits& hits);
  
  virtual bool isTrueTrack( const LHCb::Track* track, const PrUTHits& hits);

  virtual bool isTrueTrack( const LHCb::Track* track, const Tf::UTStationHitManager<PrUTHit>::HitRange& hits);//AD, overload
  
  virtual void chi2Tuple( const double p, const double chi2, const unsigned int nHits);
  
  //added by AD 2/1/16 for efficiency vs step

  virtual void initializeSteps(std::vector<std::string> steps);//initialize all steps in the process  

  virtual void recordStepInProcess(std::string step, bool result);//record the result of a step in the process
  
  virtual void resetflags();//reset all flags

  virtual void forceMCHits(PrUTHits& hits, LHCb::Track* track);//Special. Force only MC matched hits in the track.
  
  virtual void tuneFisher( const LHCb::Track* seedTrack);
  
  virtual void tuneDeltaP( const LHCb::Track* seedTrack, const double deltaP, const double momentum);
  
  virtual void tuneFinalMVA( const LHCb::Track* seedTrack, const bool goodTrack, std::vector<double> vals);
  

  virtual void getMagnetError( const LHCb::Track* seedTrack);
  

protected:

private:
  
  DeSTDetector* m_tracker;
  std::map<std::string,bool> m_flags;
  /// The ideal state creator, which makes a state out of an MCParticle
  IIdealStateCreator* m_idealStateCreator;

};
#endif // PRDEBUGUTTRUTHTOOL_H
