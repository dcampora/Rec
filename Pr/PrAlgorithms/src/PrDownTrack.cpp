// $Id: PrDownTrack.cpp,v 1.5 2016-04-10 12:09:30 adavis Exp $
// Include files 

// local
#include "PrDownTrack.h"
#include "Event/StateParameters.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PrDownTrack, from Pat/PatKShort package
//
// 2007-10-18 : Olivier Callot
// 2016-04-10 : Adam Davis
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrDownTrack::PrDownTrack( LHCb::Track* tr, 
                          double zUT,
                          const std::array<double,7>& magnetParams,
                          const std::array<double,3>& momentumParams,
                          const std::vector<double>& yParams,
                          double magnetScale) :
  m_momPar(&momentumParams),
  m_track(tr),
  m_magnetScale(magnetScale),
  m_zUT(zUT),
  m_ignore(false),
  m_firedLayers(0)
{
  m_hits.reserve(6);
  
  // -- Note: You want the state the furthest away from the magnet, as then the straight-line approximation is the best
  m_state = &tr->closestState( 10000. );

  // -- See PrFitKsParams to see how these coefficients are derived. 
  double zMagnet = 
    magnetParams[0] + 
    magnetParams[1] * m_state->ty() * m_state->ty() +
    magnetParams[2] * m_state->tx() * m_state->tx() + 
    magnetParams[3] / m_state->p() + ///this is where the old one stopped.
    magnetParams[4] * std::abs( m_state->x() ) + 
    magnetParams[5] * std::abs( m_state->y() ) +
    magnetParams[6] * std::abs( m_state->ty() );
  
  const double dz      = zMagnet - m_state->z();
  double xMagnet = m_state->x() + dz * m_state->tx();
  m_slopeX       = xMagnet / zMagnet;
  const double dSlope = std::abs( m_slopeX - m_state->tx() );
  const double dSlope2 = dSlope*dSlope;

  double by = m_state->y() / ( m_state->z() + 
                               ( yParams[0] * fabs(m_state->ty()) * zMagnet + yParams[1] )* dSlope2  );
  m_slopeY = by * ( 1. + yParams[0] * fabs(by) * dSlope2 );
 
  const double yMagnet = m_state->y() + dz * by - yParams[1] * by * dSlope2;
  
  // -- These resolutions are semi-empirical and are obtained by fitting residuals
  // -- with MCHits and reconstructed tracks
  // -- See Tracking &Alignment meeting, 19.2.2015, for the idea
  m_errXMag = dSlope2 * 15.0 + dSlope * 15.0 + 3.0;
  m_errYMag = dSlope2 * 80.0 + dSlope * 10.0 + 4.0;
  
  // -- Assume better resolution for SciFi than for OT
  // -- obviously this should be properly tuned...
  m_errXMag /= 2.0;
  m_errYMag /= 1.5;

  //m_errXMag = 0.5  + 5.3*dSlope + 6.7*dSlope2;
  //m_errYMag = 0.37 + 0.7*dSlope - 4.0*dSlope2 + 11*dSlope2*dSlope;
  m_magnet = Gaudi::XYZPoint( xMagnet, yMagnet, zMagnet );
  
  //=== Save for reference
  m_magnetSave = m_magnet;
  m_slopeXSave = m_slopeX;
  m_displX     = 0.;
  m_displY     = 0.;

  //=== Initialize all other data members
  m_chisq      = 0.;
  m_slopeXCand = m_slopeX;

  m_curvature  = 1.7e-5 * ( m_state->tx() - m_slopeX );
}
//=============================================================================
