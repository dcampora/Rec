#ifndef IPRFORWARDTOOL_H 
#define IPRFORWARDTOOL_H 1

// Include files
// from STL
#include <string>
#include "GaudiAlg/GaudiTool.h"
#include "Event/Track.h"
#include "PrKernel/IPrDebugTool.h"
#include "PrForwardTrack.h"

static const InterfaceID IID_IPrForwardTool ( "IPrForwardTool", 1, 0 );

/** @class IPrForwardTool IPrForwardTool.h
 *  Interface for the various implementations of the Forward tool
 *
 *  @author Olivier Callot
 *  @date   2012-07-13
 */
class IPrForwardTool : public GaudiTool {
public:

  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_IPrForwardTool; }

  IPrForwardTool( const std::string& type,
                  const std::string& name,
                  const IInterface* parent) : GaudiTool( type, name, parent ), m_debugTool(nullptr) {};


  virtual void extendTrack( LHCb::Track* velo, LHCb::Tracks* result ) = 0;

  void setDebugParams( IPrDebugTool* tool, int key, int veloKey ) {
    m_debugTool = tool;
    m_wantedKey = key;
    m_veloKey   = veloKey;
  }

  bool matchKey( const PrHit& hit ) const {
    if ( m_debugTool ) return m_debugTool->matchKey( hit.id(), m_wantedKey );
    return false;
  };

  void printHit ( const PrHit& hit, const std::string title = "" ) const {
    info() << "  " << title
           << format( "Plane%3d zone%2d z0 %8.2f x0 %8.2f  coord %8.2f used%2d ",
                      hit.planeCode(), hit.zone(), hit.z(), hit.x(), hit.coord(), hit.isUsed() );
    if ( m_debugTool ) m_debugTool->printKey( info(), hit.id() );
    if ( matchKey( hit ) ) info() << " ***";
    info() << endmsg;
  }

  //=========================================================================
  //  Print the content of a track
  //=========================================================================
  void printTrack( const PrForwardTrack& track ) const {
    if ( 0 < track.nDoF() ) {
      
      info() << format( "   Track nDoF %3d   chi2/nDoF %7.3f quality %6.3f",
                        track.getChi2nDof().get(1), track.chi2PerDoF(), track.quality() ) 
             << endmsg;
    } else {
      info() << "   Track" << endmsg;
    }

    for( const auto& hit : track.hits()){
      info() << format( "dist %7.3f dy %7.2f chi2 %7.2f ", track.distance( *hit ), track.distance( *hit )/hit->dxDy(), track.chi2( *hit ) );
      printHit( *hit );
    }
  }
  //=========================================================================
  //  Print the hits for a given track
  //=========================================================================
  void printHitsForTrack( const PrHits& hits, const PrForwardTrack& track ) const {
    info() << "=== Hits to Track ===" << endmsg;
    for( const auto& hit : hits){
      info() << format( "dist %7.3f dy %7.2f chi2 %7.2f ", track.distance( *hit ), track.distance( *hit )/hit->dxDy(), track.chi2( *hit ) );
      printHit( *hit );
    }
  }
protected:
  IPrDebugTool*   m_debugTool;
  int             m_wantedKey;
  int             m_veloKey;
private:
};
#endif // IPRFORWARDTOOL_H
