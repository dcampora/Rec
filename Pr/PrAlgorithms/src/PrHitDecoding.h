#ifndef PRHITDECODING_H 
#define PRHITDECODING_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "PrKernel/PrHitManager.h"

/** @class PrHitDecoding PrHitDecoding.h
 *
 *  \brief Creates PrHits
 *
 *   Parameters:
 * - HitManagerName: Name of the hit manager
 *
 *  @author Thomas Nikodem
 *  @date   2016-05-23
 */


class PrHitDecoding : public GaudiAlgorithm {
public: 
  /// Standard constructor
  PrHitDecoding( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~PrHitDecoding( ); ///< Destructor

  virtual StatusCode initialize();    ///< Algorithm initialization
  virtual StatusCode execute   ();    ///< Algorithm execution

protected:

private:
  
  std::string    m_hitManagerName;
  PrHitManager*  m_hitManager;

};
#endif // PRHITDECODING_H
