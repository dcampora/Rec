// $Id: PatDownTrack.h,v 1.4 2016-04-10 11:45:03 adavis Exp $
#ifndef PRDOWNTRACK_H 
#define PRDOWNTRACK_H 1

// Include files
#include "GaudiKernel/Point3DTypes.h"

#include "Event/Track.h"
#include "Event/State.h"

#include "PrKernel/PrUTHit.h"
#include "TfKernel/RecoFuncs.h"

/** @class PrDownTrack PrDownTrack.h
 *  Track helper for Downstream tarck search
 *  Adapted from Pat/PatKShort package
 *  Further adapted for use with PrLongLivedTracking
 *
 *  @author Olivier Callot
 *  @date   2007-10-18
 *  
 *  @author Adam Davis
 *  @date   2016-04-10
 */

class PrDownTrack {
public:
  /// Standard constructor
  PrDownTrack( LHCb::Track* tr, 
                double zUT,
                const std::array<double,7>& magnetParams,
                const std::array<double,3>& momentumParams,
                const std::vector<double>& yParams,
                double magnetScale ); 
  /// Copy constructor
  PrDownTrack(const PrDownTrack& ) =  default;
  /// Move Constructor
  PrDownTrack(PrDownTrack&& ) = default;
  /// Copy assignment operator
  PrDownTrack &operator=(const PrDownTrack& ) = default;
  /// Move assignment operator
  PrDownTrack &operator=(PrDownTrack&& ) = default;
  
  virtual ~PrDownTrack( ) {} ///< Destructor

  /// getters
  LHCb::Track*      track()       const { return m_track;      }
  LHCb::State*      state()       const { return m_state;      }
  PrUTHits&         hits()              { return m_hits;       }
  const PrUTHits&   hits()        const { return m_hits;       }   
  double            xMagnet()     const { return m_magnet.x(); }
  double            yMagnet()     const { return m_magnet.y(); }
  double            zMagnet()     const { return m_magnet.z(); }
  double            slopeX()      const { return m_slopeX;     }
  double            slopeY()      const { return m_slopeY;     }
  double            errXMag()     const { return m_errXMag;    }
  double            errYMag()     const { return m_errYMag;    }
  double            displX()      const { return m_displX;     }
  double            displY()      const { return m_displY;     }
  double            chisq()       const { return m_chisq;      }
  double            chi2()        const { return m_chisq;      }
  bool              ignore()      const { return m_ignore;     }
  int               firedLayers() const { return m_firedLayers;}
  double            mvaVal()      const { return m_mvaVal;     }
  
    
    
  
  /// setters
  void setFiredLayers( const int nF ){
    m_firedLayers = nF;
  }
  
  void setIgnore( const bool ignore ){
    m_ignore = ignore;
  }
  
  void setCurvature( const double curvature ){
    m_curvature = curvature;
  }
  
  void setSlopeX( double slope ){ 
    m_slopeX = slope;
    m_slopeXCand = slope;
  }
  
  void setDisplY( double displY )       { m_displY = displY; }
  void setDisplX( double displX )       { m_displX = displX; }
  void setChisq( double chisq )         { m_chisq = chisq; }
  void setChi2( double chi2  )          { m_chisq = chi2; }
  void setMVAVal( double mvaVal )       { m_mvaVal = mvaVal; }
  
    
  //functions
  inline double xAtZ( double z ) const {
    return m_magnet.x() + (z-m_magnet.z() ) * m_slopeX + m_curvature * ( z-m_zUT) * (z-m_zUT);
  }
  inline double xAtZ( double z, double slopeX ) const {
    return m_magnet.x() + (z-m_magnet.z() ) * slopeX;
  }
  inline double yAtZ( double z ) const {
    return m_magnet.y() + m_displY + (z-m_magnet.z() ) * slopeY();
  }
  inline void updateX( double dx, double dsl )  {
    m_displX += dx;
    m_magnet  = Gaudi::XYZPoint( m_magnet.x() + dx, m_magnet.y(), m_magnet.z() );
    m_slopeX += dsl;
  }

  inline double dxMagnet() const { return m_magnetSave.x() - m_magnet.x(); }

  inline double initialChisq() const {
    return m_displX * m_displX / ( m_errXMag * m_errXMag ) + 
           m_displY * m_displY / ( m_errYMag * m_errYMag );
  }
  inline double initialChi2() const{ return initialChisq(); }
  
  inline double sagitta( double z ) const { return m_curvature * ( z-m_zUT) * (z-m_zUT); }

  inline double moment()  const { 
    return ( (*m_momPar)[0] +    
             (*m_momPar)[1] * m_state->tx() * m_state->tx() +
             (*m_momPar)[2] * m_state->ty() * m_state->ty() ) / 
      ( m_state->tx() - m_slopeX ) * m_magnetScale;
  }
  inline double momentum() const { return moment();  }//redef for prlonglivedtracking

  inline double pt() const{
    const double tx2 = slopeX()*slopeX();
    const double ty2 = slopeY()*slopeY();
    const double sinTrack = sqrt( 1. - 1./(1.+tx2 + ty2) );
    return sinTrack*std::abs(momentum());
  }
  
  inline double distance( const PrUTHit* hit ) const {
    return hit->x() - xAtZ( hit->z() );
  }

  void startNewCandidate() {
    m_hits.clear();
    m_magnet = m_magnetSave;
    m_slopeX = m_slopeXSave;
    m_displY  = 0.;
    m_displX  = 0.;
  }
  
  void startNewXCandidate(PrUTHit* firstHit) {
    m_hits.clear();
    m_hits.push_back(firstHit);
    m_magnet = m_magnetSave;
    m_slopeX = m_slopeXCand;
    m_displY  = 0.;
    m_displX  = 0.;
  }
  
  void startNewXCandidate() {
    m_hits.clear();
    m_magnet = m_magnetSave;
    m_slopeX = m_slopeXCand;
    m_displY  = 0.;
    m_displX  = 0.;
  }

  void startNewXUCandidate(const double slopeX, const double displX, const double magnetX) {
    m_magnet  = Gaudi::XYZPoint( magnetX, m_magnet.y(), m_magnet.z() );
    m_slopeX = slopeX;
    m_displY  = 0.;
    m_displX  = displX;
  }

  void sortFinalHits() {
    std::sort( m_hits.begin(), m_hits.end(), Tf::increasingByZ<PrUTHit>() );
  }
  
 
protected:

private:
  const std::array<double,3>* m_momPar;
  //const std::vector<double>* m_magPar;

  LHCb::Track*        m_track;
  LHCb::State*        m_state;
  Gaudi::XYZPoint     m_magnet;
  Gaudi::XYZPoint     m_magnetSave;
  
  double      m_magnetScale;
  double      m_zUT;
  double      m_slopeX;
  double      m_slopeXSave;
  double      m_slopeXCand;
  double      m_slopeY;
  double      m_displX;
  double      m_displY;
  double      m_errXMag;
  double      m_errYMag;
  double      m_chisq;
  double      m_curvature;
  bool        m_ignore;
  int         m_firedLayers;
  double      m_mvaVal;
  
  PrUTHits m_hits;      /// working list of hits on this track
};
// -- A typedef for a collection of downstream tracks... From PatDownTrack
typedef std::vector<PrDownTrack> PrDownTracks;

#endif // PRDOWNTRACK_H
