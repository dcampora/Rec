
#ifndef RICHHPDIMAGEANALYSIS_FullImageFit_H
#define RICHHPDIMAGEANALYSIS_FullImageFit_H 1

// local
#include "RichHPDImageAnalysis/HPDPixel.h"

// ROOT
#include "Minuit2/FCNBase.h"
#include "TH2D.h"

namespace Rich
{
  namespace HPDImage
  {

    /** @class FullImageFit RichHPDImageAnalysis/FullImageFit.h
     *
     *  Fit to the full histogram image.
     *
     *  @author Chris Jones
     *  @date   2011-03-05
     */
    class FullImageFit : public ROOT::Minuit2::FCNBase
    {

    public:

      /// Default Constructor
      FullImageFit() { }

      /// Constructor
      FullImageFit( const TH2* hist );

    public:

      virtual double operator()( const std::vector<double>& par ) const final;

      virtual double Up() const noexcept {  return m_errDef; }
      void setErrDef( const double def ) { m_errDef = def; }

    private:

      double m_errDef{0.0} ;
      const TH2* m_hist = nullptr;
      double m_sf{0} ;

    };

  }
}

#endif // RICHHPDIMAGEANALYSIS_FullImageFit_H
