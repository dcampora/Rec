// $Id: $
#ifndef RICHHPDIMAGEANALYSIS_SIMPLECHI2FIT_H
#define RICHHPDIMAGEANALYSIS_SIMPLECHI2FIT_H 1

// local
#include "RichHPDImageAnalysis/HPDPixel.h"

// ROOT
#include "Minuit2/FCNBase.h"
#include "TH2D.h"

namespace Rich
{
  namespace HPDImage
  {

    /** @class SimpleChi2Fit RichHPDImageAnalysis/SimpleChi2Fit.h
     *
     *  Simple Chi^2 fit
     *
     *  @author Chris Jones
     *  @date   2011-03-05
     */
    class SimpleChi2Fit final : public ROOT::Minuit2::FCNBase
    {

    public:

      /// Default Constructor
      SimpleChi2Fit() = default;

      /// Constructor
      SimpleChi2Fit( const TH2* hist,
                     const Pixel::List& list );

    public:

      virtual double operator()( const std::vector<double>& par ) const ;

      virtual double Up() const noexcept { return m_errDef; }

      void setErrDef( const double def ) { m_errDef = def; }

    private:

      double m_errDef{0} ;
      double m_sf{0} ;
      const Pixel::List * m_boundary = nullptr;

    };

  }
}

#endif // RICHHPDIMAGEANALYSIS_SIMPLECHI2FIT_H
