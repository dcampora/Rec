
#ifndef RICHHPDIMAGEANALYSIS_CLEANHPDIMAGE_H
#define RICHHPDIMAGEANALYSIS_CLEANHPDIMAGE_H 1

// STL
#include <list>
#include <memory>

// ROOT
#include "TH2D.h"

namespace Rich
{
  namespace HPDImage
  {

    /** @class Clean RichHPDImageAnalysis/CleanHPDImage.h
     *
     *  Applies some cleaning to HPD images.
     *  
     *  @author Chris Jones
     *  @date   2011-03-07
     */
    class Clean final
    {

    public:

      /** @class Params RichHPDImageAnalysis/CleanHPDImage.h
       *
       *  Parameters for HPD Cleaner
       *  
       *  @author Chris Jones
       *  @date   2011-03-07
       */
      class Params final
      {
      public:
        /// Default Constructor
        Params() { }
      public:
        double hotBinFactor{5.0};
        double centreRegionSize{3.0};
        double minBinContent{1.0};
        bool giveRemovedPixAvCont{true};
        double neighbourFracForDeadPix{0.1};
        double hotRowColFraction{0.25};
        double hotBiRowColFraction{0.35};
        double maxEventOcc{0.5};
        bool applySCurveFilter{false};
      };

    public:

      /// Standard constructor
      Clean( const TH2D& inH,
             const unsigned long long nEvents = 0,
             const Params& params = Params() )
        : m_inHist  ( &inH    ),
          m_params  ( params  ),
          m_nEvents ( nEvents )
      { 
        reset(); 
      }

    public:

      /// Access the input histogram
      const TH2D* input() const { return m_inHist; }
      
      /// Run the filter
      std::shared_ptr<TH2D> filter() const;

    private:
      
      /// Compute average bin content from neighbours
      double avFromNeighbours( const int COL,
                               const int ROW ) const;

      /// Add to list of excluded pixels
      void excludePixel( const int i, const int j ) const
      {
        m_excludedPixels.push_back( (i*m_scale)+j );
      }

      /// check if the given pixel is excluded
      bool isExcluded( const int i, const int j ) const
      {
        return std::find( m_excludedPixels.begin(),
                          m_excludedPixels.end(),
                          (i*m_scale)+j ) != m_excludedPixels.end();
      }

      /// Reset
      void reset() const 
      { 
        m_avPixCont = 0;
        m_excludedPixels.clear();
      }
   
    private:

      /// Pointer to original histogram
      const TH2D* m_inHist = nullptr;

      /// Parameters
      Params m_params;

      /// Number of events in the histogram
      unsigned long long m_nEvents{0};

    private: // working variables
      
      /// Average pixel content
      mutable double m_avPixCont{0};

      /// List of excluded pixels
      mutable std::vector<unsigned int> m_excludedPixels;

    private:
      
      /// factor for excluded pixel list
      const int m_scale = 100000;

    };

  }
}

#endif // RICHHPDIMAGEANALYSIS_CLEANHPDIMAGE_H
