
//-----------------------------------------------------------------------------
/** @file RichBasePhotonPredictor.h
 *
 *  Header file for tool : Rich::Rec::BasePhotonPredictor
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   26/07/2007
 */
//-----------------------------------------------------------------------------

#ifndef RICHRECTOOLS_RichBasePhotonPredictor_H
#define RICHRECTOOLS_RichBasePhotonPredictor_H 1

// base class
#include "RichRecBase/RichRecToolBase.h"

// Event
#include "Event/RichRecPixel.h"
#include "Event/RichRecSegment.h"

// Interfaces
#include "RichRecInterfaces/IRichPhotonPredictor.h"
#include "RichRecInterfaces/IRichRecGeomTool.h"
#include "RichRecInterfaces/IRichCherenkovAngle.h"
#include "RichRecInterfaces/IRichCherenkovResolution.h"
#include "RichInterfaces/IRichParticleProperties.h"

// RichUtils
#include "RichUtils/RichPoissonEffFunctor.h"

// RichDet
#include "RichDet/DeRich1.h"

// boost
#include "boost/format.hpp"

// STL
#include <cmath>
#include <vector>
#include <array>

namespace Rich
{
  namespace Rec
  {

    //-----------------------------------------------------------------------------
    /** @class BasePhotonPredictor RichBasePhotonPredictor.h
     *
     *  Tool which performs the association between RichRecTracks and
     *  RichRecPixels to form RichRecPhotons.
     *
     *  This particular implementation defines bans around the expected CK theta
     *  values for the allows mass hyothesis types, that are defined as n sigma times
     *  the expected CK resolution.
     *  The hit is required to be in range for at least one possible meass hypothesis.
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   26/07/2007
     */
    //-----------------------------------------------------------------------------

    class BasePhotonPredictor : public ToolBase,
                                virtual public IPhotonPredictor
    {

    public: // Methods for Gaudi Framework

      /// Standard constructor
      BasePhotonPredictor( const std::string& type,
                           const std::string& name,
                           const IInterface* parent );

      // Initialize method
      StatusCode initialize() override;

    public: // methods (and doxygen comments) inherited from public interface

      // Is it possible to make a photon candidate using this segment and pixel.
      bool photonPossible( LHCb::RichRecSegment * segment,
                           LHCb::RichRecPixel * pixel ) const override = 0;

    protected : // data

      /// Geometry tool
      const IGeomTool * m_geomTool = nullptr;

      /// Pointer to Cherenkov angle tool
      const ICherenkovAngle * m_ckAngle = nullptr;

      /// Pointer to Cherenkov angle resolution tool
      const ICherenkovResolution * m_ckRes = nullptr;

      /// Pointer to RichParticleProperties interface
      const IParticleProperties * m_richPartProp = nullptr;

      /// Min hit radius of interest around track centres
      RadiatorArray<double> m_minROI = {{}};

      /// Max hit radius of interest around track centres
      RadiatorArray<double> m_maxROI = {{}};

      /// N sigma for acceptance bands
      RadiatorArray<double> m_nSigma = {{}};

      /// Square of m_maxROI
      RadiatorArray<double> m_maxROI2 = {{}};

      /// Square of m_minROI
      RadiatorArray<double> m_minROI2 = {{}};

      /// Internal cached parameter for speed
      RadiatorArray<double> m_scale = {{}};

      /// Particle ID types to consider in the likelihood minimisation
      Rich::Particles m_pidTypes;

    };

  }
}

#endif // RICHRECTOOLS_RichBasePhotonPredictor_H
