
//-----------------------------------------------------------------------------
/** @file RichPhotonPredictorUsingRings.cpp
 *
 *  Implementation file for tool : Rich::Rec::PhotonPredictorUsingRings
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   15/03/2002
 */
//-----------------------------------------------------------------------------

// local
#include "RichPhotonPredictorUsingRings.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Rec;

//-----------------------------------------------------------------------------

DECLARE_TOOL_FACTORY( PhotonPredictorUsingRings )

// Standard constructor
PhotonPredictorUsingRings::
PhotonPredictorUsingRings( const std::string& type,
                           const std::string& name,
                           const IInterface* parent )
  : BasePhotonPredictor( type, name, parent ),
    m_ringLoc ( LHCb::RichRecRingLocation::MarkovRings )
{
  // location in TES of rings to use
  declareProperty( "RichRecRingLocation", m_ringLoc );
}

StatusCode PhotonPredictorUsingRings::initialize()
{
  // Sets up various tools and services
  const StatusCode sc = BasePhotonPredictor::initialize();
  if ( sc.isFailure() ) { return sc; }

  // Setup incident services
  incSvc()->addListener( this, IncidentType::BeginEvent );

  // Make sure we are ready for a new event
  InitNewEvent();

  return sc;
}

// fast decision on whether a photon is possible
bool
PhotonPredictorUsingRings::photonPossible( LHCb::RichRecSegment * segment,
                                           LHCb::RichRecPixel * pixel ) const
{

  // Are they in the same Rich detector ?
  if ( segment->trackSegment().rich() != pixel->detector() ) return false;

  // Hit separation criteria : based on global coordinates
  const auto sepG = m_geomTool->trackPixelHitSep2(segment, pixel);
  if ( sepG > m_maxROI2[segment->trackSegment().radiator()] ||
       sepG < m_minROI2[segment->trackSegment().radiator()] ) return false;

  // Temporary whilst no rings in RICH1
  if ( Rich::Rich1 == segment->trackSegment().rich() ) return true;

  // Run over RichRecRings and check if the current pixel "belongs"
  // to a ring associated to the current segment
  // Search could probably be made faster using stl etc...
  for ( const auto* ring : *richRings() )
  {
    if ( ring &&
         ring->richRecSegment() &&
         ring->richRecSegment()->key() == segment->key() )
    {
      for ( const auto& pix : ring->richRecPixels() )
      {
        if ( pix.pixel() && pix.pixel()->key() == pixel->key() ) { return true; }
      }

    }
  }

  // Return false if nothing found by above search
  return false;
}


// Method that handles various Gaudi "software events"
void PhotonPredictorUsingRings::handle ( const Incident& incident )
{
  if ( IncidentType::BeginEvent == incident.type() ) InitNewEvent();
}

