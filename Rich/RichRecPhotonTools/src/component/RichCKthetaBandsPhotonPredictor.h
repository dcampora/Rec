
//-----------------------------------------------------------------------------
/** @file RichCKthetaBandsPhotonPredictor.h
 *
 *  Header file for tool : Rich::Rec::CKthetaBandsPhotonPredictor
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   26/07/2007
 */
//-----------------------------------------------------------------------------

#ifndef RICHRECTOOLS_RichCKthetaBandsPhotonPredictor_H
#define RICHRECTOOLS_RichCKthetaBandsPhotonPredictor_H 1

// base class
#include "RichBasePhotonPredictor.h"

namespace Rich
{
  namespace Rec
  {

    //-----------------------------------------------------------------------------
    /** @class CKthetaBandsPhotonPredictor RichCKthetaBandsPhotonPredictor.h
     *
     *  Tool which performs the association between RichRecTracks and
     *  RichRecPixels to form RichRecPhotons.
     *
     *  This particular implementation defines bans around the expected CK theta
     *  values for the allows mass hyothesis types, that are defined as n sigma times
     *  the expected CK resolution. 
     *  The hit is required to be in range for at least one possible meass hypothesis.
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   26/07/2007
     */
    //-----------------------------------------------------------------------------

    class CKthetaBandsPhotonPredictor final : public BasePhotonPredictor
    {
      
    public: // Methods for Gaudi Framework

      /// Standard constructor
      CKthetaBandsPhotonPredictor( const std::string& type,
                                   const std::string& name,
                                   const IInterface* parent );

    public: // methods (and doxygen comments) inherited from public interface
      
      // Is it possible to make a photon candidate using this segment and pixel.
      bool photonPossible( LHCb::RichRecSegment * segment,
                           LHCb::RichRecPixel * pixel ) const final;

    };

  }
}

#endif // RICHRECTOOLS_RichCKthetaBandsPhotonPredictor_H
