
//-----------------------------------------------------------------------------
/** @file RichSimplePhotonPredictor.h
 *
 *  Header file for tool : Rich::Rec::SimplePhotonPredictor
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   15/03/2002
 */
//-----------------------------------------------------------------------------

#ifndef RICHRECTOOLS_RICHPHOTONPREDICTOR_H
#define RICHRECTOOLS_RICHPHOTONPREDICTOR_H 1

// base class
#include "RichBasePhotonPredictor.h"

namespace Rich
{
  namespace Rec
  {

    //-----------------------------------------------------------------------------
    /** @class SimplePhotonPredictor RichSimplePhotonPredictor.h
     *
     *  Tool which performs the association between RichRecTracks and
     *  RichRecPixels to form RichRecPhotons.
     *
     *  This particular implementation uses a simple fixed cut range per radiator,
     *  on the seperation between the pixel and ray-traced track impact point.
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   15/03/2002
     */
    //-----------------------------------------------------------------------------

    class SimplePhotonPredictor final : public BasePhotonPredictor
    {

    public: // Methods for Gaudi Framework

      /// Standard constructor
      SimplePhotonPredictor( const std::string& type,
                             const std::string& name,
                             const IInterface* parent );

      /// Finalize method
      StatusCode finalize() final;

    public: // methods (and doxygen comments) inherited from public interface

      // Is it possible to make a photon candidate using this segment and pixel.
      bool photonPossible( LHCb::RichRecSegment * segment,
                           LHCb::RichRecPixel * pixel ) const final;

    private: // private data

      /// Number of selected combinations for each radiator
      mutable RadiatorArray<unsigned long long> m_Nselected = {{}}; 
      /// Number of rejected combinations for each radiator
      mutable RadiatorArray<unsigned long long> m_Nreject = {{}};   

    };

  }
}

#endif // RICHRECTOOLS_RICHPHOTONPREDICTOR_H
