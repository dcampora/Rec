
//-----------------------------------------------------------------------------
/** @file RichPixelCreatorFromAllMCRichHits.cpp
 *
 *  Implementation file for RICH reconstruction tool : Rich::Rec::PixelCreatorFromAllMCRichHits
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   15/09/2003
 */
//-----------------------------------------------------------------------------

// local
#include "RichPixelCreatorFromAllMCRichHits.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Rec::MC;

//-----------------------------------------------------------------------------

DECLARE_TOOL_FACTORY( PixelCreatorFromAllMCRichHits )

// Standard constructor
PixelCreatorFromAllMCRichHits::
PixelCreatorFromAllMCRichHits( const std::string& type,
                               const std::string& name,
                               const IInterface* parent )
  : Rich::Rec::PixelCreatorBase ( type, name, parent )
{
  // book keeping cannot work with this tool
  setProperty( "DoBookKeeping", false );
}

StatusCode PixelCreatorFromAllMCRichHits::initialize()
{
  // Sets up various tools and services
  const StatusCode sc = Rich::Rec::PixelCreatorBase::initialize();
  if ( sc.isFailure() ) { return sc; }

  // book keeping cannot work with this tool
  if ( bookKeep() )
    return Error( "Pixel Book-keeping cannot work with this pixel creator" );

  // Acquire instances of tools
  acquireTool( "RichMCTruthTool", m_mcTool, nullptr, true );

  return sc;
}

LHCb::RichRecPixel *
PixelCreatorFromAllMCRichHits::buildPixel( const Rich::PDPixelCluster& cluster ) const
{
  LHCb::RichRecPixel * pix(nullptr);
  // build hits for each pixel in the cluster
  for ( const auto S : cluster.smartIDs() ) { pix = buildPixel(S); }
  // return (last one ...)
  return pix;
}

LHCb::RichRecPixel *
PixelCreatorFromAllMCRichHits::buildPixel( const LHCb::RichSmartID& id ) const
{
  // Note : This method actually builds more than one pixel ...

  _ri_verbo << "Making pixels for " << id << endmsg;

  // First run base class method to produce reconstructed pixel
  auto * pixel = Rich::Rec::PixelCreatorBase::buildPixel(id);

  // set the associated cluster
  pixel->setAssociatedCluster( Rich::PDPixelCluster(id) );

  // save original global hit position before MC cheating starts
  const auto gPosOriginal = pixel->globalPosition();

  // Get MC hits
  const SmartRefVector<LHCb::MCRichHit>& mcRichHits = m_mcTool->mcRichHits( id );

  // Loop over hits
  bool first(true);
  for ( const auto hit : mcRichHits )
  {
    // if first, just reuse the original, otherwise clone
    auto * pix = ( first ? pixel : new LHCb::RichRecPixel(*pixel) );
    // save this pixel for clones
    if ( !first ) savePixel( pix );
    // Is this a true CK photon ?
    const LHCb::MCRichOpticalPhoton * mcPhot = m_mcTool->mcOpticalPhoton( hit );
    // Set global coordinates
    pix->setGlobalPosition( mcPhot ? mcPhot->hpdQWIncidencePoint() : gPosOriginal );
    // local position
    pix->setLocalPosition( smartIDTool()->globalToPDPanel(pix->globalPosition()) );
    // set the corrected local positions
    geomTool()->setCorrLocalPos(pix,id.rich());
    // some printout
    _ri_verbo << " -> MC cheated global pos " << pix->globalPosition() << endmsg;
    // no longer first ...
    first = false;
  }

  // return
  return pixel;
}
