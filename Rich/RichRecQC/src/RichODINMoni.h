
//-----------------------------------------------------------------------------
/** @file RichODINMoni.h
 *
 *  Header file for algorithm class : Rich::DAQ::ODINMoni
 *
 *  @author Chris Jones       Christopher.Rob.Jones@cern.ch
 *  @date   05/04/2002
 */
//-----------------------------------------------------------------------------

#ifndef RICHRECQC_RichODINMoni_H
#define RICHRECQC_RichODINMoni_H 1

// STD
#include <sstream>

// base class
#include "RichKernel/RichHistoAlgBase.h"

// Event
#include "Event/ODIN.h"

namespace Rich
{
  namespace DAQ
  {
    //-----------------------------------------------------------------------------
    /** @class ODINMoni RichODINMoni.h
     *
     *  Simple monitor for the hits in each HPD
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   05/04/2002
     */
    //-----------------------------------------------------------------------------

    class ODINMoni final : public HistoAlgBase
    {

    public:

      /// Standard constructor
      ODINMoni( const std::string& name,
                ISvcLocator* pSvcLocator );

      virtual StatusCode execute() final; ///< Algorithm execution

    protected:

      /// Pre-Book all (non-MC) histograms
      virtual StatusCode prebookHistograms() final;

    };

  }
}

#endif // RICHRECQC_RichODINMoni_H
