
//-----------------------------------------------------------------------------
/** @file RichHPDFlashMoni.h
 *
 *  Header file for algorithm class : Rich::Rec::HPDFlashMoni
 *
 *  @author Antonis Papanestis   Antonis.Papanestis@cern.ch
 *  @date   11/10/2011
 */
//-----------------------------------------------------------------------------

#ifndef RICHRECMONITOR_RichHPDFlashMoni_H
#define RICHRECMONITOR_RichHPDFlashMoni_H 1

// STD
#include <sstream>

// base class
#include "RichRecBase/RichRecHistoAlgBase.h"

// RichUtils
#include "RichUtils/RichPoissonEffFunctor.h"
#include "RichUtils/RichStatDivFunctor.h"

// temporary histogramming numbers
#include "RichRecUtils/RichDetParams.h"

// RichDet
#include "RichDet/DeRichSystem.h"

// Interfaces
#include "RichInterfaces/IRichHPDOccupancyTool.h"
#include "RichInterfaces/IRichRawBufferToSmartIDsTool.h"
#include "RichInterfaces/IRichSmartIDTool.h"

namespace Rich
{
  namespace Rec
  {
    //-------------------------------------------------------------------------
    /** @class HPDFlashMoni RichHPDFlashMoni.h
     *
     *  Simple monitor for large events in each HPD
     *
     *  @author Antonis Papanestis   Antonis.Papanestis@cern.ch
     *  @date   11/10/2011
     */
    //-------------------------------------------------------------------------

    class HPDFlashMoni final : public Rich::Rec::HistoAlgBase
    {

    public:

      /// Standard constructor
      HPDFlashMoni( const std::string& name,
                    ISvcLocator* pSvcLocator );

      virtual StatusCode initialize() final;    // Algorithm initialization
      virtual StatusCode execute   () final;    // Algorithm execution
      virtual StatusCode prebookHistograms() final;

    private:

      /// dump this event in a histogram for the specified detector
      StatusCode dumpEvent(int thisRich);

    private: // data

      /// Raw Buffer Decoding tool
      const Rich::DAQ::IRawBufferToSmartIDsTool * m_decoder = nullptr;
      const Rich::ISmartIDTool * m_idTool = nullptr;

      mutable const Rich::IHPDOccupancyTool * m_HpdOccupancyTool = nullptr;

      /// Pointer to RICH system detector element
      const DeRichSystem * m_richSys = nullptr;

      std::vector<AIDA::IProfile1D*> m_perCentHistos;
      std::vector<AIDA::IHistogram2D*> m_HpdPanelHistos;

      //std::vector<AIDA::IHistogram2D*> m_hitMaps;
      AIDA::IHistogram1D* m_goodBadEvents = nullptr;
      AIDA::IProfile1D* m_thresholdHisto = nullptr;

      int m_operationMode; // 0=absolute, 1=relative

      int m_fixedUpperThreshold;     //< fixed upper threshold
      int m_fixedLowerThreshold;     //< fixed lower threshold
      int m_absThreshold;       //< absolute threshold
      double m_relThreshold;    //< relative threshold
      double m_secondThreshold; //< lower threshold relative to higher

      unsigned long long m_events{0};  //< keep track of events
      unsigned long long m_skipEvents;       //< events to skip

      bool m_dumpEvents;      //< dump events in histograms
      unsigned long long m_dumpedEvents{0};     //< dumped events counter
      unsigned long long m_maxDumpedEvents;  //< max number of dumped events

      bool m_stopSequence;    //< control sequence filter (only bad events pass)

    };
  }
}

#endif // RICHRECMONITOR_RichHPDFlashMoni_H
