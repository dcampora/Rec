
//-----------------------------------------------------------------------------
/** @file RichPixelClusterMoni.h
 *
 *  Header file for algorithm class : Rich::Rec::PixelClusterMoni
 *
 *  @author Chris Jones       Christopher.Rob.Jones@cern.ch
 *  @date   31/02/2010
 */
//-----------------------------------------------------------------------------

#ifndef RICHRECMONITOR_RichPixelClusterMoni_H
#define RICHRECMONITOR_RichPixelClusterMoni_H 1

// STD
#include <sstream>

// base class
#include "RichRecBase/RichRecHistoAlgBase.h"

// RichUtils
#include "RichUtils/RichPoissonEffFunctor.h"
#include "RichUtils/RichStatDivFunctor.h"

// temporary histogramming numbers
#include "RichRecUtils/RichDetParams.h"

// Event
#include "Event/MCRichDigitSummary.h"

// RichDet
#include "RichDet/DeRichSystem.h"

// Interfaces
#include "MCInterfaces/IRichMCTruthTool.h"
#include "MCInterfaces/IRichRecMCTruthTool.h"
#include "RichInterfaces/IRichRawBufferToSmartIDsTool.h"

namespace Rich
{
  namespace Rec
  {

    //-----------------------------------------------------------------------------
    /** @class PixelClusterMoni RichPixelClusterMoni.h
     *
     *  Monitor class for the clustering information in the RICH
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   31/02/2010
     */
    //-----------------------------------------------------------------------------

    class PixelClusterMoni final : public Rich::Rec::HistoAlgBase
    {

    public:

      /// Standard constructor
      PixelClusterMoni( const std::string& name,
                        ISvcLocator* pSvcLocator );

      virtual StatusCode execute() final; ///< Algorithm execution

    protected:

      /// Pre-Book all (non-MC) histograms
      virtual StatusCode prebookHistograms() final;

    };

  }
}

#endif // RICHRECMONITOR_RichPixelClusterMoni_H
