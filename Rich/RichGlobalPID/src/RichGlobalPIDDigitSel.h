
//--------------------------------------------------------------------------
/** @file RichGlobalPIDDigitSel.h
 *
 *  Header file for RICH Global PID algorithm class : Rich::Rec::GlobalPID::DigitSel
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   12/12/2002
 */
//--------------------------------------------------------------------------

#ifndef RICHGLOBALPID_RICHGLOBALPIDDIGITSEL_H
#define RICHGLOBALPID_RICHGLOBALPIDDIGITSEL_H 1

#include <sstream>

// Base
#include "RichGlobalPIDAlgBase.h"
#include "RichRecUtils/RichRecProcCode.h"

namespace Rich
{
  namespace Rec
  {
    namespace GlobalPID
    {

      //--------------------------------------------------------------------------
      /** @class DigitSel RichGlobalPIDDigitSel.h
       *
       *  RichDigit selection algorithm for Rich Global PID
       *
       *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
       *  @date   12/12/2002
       */
      //--------------------------------------------------------------------------

      class DigitSel final : public AlgBase
      {

      public:

        /// Standard constructor
        DigitSel( const std::string& name,
                  ISvcLocator* pSvcLocator );

        virtual StatusCode initialize() final;    // Algorithm initialization
        virtual StatusCode execute   () final;    // Algorithm execution

      private: // private data

        /// Maximum number of usable pixels
        int m_maxUsedPixels;

      };

    }
  }
}

#endif // RICHGLOBALPID_RICHGLOBALPIDDIGITSEL_H
