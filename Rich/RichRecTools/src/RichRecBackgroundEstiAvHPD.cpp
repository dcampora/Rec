
//--------------------------------------------------------------------------
/** @file RichRecBackgroundEstiAvHPD.cpp
 *
 *  Implementation file for algorithm class : Rich::Rec::BackgroundEstiAvHPD
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   17/04/2002
 */
//--------------------------------------------------------------------------

// local
#include "RichRecBackgroundEstiAvHPD.h"
#include "RichDet/DeRich1.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Rec;

//--------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_TOOL_FACTORY( BackgroundEstiAvHPD )

// Standard constructor, initializes variables
BackgroundEstiAvHPD::BackgroundEstiAvHPD( const std::string& type,
                                          const std::string& name,
                                          const IInterface* parent )
: Rich::Rec::ToolBase ( type, name, parent ),
  m_pdData ( Rich::NRiches )
{
  // Define interface for this tool
  declareInterface<IPixelBackgroundEsti>(this);

  declareProperty( "MaxBackgroundNormIterations", m_maxBkgIterations = 10,
                   "Maximum number of iterations in background normalisation" );

  declareProperty( "PixelsPerPD", m_nPixelsPerPD = 784.763611,
                   "Number of active pixels per PD - To be got from XML eventually" );

  declareProperty( "MinPixelBackground", m_minPixBkg = 0.0,
                   "Minimum pixel background" );

  declareProperty( "IgnoreExpectedSignals", m_ignoreExpSignal = false,
                   "Ignore expectations when calculating backgrounds" );

  declareProperty( "MinHPDBckForInclusion", m_minHPDbckForInc = 0.0,
                   "Min HPD background level for setting background levels" );
}

// Initialize
StatusCode BackgroundEstiAvHPD::initialize()
{
  // Sets up various tools and services
  const StatusCode sc = Rich::Rec::ToolBase::initialize();
  if ( sc.isFailure() ) { return Error( "Failed to initialize base class", sc ); }

  // force debug
  //setProperty( "OutputLevel", MSG::VERBOSE );

  // Get Rich1
  DeRich1 * aRich1 = getDet<DeRich1> ( DeRichLocations::Rich1 );

  // HPDs or PMTs
  if ( aRich1 ->RichPhotoDetConfig() == Rich::HPDConfig )
  {
    m_nPixelsPerPD = ( aRich1->exists("RichEffectiveActiveNumPixelPerHpd") ?
                       (double)(aRich1 ->param<int>("RichEffectiveActiveNumPixelPerHpd")) :
                       784.763611 );
  }
  else if ( aRich1 ->RichPhotoDetConfig() == Rich::PMTConfig )
  {
    m_nPixelsPerPD = ( aRich1->exists("RichPmtTotNumPixel") ?
                       (double)(aRich1 ->param<int>("RichPmtTotNumPixel")) : 64.0 );
  }
  else
  {
    return Error( "Unknown PD type" );
  }

  // Acquire instances of tools
  acquireTool( "RichExpectedTrackSignal", m_tkSignal );
  acquireTool( "RichGeomEff",             m_geomEff  );

  if ( m_ignoreExpSignal )
  { _ri_debug << "Will ignore expected signals when computing backgrounds" << endmsg; }

  _ri_debug << "Minimum pixel background = " << m_minPixBkg << endmsg;
  _ri_debug << "Min HPD background level for setting background levels = "
            << m_minHPDbckForInc << endmsg;

  // pre-cache creator tools
  pixelCreator();
  trackCreator();
  statusCreator();

  return sc;
}

void BackgroundEstiAvHPD::computeBackgrounds() const
{
  _ri_debug << "Computing backgrounds using ALL "
            << richTracks()->size() << " tracks" << endmsg;

  // General init
  richInit();

  // get observed signals
  fillObservedSignalMap();

  // get expected signals
  fillExpectedSignalMap();

  // get the overal RICH backgrounds
  overallRICHBackgrounds();

  // compute the pixel backgrounds
  pixelBackgrounds();
}

void BackgroundEstiAvHPD::computeBackgrounds( const LHCb::RichRecTrack::Vector & tracks ) const
{
  _ri_debug << "Computing backgrounds using "
            << tracks.size() << " SELECTED tracks" << endmsg;

  // General init
  richInit();

  // get observed signals
  fillObservedSignalMap();

  // get expected signals
  fillExpectedSignalMap(tracks);

  // get the overal RICH backgrounds
  overallRICHBackgrounds();

  // compute the pixel backgrounds
  pixelBackgrounds();
}

void BackgroundEstiAvHPD::computeBackgrounds( const LHCb::RichRecTrack * track ) const
{
  _ri_debug << "Computing backgrounds using SINGLE track : key=" << track->key()
            << endmsg;

  // General init
  richInit();

  // get observed signals
  fillObservedSignalMap();

  // get expected signals
  fillExpectedSignalMap(track);

  // get the overal RICH backgrounds
  overallRICHBackgrounds();

  // compute the pixel backgrounds
  pixelBackgrounds();
}

void BackgroundEstiAvHPD::fillObservedSignalMap() const
{
  // Loop over pixels
  for ( auto * pixel : *richPixels() )
  {
    // RICH and HPD IDs
    const auto rich = pixel->pdPixelCluster().rich();
    const auto hpd  = pixel->pdPixelCluster().pdID();
    // the working data object for this HPD
    auto & data = pdData( rich, hpd );
    // count the number of hits in each HPD, in each RICH
    ++(data.obsSignal);
    // add to the list of pixels
    data.pixels.push_back(pixel);
  }
}

void BackgroundEstiAvHPD::fillExpectedSignalMap( const LHCb::RichRecTrack * track ) const
{
  if ( track && !m_ignoreExpSignal )
  {

    // skip tracks not in use
    if ( track->inUse() )
    {

      // Current best hypothesis for this track
      const auto id = track->currentHypothesis();

      // skip tracks below threshold, as they for sure contribute nothing to the signal
      if ( Rich::BelowThreshold != id )
      {

        // loop over segments
        for ( auto* segment : track->richRecSegments() )
        {

          // Expected detectable emitted photons for this segment
          const auto detPhots = m_tkSignal->nDetectablePhotons(segment,id);

          // which RICH
          const auto rich = segment->trackSegment().rich();

          _ri_debug << "  -> Segment " << segment->key() << " " << rich
                    << " " << segment->trackSegment().radiator()
                    << " DetPhots=" << detPhots << endmsg;

          // Tally total expected hits for each PD
          m_geomEff->geomEfficiency(segment,id); // needed to ensure map below is filled
          for ( const auto& PD : segment->geomEfficiencyPerPD(id) )
          {
            const auto sig = detPhots * PD.second; // expected signal for this PD
            pdData( rich, LHCb::RichSmartID(PD.first) ).expSignal += sig;
            _ri_debug << "   -> " << LHCb::RichSmartID(PD.first) << " DetPhots=" << sig << endmsg;
          }

        } // loop over segments

      } // Hypo OK

    } // track in use

  } // track OK

}

void BackgroundEstiAvHPD::overallRICHBackgrounds() const
{
  // Initialise detector backgrounds
  std::vector<double> bckEstimate(Rich::NRiches,0);

  // Obtain background term PD by PD
  for ( const auto rich : detectors() )
  {
    _ri_debug << "Computing HPD backgrounds in " << rich << endmsg;

    int iter = 1;
    bool cont = true;
    double rnorm = 0.0;
    while ( cont )
    {
      _ri_debug << " -> Iteration " << iter << endmsg;

      int nBelow(0), nAbove(0);
      double tBelow = 0.0;
      for ( auto& iPD : m_pdData[rich] )
      {
        // The observed signal
        const auto obs = iPD.second.obsSignal;

        // Only process PDs with observed hits
        if ( obs > 0 )
        {

          // The PD ID
          const auto & pd  = iPD.first;

          // The background for this PD
          auto       & bkg = iPD.second.expBackgrd;

          if ( 1 == iter )
          {

            // The expected signal
            const auto exp = iPD.second.expSignal;
            // First iteration, just set background for this HPD to the difference
            // between the observed and and expected number of hits in the HPD
            _ri_debug << "  -> HPD " << pd << " obs. = " << obs << " exp. = " << exp << endmsg;
            bkg = obs - exp;
          }
          else
          {
            // For additional interations apply the normalisation factor
            bkg = ( bkg > 0 ? bkg-rnorm : 0 );
          }

          if ( bkg < 0.0 )
          {
            // Count the number of HPDs below expectation for this iteration
            ++nBelow;
            // save the total amount below expectation
            tBelow += fabs( bkg );
          }
          else if ( bkg > 0.0 )
          {
            // count the number of HPDs above expectation
            ++nAbove;
          }

        }

      } // end loop over signal PDs

      _ri_debug << "  -> Above = " << nAbove << " Below = " << nBelow << endmsg;

      if ( nBelow > 0 && nAbove > 0 )
      {
        // we have some HPDs above and below expectation
        // calculate the amount of signal below per above HPD
        rnorm = tBelow / ( static_cast<double>(nAbove) );
        _ri_debug << "   -> Correction factor per HPD above = " << rnorm << endmsg;
      }
      else
      {
        _ri_debug << "  -> Aborting iterations" << endmsg;
        cont = false;
      }

      // Final protection against infinite loops
      if ( ++iter > m_maxBkgIterations ) { cont = false; }

    } // while loop

      // Finally, fill background estimates
    for ( auto& PD : m_pdData[rich] )
    {
      auto & bkg =  PD.second.expBackgrd;
      if ( bkg < m_minHPDbckForInc ) { bkg = 0; }
      bckEstimate[rich] += bkg;
    }

  } // end rich loop

  // Update detector backgrounds
  // CRJ : Ugly conversion from double to float .... To be improved
  std::vector<LHCb::RichRecStatus::FloatType> bckEstiUpdate(Rich::NRiches,0);
  bckEstiUpdate[Rich::Rich1] = (LHCb::RichRecStatus::FloatType)(bckEstimate[Rich::Rich1]);
  bckEstiUpdate[Rich::Rich2] = (LHCb::RichRecStatus::FloatType)(bckEstimate[Rich::Rich2]);
  auto * status = const_cast<LHCb::RichRecStatus*>(richStatus());
  status->setDetOverallBkg(bckEstiUpdate);

  // if ( msgLevel(MSG::DEBUG) )
  // {
  //   for ( const auto rich : detectors() )
  //   {
  //     debug() << "Overall backgrounds " << rich << " " << bckEstimate[rich] << endmsg;
  //     // loop over HPDs with signal and print the backgrounds
  //     for ( const auto& PD : m_pdData[rich] )
  //     {
  //       debug() << "  " << PD.first << " bkg = " << PD.second.expBackgrd << endmsg;
  //     }
  //   }
  // }

}

void BackgroundEstiAvHPD::pixelBackgrounds() const
{

  // Loop over the RICH data maps
  for ( const auto & richData : m_pdData )
  {
    // Loop over the PD data objects
    for ( const auto & pdData : richData )
    {
      // Do we have any pixels to set background for ?
      if ( !pdData.second.pixels.empty() )
      {

        // background for this HPD
        const auto & rbckexp = pdData.second.expBackgrd;
        double bkg = ( rbckexp>0 ? rbckexp/m_nPixelsPerPD : 0 );
        if ( bkg < m_minPixBkg ) { bkg = m_minPixBkg; }

        // printout
        if ( msgLevel(MSG::VERBOSE) )
        {
          verbose() << pdData.first
                    << " Obs "  << pdData.second.obsSignal
                    << " Exp "  << pdData.second.expSignal
                    << " bkg "  << bkg
                    << endmsg;
        }

        // Loop over the pixels for this PD and save background
        for ( auto * pixel : pdData.second.pixels )
        {
          pixel->setCurrentBackground( (LHCb::RichRecRing::FloatType)(bkg) );
        }

      }
    }
  }

}
