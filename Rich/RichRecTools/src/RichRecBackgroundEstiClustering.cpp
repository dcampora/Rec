
//--------------------------------------------------------------------------
/** @file RichRecBackgroundEstiClustering.cpp
 *
 *  Implementation file for algorithm class : Rich::Rec::BackgroundEstiClustering
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   17/04/2002
 */
//--------------------------------------------------------------------------

// local
#include "RichRecBackgroundEstiClustering.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Rec;

//--------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_TOOL_FACTORY( BackgroundEstiClustering )

// Standard constructor, initializes variables
BackgroundEstiClustering::BackgroundEstiClustering( const std::string& type,
                                                    const std::string& name,
                                                    const IInterface* parent )
  : BackgroundEstiAvHPD ( type, name, parent ) { }

double BackgroundEstiClustering::backWeight( LHCb::RichRecPixel * pixel ) const
{
  const double clusSize = (double) pixel->associatedCluster().size();
  const double nPhots   = (double) pixel->richRecPhotons().size();
  return ( clusSize / ( nPhots + 1.0 ) );
}

void BackgroundEstiClustering::pixelBackgrounds() const
{
  // first loop over all pixels once to calculate normalisation factor for each HPD
  // (sum of each pixels background weight)
  typedef Rich::Map<LHCb::RichSmartID, std::pair<int,double> > HPDNorm;
  HPDNorm hpdNorm;
  for ( auto* pixel : *richPixels() )
  {
    const auto pd = pixel->pdPixelCluster().pdID();
    ++hpdNorm[ pd ].first; // count hits in each hpd
    hpdNorm[ pd ].second += backWeight(pixel); // sum the total weight
  }
  
  // loop over pixels
  for ( auto* pixel : *richPixels() )
  {
    const auto  pd = pixel->pdPixelCluster().pdID();
    const auto det = pixel->pdPixelCluster().rich();

    // background for this HPD
    const auto hpdTotBackground = (m_pdData[det])[pd].obsSignal - (m_pdData[det])[pd].expSignal;

    // HPD normalisation
    const auto iF = hpdNorm.find(pd);
    const auto norm = ( iF == hpdNorm.end() ? 1.0 : iF->second.second / iF->second.first ); 

    // background weight for this pixel
    const auto bckWeight = backWeight(pixel) / norm;

    // background contribution for this pixel
    const LHCb::RichRecRing::FloatType pixBkg = hpdTotBackground * bckWeight / m_nPixelsPerPD ;
    
    // Save this value in the pixel
    pixel->setCurrentBackground( pixBkg > 0 ? pixBkg : 0.0f );
    
    // printout
    _ri_verbo << "Pixel " << pixel->pdPixelCluster()
              << " hpdTotBkg " << hpdTotBackground
              << " bckWeight " << bckWeight
              << " Obs "  << (m_pdData[det])[pd].obsSignal
              << " Exp "  << (m_pdData[det])[pd].expSignal
              << " bkg "  << pixel->currentBackground()
              << endmsg;
    
  }

}
