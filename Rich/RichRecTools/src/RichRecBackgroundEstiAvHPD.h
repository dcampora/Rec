
//---------------------------------------------------------------------------
/** @file RichRecBackgroundEstiAvHPD.h
 *
 *  Header file for algorithm class : Rich::Rec::BackgroundEstiAvHPD
 *
 *  @author Chris Jones       Christopher.Rob.Jones@cern.ch
 *  @date   10/01/2003
 */
//---------------------------------------------------------------------------

#ifndef RICHRECTOOLS_RichRecBackgroundEstiAvHPD_H
#define RICHRECTOOLS_RichRecBackgroundEstiAvHPD_H 1

// Base class
#include "RichRecBase/RichRecToolBase.h"

// Event
#include "Event/RichRecStatus.h"

// RichUtils
#include "RichUtils/RichMap.h"

// Interfaces
#include "RichRecInterfaces/IRichPixelBackgroundEsti.h"
#include "RichRecInterfaces/IRichExpectedTrackSignal.h"
#include "RichRecInterfaces/IRichGeomEff.h"

namespace Rich
{
  namespace Rec
  {

    //---------------------------------------------------------------------------
    /** @class BackgroundEstiAvHPD RichRecBackgroundEstiAvHPD.h
     *
     *  Background estimation tool for RICH HPDs
     *
     *  Compares the expected signal yield in each HPD, based on the observed tracks
     *  and current set of mass hypotheses for those tracks, to the total signal in
     *  each HPD. Each pixel in that HPD is then assigned the same background value.
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   10/01/2003
     */
    //---------------------------------------------------------------------------

    class BackgroundEstiAvHPD : public Rich::Rec::ToolBase,
                                virtual public IPixelBackgroundEsti
    {

    public:

      /// Standard constructor
      BackgroundEstiAvHPD( const std::string& type,
                           const std::string& name,
                           const IInterface* parent );

      virtual StatusCode initialize() final;  // initialization

    public:

      // Compute the pixel background estimates using all tracks
      virtual void computeBackgrounds( ) const final;

      // Compute the pixel background estimates using the given tracks only
      virtual void computeBackgrounds( const LHCb::RichRecTrack::Vector & tracks ) const final;

      // Compute the pixel background estimates using the given track only
      virtual void computeBackgrounds( const LHCb::RichRecTrack * track ) const final;

    protected: // definitions

      /// Data values for a single PD
      class PDData
      {
      public:
        /// Default constructor
        PDData() { pixels.reserve(5); }
      public:
        inline void reset() 
        {
          // empty the pixel list
          pixels.clear();
          // set the values back to 0
          obsSignal = expSignal = expBackgrd = 0;
        }
      public:
        double obsSignal{0};  ///< Observed signal
        double expSignal{0};  ///< Expected signal
        double expBackgrd{0}; ///< Expected background
        std::vector<LHCb::RichRecPixel*> pixels; ///< List of pixels for this HPD
      };

      // working maps
      typedef Rich::Map<LHCb::RichSmartID,PDData> PDMap;
      typedef std::vector<PDMap>            RichDataMap;

    protected: // methods

      /// General Rich Init.
      void richInit() const;

      /// Fill the observed signals map
      void fillObservedSignalMap() const;

      /// Fill the expected signals map
      void fillExpectedSignalMap( const LHCb::RichRecTrack::Vector & tracks ) const;

      /// Fill the expected signals map
      virtual void fillExpectedSignalMap( const LHCb::RichRecTrack * track ) const;

      /// Fill the expected signals map
      void fillExpectedSignalMap() const;

      /// Compute the overall RICH1/RICH2 backgrounds
      virtual void overallRICHBackgrounds() const;

      /// Compute the background term for each pixel
      virtual void pixelBackgrounds() const;

      /// Access the PDData object for a given RichSmartID and RICH
      PDData & pdData( const Rich::DetectorType rich,
                       const LHCb::RichSmartID & hpd ) const;

    private: // data members

      /// Pointers to expected track signal tool
      const IExpectedTrackSignal * m_tkSignal = nullptr;

      /// Geometrical efficiency tool
      const IGeomEff * m_geomEff = nullptr;

      /// Maximum number of iterations in background normalisation
      int m_maxBkgIterations;

    protected: // data

      /// Number of pixels per HPD without cathode acceptance
      double m_nPixelsPerPD;

      /// The HPD data map
      mutable RichDataMap m_pdData;

      /** Minimum pixel background value.
       *  Any value below this will be set to this value */
      double m_minPixBkg;

      /** Ignore the expected signal when computing the background terms.
          Effectively, will assume all observed hists are background */
      bool m_ignoreExpSignal;

      /** Min HPD background level for setting background levels */
      double m_minHPDbckForInc;

    };

    inline BackgroundEstiAvHPD::PDData & 
    BackgroundEstiAvHPD::pdData( const Rich::DetectorType rich,
                                 const LHCb::RichSmartID & hpd ) const
    {
      return (m_pdData[rich])[hpd];
    }
    
    inline void BackgroundEstiAvHPD::richInit() const
    {
      // reset the working maps
      for ( auto & data : m_pdData ) 
      { for ( auto & hpd : data ) { hpd.second.reset(); } }
    }

    inline void BackgroundEstiAvHPD::fillExpectedSignalMap() const
    {
      for ( const auto* track : *richTracks() ) { fillExpectedSignalMap(track); }
    }

    inline void
    BackgroundEstiAvHPD::
    fillExpectedSignalMap( const LHCb::RichRecTrack::Vector & tracks ) const
    {
      for ( const auto* track : tracks ) { fillExpectedSignalMap(track); }
    }


  }
}

#endif // RICHRECTOOLS_RichRecBackgroundEstiAvHPD_H
