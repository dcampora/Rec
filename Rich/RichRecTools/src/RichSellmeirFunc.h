
//-----------------------------------------------------------------------------
/** @file RichSellmeirFunc.h
 *
 *  Header file for tool : Rich::Rec::SellmeirFunc
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   15/03/2002
 */
//-----------------------------------------------------------------------------

#ifndef RICHRECTOOLS_RICHSELLMEIRFUNC_H
#define RICHRECTOOLS_RICHSELLMEIRFUNC_H 1

// base class
#include "RichRecBase/RichRecToolBase.h"

// Kernel
#include "Kernel/RichRadiatorType.h"
#include "Kernel/RichParticleIDType.h"

// Rich Utils
#include "RichUtils/StlArray.h"

// Event model
#include "Event/RichRecSegment.h"

// Detector Description
#include "RichDet/DeRich1.h"
#include "RichDet/DeRichAerogelRadiator.h"

// interfaces
#include "RichRecInterfaces/IRichSellmeirFunc.h"
#include "RichInterfaces/IRichParticleProperties.h"

// VDT
#include "vdt/exp.h"
#include "vdt/log.h"

namespace Rich
{
  namespace Rec
  {

    //-----------------------------------------------------------------------------
    /** @class SellmeirFunc RichSellmeirFunc.h
     *
     *  Tool to calculate quantities using the Sellmeir function and related
     *  parameters.
     *
     *  Uses formulae 37-39 in CERN-EP/89-150  (Ypsilantis)
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   15/03/2002
     */
    //-----------------------------------------------------------------------------

    class SellmeirFunc final : public Rich::Rec::ToolBase,
                               virtual public ISellmeirFunc
    {

    public: // Methods for Gaudi Framework

      /// Standard constructor
      SellmeirFunc( const std::string& type,
                    const std::string& name,
                    const IInterface* parent );

      // Initialize method
      StatusCode initialize() final;

    public: // methods (and doxygen comments) inherited from public interface

      // Computes the number of photons emitted in the given energy range for a
      // given RichRecSegment under a certain mass hypothesis
      double photonsInEnergyRange( const LHCb::RichRecSegment * segment,
                                   const Rich::ParticleIDType id,
                                   const double botEn,
                                   const double topEn ) const final;

    private: // definitions

      /// Array for radiator information
      using RadArray  = std::array<double,Rich::NRadiatorTypes>;
      /// Array for particle information
      using PartArray = std::array<double,Rich::NParticleTypes>;

    private: // methods

      /// Update the cached parameters
      StatusCode umsUpdate();

      /// internal calculation for photon yield
      double paraW( const Rich::RadiatorType rad, const double energy ) const;

    private: // data

      // cached parameters for speed
      RadArray m_RXSPscale = {{}};
      RadArray m_RXSMscale = {{}};
      RadArray m_REP       = {{}};
      RadArray m_REM       = {{}};
      RadArray m_X         = {{}};

      // particle hypothesis masses squared
      PartArray m_particleMassSq = {{}};

      // Rich1 Detector element
      DeRich1 * m_Rich1DE = nullptr;

      // particle properties
      const IParticleProperties * m_partProp = nullptr;

    };

    inline double SellmeirFunc::paraW ( const Rich::RadiatorType rad,
                                        const double energy ) const
    {
      const auto X = ( m_RXSPscale[rad] * 
                       (double)vdt::fast_logf( (float)((m_REP[rad]+energy)/(m_REP[rad]-energy)) ) );
      const auto Y = ( m_RXSMscale[rad] * 
                       (double)vdt::fast_logf( (float)((m_REM[rad]+energy)/(m_REM[rad]-energy)) ) );
      return m_X[rad] * (X-Y);
    }

  }
}

#endif // RICHRECTOOLS_RICHSELLMEIRFUNC_H
