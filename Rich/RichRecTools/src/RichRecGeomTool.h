
//-----------------------------------------------------------------------------
/** @file RichRecGeomTool.h
 *
 *  Header file for tool : Rich::Rec::GeomTool
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   15/03/2002
 */
//-----------------------------------------------------------------------------

#ifndef RICHRECTOOLS_RICHRECGEOMTOOL_H
#define RICHRECTOOLS_RICHRECGEOMTOOL_H 1

// STL
#include <cmath>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichRecBase/RichRecToolBase.h"

// Rich Utils
#include "RichUtils/StlArray.h"

// Event
#include "Event/RichRecPixel.h"
#include "Event/RichRecSegment.h"

// interfaces
#include "RichInterfaces/IRichDetParameters.h"
#include "RichRecInterfaces/IRichRecGeomTool.h"
#include "RichRecInterfaces/IRichCherenkovAngle.h"

// VDT
#include "vdt/asin.h"

namespace Rich
{
  namespace Rec
  {

    //-----------------------------------------------------------------------------
    /** @class GeomTool RichRecGeomTool.h
     *
     *  Tool to answer simple geometrical questions
     *  using the reconstruction event model
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   15/03/2002
     *
     *  @todo Find a way to calibrate the average radiator distortion parameters
     *  @todo Finish hpdPanelAcceptance method
     */
    //-----------------------------------------------------------------------------

    class GeomTool final : public Rich::Rec::ToolBase,
                           virtual public IGeomTool
    {

    public: // Methods for Gaudi Framework

      /// Standard constructor
      GeomTool( const std::string& type,
                const std::string& name,
                const IInterface* parent );

      // Initialize method
      StatusCode initialize() final;

    public: // methods (and doxygen comments) inherited from public interface

      // Returns square of distance seperating the pixel hit and hit position extrapolated
      // using the RichRecSegment direction in local corrdinates
      double trackPixelHitSep2( const LHCb::RichRecSegment * segment,
                                const LHCb::RichRecPixel * pixel ) const final;

      // Computes the fraction of the Cherenkov cone for a given segment that
      //  is within the average HPD panel acceptance
      double hpdPanelAcceptance( LHCb::RichRecSegment * segment,
                                 const Rich::ParticleIDType id ) const final;

      // Returns the corrected pixel position for the average distortions in the given radiator
      Gaudi::XYZPoint
      radCorrLocalPos( const Gaudi::XYZPoint & lpos,
                       const Rich::RadiatorType rad ) const final;

      // Set the corrected pixel position for the average
      // optical distortion for the given RICH detector
      void setCorrLocalPos( const LHCb::RichRecPixel * pixel,
                            const Rich::DetectorType rich ) const final;
      
    private: // methods

      /// Get the corrected position for given point in given radiator
      inline Gaudi::XYZPoint getCorrPos( const Gaudi::XYZPoint & point,
                                         const Rich::RadiatorType rad ) const
      {
        return Gaudi::XYZPoint( (1-m_radScale[rad]) * point.x(),
                                (1+m_radScale[rad]) * point.y(),
                                point.z() );
      }

    private: // private data

      // Pointers to tool instances
      const IDetParameters  * m_detParams = nullptr; ///< Detector parameters tool
      const ICherenkovAngle * m_ckAngle   = nullptr; ///< Pointer to the Cherenkov angle tool

      /// Radiator correction scale parameter
      RadiatorArray<double> m_radScale = {{}};

      /// The radiator outer limits in local coordinates
      RadiatorArray<IDetParameters::RadLimits> m_radOutLimLoc = {{}};

    };

  }
}

#endif // RICHRECTOOLS_RICHRECGEOMTOOL_H
