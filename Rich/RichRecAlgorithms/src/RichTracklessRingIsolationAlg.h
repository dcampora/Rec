
//---------------------------------------------------------------------------
/** @file RichTracklessRingIsolationAlg.h
 *
 *  Header file for algorithm class : Rich::Rec::TracklessRingIsolationAlg
 *
 *  @author Chris Jones       Christopher.Rob.Jones@cern.ch
 *  @date   10/01/2003
 */
//---------------------------------------------------------------------------

#ifndef RICHRECALGORITHMS_RichTracklessRingIsolationAlg_H
#define RICHRECALGORITHMS_RichTracklessRingIsolationAlg_H 1

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Base class
#include "RichRecBase/RichRecAlgBase.h"

// Event
#include "Event/RichRecStatus.h"

// Utils
#include "RichUtils/RichMap.h"

// Rec base
#include "RichRecBase/FastRingFitter.h"

namespace Rich
{
  namespace Rec
  {

    //---------------------------------------------------------------------------
    /** @class TracklessRingIsolationAlg RichTracklessRingIsolationAlg.h
     *
     *  Reads in a set of trackless RichRecRing objects and tries to determine
     *  those which are 'isolated'
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   12/06/2008
     */
    //---------------------------------------------------------------------------

    class TracklessRingIsolationAlg final : public Rich::Rec::AlgBase
    {

    public:

      /// Standard constructor
      TracklessRingIsolationAlg( const std::string& name,
                                 ISvcLocator* pSvcLocator );

      virtual StatusCode initialize() final; ///< Algorithm initialisation
      virtual StatusCode execute() final;    ///< Algorithm execution

    private:

      /** Test the given cut, and keep a summary tally of the result
       *  @param desc   Cut description
       *  @param rad    RICH radiator
       *  @param result Was the cut passed or failed
       *  @return result
       */
      inline bool testCut( const std::string & desc, 
                           const Rich::RadiatorType rad,
                           const bool result ) const
      {
        if ( !m_abortEarly )
        {
          if ( msgLevel(MSG::VERBOSE) && !result )
          {
            verbose() << "  -> '" << desc << "' FAILED" << endmsg;
          }
          counter(Rich::text(rad)+" '"+desc+"'") += ( result ? 1.0 : 0.0 );
        }
        return result;
      }

    private:

      bool m_abortEarly; ///< Reject tracks as soon as possible

      /// Input location in TES for rings
      std::string m_inputRings;

      /// Output location in TES for rings
      std::string m_outputRings;
        
      /// Minimum separation between ring centres for each radiator
      RadiatorArray<double> m_sizesepcut = {{}};

      /// Search window for pixels
      RadiatorArray<double> m_pixelWin = {{}};

      /// Minimum fraction of pixels in ring region
      RadiatorArray<double> m_sizeringwidth = {{}};
      RadiatorArray<double> m_sizepixelcut = {{}};
      RadiatorArray<double> m_ckThetaMax = {{}};
      RadiatorArray<double> m_sepGMax = {{}};
      RadiatorArray<double> m_scale = {{}};
      ///Phi cuts
      RadiatorArray<unsigned int> m_nPhiRegions = {{}};
      RadiatorArray<double> m_sizephicut = {{}};

      // Max ring refiting variance
      RadiatorArray<double> m_maxFitVariance = {{}};
    
    };

  }
}

#endif // RICHRECALGORITHMS_RichTracklessRingIsolationAlg_H
