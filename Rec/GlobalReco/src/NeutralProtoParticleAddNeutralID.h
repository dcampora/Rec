#ifndef NEUTRALPROTOPARTICLEADDNEUTRALID_H 
#define NEUTRALPROTOPARTICLEADDNEUTRALID_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "CaloInterfaces/IGammaPi0SeparationTool.h"
#include "CaloInterfaces/INeutralIDTool.h"
#include "Event/ProtoParticle.h"

/** @class NeutralProtoParticleAddNeutralID NeutralProtoParticleAddNeutralID.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2014-05-27
 */

class NeutralProtoParticleAddNeutralID final : public GaudiAlgorithm
{

 public:

  /// Standard constructor
  NeutralProtoParticleAddNeutralID( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~NeutralProtoParticleAddNeutralID( ) = default; ///< Destructor

  virtual StatusCode initialize();    ///< Algorithm initialization
  virtual StatusCode execute   ();    ///< Algorithm execution

 private:

  bool addInput( int index,
                 double* data,
                 const LHCb::ProtoParticle* proto,
                 const LHCb::ProtoParticle::additionalInfo flag,
                 const std::string func = "none" );

 private:

  std::string m_input;
  bool m_isNotE;
  bool m_isNotH;
  bool m_isPhoton;
  double m_isNotE_Pt;
  double m_isNotH_Pt;
  double m_isPhoton_Pt;
  IGammaPi0SeparationTool* m_gammaPi0 = nullptr;
  INeutralIDTool* m_neutralID = nullptr;

};

inline bool
NeutralProtoParticleAddNeutralID::addInput( int index,
                                            double* data,
                                            const LHCb::ProtoParticle* proto,
                                            const LHCb::ProtoParticle::additionalInfo flag,
                                            const std::string func )
{
  auto v = proto->info( flag,-1e+06 );
  if      ( func == "abs"  ) { v = fabs(v); }
  else if ( func == "area" ) { v = double( (int( v ) >> 12) & 0x3 ); }
  data[index] = v;
  return (  proto->hasInfo( flag ) ) ;
}

#endif // NEUTRALPROTOPARTICLEADDNEUTRALID_H
