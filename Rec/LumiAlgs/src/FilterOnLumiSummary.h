#ifndef FILTERONLUMISUMMARY_H 
#define FILTERONLUMISUMMARY_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

// event model
#include "Event/HltLumiSummary.h"

/** @class FilterOnLumiSummary FilterOnLumiSummary.h
 *  
 *
 *  @author Jaap Panman
 *  @date   2010-01-29
 */
class FilterOnLumiSummary : public GaudiAlgorithm {
public: 
  /// Standard constructor
  FilterOnLumiSummary( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution

private:

  std::string m_DataName;             // input location of summary data
  std::string m_ValueName;            // value required 
  std::string m_CounterName;          // counter looked at
  int m_Counter;                      // int value required   
  int m_Value;			      // int counter looked at


};
#endif // FILTERONLUMISUMMARY_H
