#ifndef LUMICHECKCONDDB_H 
#define LUMICHECKCONDDB_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

// for incidents listener
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"

// for DB
#include "GaudiKernel/IDetDataSvc.h"
#include "GetLumiParameters.h"

// for TCK
#include "Kernel/IPropertyConfigSvc.h"

/** @class LumiCheckCondDB LumiCheckCondDB.h
 *   
 *  @author Jaap Panman
 *  @date   2010-10-20
 */

class LumiCheckCondDB final : public GaudiAlgorithm,
                              public virtual IIncidentListener 
{

public: 

  /// Standard constructor
  LumiCheckCondDB( const std::string& name, ISvcLocator* pSvcLocator );

  virtual StatusCode initialize();    ///< Algorithm initialization
  virtual StatusCode execute   ();    ///< Algorithm execution
  virtual StatusCode stop      ();    ///< Algorithm stopping
  virtual StatusCode finalize  ();    ///< Algorithm finalization

  // ==========================================================================
  // IIncindentListener interface
  // ==========================================================================
  virtual void handle ( const Incident& ) ;
  // ==========================================================================

private:

  void checkDB( const std::string& state ) const; ///< DB checking code - event loop

private:

  unsigned long long m_startTime{0};         ///< start probing database here
  unsigned long long m_numberSteps{0};       ///< number of steps checked
  unsigned long long m_stepHours{0};         ///< number of hours per step

  // database conditions and calibration factors
  IGetLumiParameters   * m_dbTool = nullptr; ///< tool to query luminosity database
  IIncidentSvc         * m_incSvc = nullptr; ///< the incident service 

  /// Reference to run records data service  
  SmartIF<IDetDataSvc> m_dds = nullptr;

};
#endif // LUMICHECKCONDDB_H
