#ifndef LUMIFILEREADER_H 
#define LUMIFILEREADER_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

#include "Event/RawEvent.h"
#include "Event/ODIN.h"
#include "Event/HltLumiSummary.h"
#include "Event/FileId.h"

// forward declarations
namespace LHCb {
  class RawBank ;
}


/** @class LumiFileReader LumiFileReader.h
 *   
 *
 *  @author Jaap Panman
 *  @date   2009-10-06
 */
class LumiFileReader : public GaudiAlgorithm {
public: 
  /// Standard constructor
  LumiFileReader( const std::string& name, ISvcLocator* pSvcLocator );

  virtual StatusCode initialize();    ///< Algorithm initialization
  virtual StatusCode execute   ();    ///< Algorithm execution
  virtual StatusCode finalize  ();    ///< Algorithm finalization

private:
  // data
  std::string m_rawEventLocation;           ///< Location where we get the RawEvent
  std::string m_OutputFileName;

  // Statistics  
  double m_totDataSize = 0;
  int m_nbEvents = 0;
  LHCb::FileId m_fileId;

};
#endif // FILEREADER_H
