#ifndef TRACKFITTER_SCHEDULER_H
#define TRACKFITTER_SCHEDULER_H 1

#include <numeric>
#include <tuple>
#include "Event/FitNodeVec.h"
//#include "Tools.h"

namespace VectorFit {

  struct SchItem {
    std::vector<FitNode, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>* track;
    std::vector<FitNode, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>::iterator prevnode;
    std::vector<FitNode, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>::iterator node;
    std::vector<FitNode, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>::iterator firstnode;
    std::vector<FitNode, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>::iterator lastnode;
    unsigned int trackIndex;

    SchItem () = default;
    SchItem (const SchItem& copy) = default;
    SchItem (
        std::vector<FitNode, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>* track,
        const unsigned int& prevnodeIndex,
        const unsigned int& nodeIndex,
        const unsigned int& backwardUpstream,
        const unsigned int& trackIndex
        ) : track(track), trackIndex(trackIndex)
    {
      prevnode  = track->begin() + prevnodeIndex;
      node      = track->begin() + nodeIndex;
      lastnode = track->end() - backwardUpstream - 1;
    }
  };

  struct DumbStaticScheduler {
    static std::vector<std::tuple<uint16_t, uint16_t, uint16_t, std::array<SchItem, VECTOR_WIDTH>>>
      generate (
          std::vector<Track, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>& ntracks,
          const bool showSchedule
          );
  };

  struct SwapStore {
    State& store;
    TrackVector state;
    TrackSymMatrix covariance;

    SwapStore (State& store, const TrackVector& state, const TrackSymMatrix& covariance) :
      store(store), state(state), covariance(covariance) {}
  };

}
#endif
