#ifndef TRACKFITTER_UPDATE_H
#define TRACKFITTER_UPDATE_H 1

#include "Event/FitNodeVec.h"
//#include "../oldfit/Similarity.h"
#include "FitMath.h"
#include "ArrayGen.h"

namespace VectorFit {

template<class D>
inline void update (
  VectorFit::FitNode& node
) {
  if (node.m_type != LHCb::Node::HitOnTrack) {
    node.getState<D, Op::Update, Op::StateVector>().setBasePointer(node.getState<D, Op::Predict, Op::StateVector>());
    node.getState<D, Op::Update, Op::Covariance>().setBasePointer(node.getState<D, Op::Predict, Op::Covariance>());
  } else {
    node.getState<D, Op::Update, Op::StateVector>().copy(node.getState<D, Op::Predict, Op::StateVector>());
    node.getState<D, Op::Update, Op::Covariance>().copy(node.getState<D, Op::Predict, Op::Covariance>());

    FitMathNonVec::update (
      node.getState<D, Op::Update, Op::StateVector>(),
      node.getState<D, Op::Update, Op::Covariance>(),
      node.getChi2<D>(),
      node.m_refVector.m_parameters.fArray,
      node.m_projectionMatrix.fArray,
      node.m_refResidual,
      node.m_errMeasure * node.m_errMeasure
    );
  }
}

template<unsigned W>
inline void update_vec (
  std::array<SchItem, W>& n,
  fp_ptr_64_const ps,
  fp_ptr_64_const pc,
  fp_ptr_64 us,
  fp_ptr_64 uc,
  fp_ptr_64 chi2
) {
  _aligned const std::array<PRECISION, 5*W> Xref = ArrayGen::getReferenceVector(n);
  _aligned const std::array<PRECISION, 5*W> H = ArrayGen::getProjectionMatrix(n);
  _aligned const std::array<PRECISION, W> refResidual = ArrayGen::getRefResidual(n);
  _aligned const std::array<PRECISION, W> errorMeas2 = ArrayGen::getErrMeasure2(n);

  FitMathCommon<W>::update (
    us,
    uc,
    chi2,
    ps,
    pc,
    Xref,
    H,
    refResidual,
    errorMeas2,
    n
  );
}

}


#endif // TRACKFITTER_UPDATE_H 
