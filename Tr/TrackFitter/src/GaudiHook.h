#ifndef TRACKFITTER_GAUDIHOOK_H
#define TRACKFITTER_GAUDIHOOK_H

#include <algorithm>
#include <functional>
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <cassert>
#include <map>
#include <array>
#include <deque>
#include <unistd.h>

// New types
#include "Event/FitNodeVec.h"
#include "Predict.h"
#include "Update.h"
#include "Scheduler.h"
#include "Smoother.h"

std::vector<VectorFit::Track,aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>> convertToNTrack (
  std::vector<std::vector<std::reference_wrapper<LHCb::FitNode>>>::iterator& itBegin,
  std::vector<std::vector<std::reference_wrapper<LHCb::FitNode>>>::iterator& itEnd,
  VectorFit::GrowingMemManager& memManager,
  const bool& debug
);

void convertBack (
  std::vector<VectorFit::Track, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>& ntracks,
  std::vector<std::vector<std::reference_wrapper<LHCb::FitNode>>>::iterator& itBegin,
  std::vector<std::vector<std::reference_wrapper<LHCb::FitNode>>>::iterator& itEnd,
  const bool& debug
);

void crosskalman (
  std::vector<std::vector<std::reference_wrapper<LHCb::FitNode>>>::iterator& itBegin,
  std::vector<std::vector<std::reference_wrapper<LHCb::FitNode>>>::iterator& itEnd,
  const bool& vectorised_opt,
  const bool& debug
);

void crosskalmanOutliers (
  std::vector<std::vector<std::reference_wrapper<LHCb::FitNode>>>::iterator& itBegin,
  std::vector<std::vector<std::reference_wrapper<LHCb::FitNode>>>::iterator& itEnd,
  const bool& debug
);

#endif
