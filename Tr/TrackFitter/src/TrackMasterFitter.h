#ifndef TRACKFITTER_TRACKMASTERFITTER_H 
#define TRACKFITTER_TRACKMASTERFITTER_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/ToolHandle.h"

// interface base class
#include "TrackInterfaces/ITrackFitter.h"
#include "TrackInterfaces/ITrackProjectorSelector.h"
#include "TrackInterfaces/IMeasurementProvider.h"
#include "TrackInterfaces/ITrackKalmanFilter.h"
#include "TrackInterfaces/IMaterialLocator.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
// dcampora
#include "Event/FitAllNodes.h"

// Forward declarations
class ITrackManipulator ;
class IMaterialLocator ;
class ITrackExtrapolator ;

namespace LHCb {
  class Track ;
  class FitNode ;
  class State ;
  class TrackFitResult ;
}

/** @class TrackMasterFitter TrackMasterFitter.h
 *  
 *
 *  @author Jose Angel Hernando Morata, Eduardo Rodrigues
 *  @date   2005-04-15
 *  reusing the previous code
 *  @author Rutger van der Eijk  07-04-1999
 *  @author Matthew Needham 
 */

class TrackMasterFitter : public extends<GaudiTool, ITrackFitter> {
public: 
  /// Standard constructor
  TrackMasterFitter( const std::string& type, 
                     const std::string& name,
                     const IInterface* parent );

  StatusCode initialize() override;
  StatusCode finalize() override;

  //! fit all tracks
  StatusCode fitAll(
    const std::vector<std::reference_wrapper<LHCb::Track>>::iterator& alltracksBegin,
    const std::vector<std::reference_wrapper<LHCb::Track>>::iterator& alltracksEnd ) const;
  //! fit a track 
  StatusCode fit( LHCb::Track& track, LHCb::ParticleID pid ) const;
  // dcampora: it's a pi+, by following http://pdg.lbl.gov/2006/reviews/montecarlorpp.pdf

private:

  //! initializes reference states for initial trajectory
  StatusCode initializeRefStates(LHCb::Track& track, LHCb::ParticleID pid ) const ;

  //! determines track state at various z positions
  StatusCode determineStates( LHCb::Track& track ) const;

  //! removes outlier from the node vector
  LHCb::Node* removeOutlier (LHCb::Track& track) const;

  //! updates the reference vector for each measurement before next iteration
  StatusCode updateRefVectors( LHCb::Track& track, LHCb::ParticleID pid , bool doUpdateTransport ) const;

  //! projectReference state
  StatusCode projectReference( LHCb::Track& track ) const;

  //! Retrieves the number of nodes with a measurement
  unsigned int nNodesWithMeasurement( const LHCb::Track& track ) const;

  //! Creates the nodes from the measurements
  StatusCode makeNodes( LHCb::Track& track, LHCb::ParticleID pid ) const;

  //! Updates material corrections stored in nodes
  StatusCode updateMaterialCorrections( LHCb::Track& track, LHCb::ParticleID pid = LHCb::ParticleID(211) ) const ;

  //! Updates transport matrices stored in nodes
  StatusCode updateTransport( LHCb::Track& track ) const ;

  const ITrackExtrapolator* extrapolator( LHCb::Track::Types tracktype ) const {
    if(tracktype == LHCb::Track::Velo ||  tracktype == LHCb::Track::VeloR  ) return &(*m_veloExtrapolator) ;
    return &(*m_extrapolator);
  }

private:
 
  ToolHandle<ITrackExtrapolator> m_extrapolator;     ///< extrapolator
  ToolHandle<ITrackExtrapolator> m_veloExtrapolator; ///< extrapolator for Velo-only tracks
  ToolHandle<ITrackKalmanFilter> m_trackNodeFitter; ///< delegate to actual track fitter (which fits from nodes)
  ToolHandle<IMeasurementProvider> m_measProvider;
  ToolHandle<IMaterialLocator>   m_materialLocator ;
  ToolHandle<ITrackProjectorSelector> m_projectorSelector ;

private:

  // job options
  std::string m_extrapolatorName;   ///< name of the extrapolator in Gaudi
  std::string m_veloExtrapolatorName; ///< name of the velo-only extrapolator 
  bool m_upstream;                  ///< switch between upstream/downstream fit
  bool m_addDefaultRefNodes  ;      ///< add default reference nodes
  bool m_stateAtBeamLine;           ///< add state closest to the beam-line?
  int m_numFitIter;                 ///< number of fit iterations to perform
  double m_chi2Outliers;            ///< chi2 of outliers to be removed
  int m_numOutlierIter;             ///< max number of outliers to be removed
  bool m_useSeedStateErrors;        ///< use errors of the seed state
  bool m_useClassicalSmoother  ;      ///< Use classical smoother
  bool m_fillExtraInfo ;            ///< Fill the extra info
  
  double m_errorX ;                 ///< Seed error on x
  double m_errorY ;                 ///< Seed error on y
  double m_errorTx ;                ///< Seed error on slope x
  double m_errorTy ;                ///< Seed error on slope y
  std::vector<double> m_errorQoP ;  ///< Seed error on QoP

  bool m_makeNodes;
  bool m_makeMeasurements;
  bool m_updateTransport ;            ///< Update the transport matrices between iterations
  int  m_maxUpdateTransports;         ///< Update transport only n-times during iterations
  bool m_updateMaterial ;             ///< Update material corrections between iterations
  bool m_updateReferenceInOutlierIters ; ///< Update projection in iterations in which outliers are removed
  double m_minMomentumForELossCorr ;  ///< Minimum momentum used in correction for energy loss
  bool m_applyMaterialCorrections ;   ///< Apply material corrections
  bool m_applyEnergyLossCorrections ; ///< Apply material corrections
  double m_maxDeltaChi2Converged ;    ///< Maximum change in chisquare for converged fit

  double m_scatteringPt ;           ///< transverse momentum used for scattering if track has no good momentum estimate
  double m_scatteringP ;            ///< momentum used for scattering in e.g. magnet off data
  double m_minMomentumForScattering ; ///< Minimum momentum used for scattering
  double m_maxMomentumForScattering ; ///< Maximum momentum used for scattering
  size_t m_minNumVeloRHits   ; ///< Minimum number of VeloR hits
  size_t m_minNumVeloPhiHits ; ///< Minimum number of VeloPhi hits
  size_t m_minNumTTHits      ; ///< Minimum number of TT hits
  size_t m_minNumTHits       ; ///< Minimum number of T hits
  size_t m_minNumMuonHits    ; ///< Minimum number of Muon hits
  
  // TrackKalmanFilter
  bool m_forceBiDirectionalFit;        ///< Flag for forcing bidirectional fit
  bool m_forceSmooth;             ///< Flag for force the smoothing (for debug reason)
  unsigned int m_DoF;
  bool m_mySwitch;             ///< Flag for force the smoothing (for debug reason)

  //! helper to print a failure comment
  StatusCode failure (const std::string& comment) const;
  StatusCode failureInfo (const std::string& comment) const;
  
  bool m_debugLevel;

  void printIterations (const std::vector<int>& counter);
  std::vector<int> fit_iterations_counter;
  std::vector<int> fit_iterations_outlier_counter;
  mutable int m_trackKey;

  // Staged fit
  void stagedFitInitializeTracks (std::vector<std::reference_wrapper<LHCb::Track>>& processingTracks) const;
  void stagedFitParallelFit (std::vector<std::reference_wrapper<LHCb::Track>>& processingTracks) const;
  void stagedFitPostProcess (std::vector<std::reference_wrapper<LHCb::Track>>& processingTracks) const;

  // Helper functions for the staged fit
  std::vector<std::reference_wrapper<LHCb::Track>>::iterator parallelUpdateRefVectors (
    std::vector<std::reference_wrapper<LHCb::Track>>& processingTracks,
    const std::vector<std::reference_wrapper<LHCb::Track>>::iterator& updateRefVectorsEnd) const;

  void parallelUpdateState (
    std::vector<std::reference_wrapper<LHCb::Track>>& processingTracks,
    const std::vector<std::reference_wrapper<LHCb::Track>>::iterator& processingTracksEnd) const;

  std::vector<std::vector<std::reference_wrapper<LHCb::FitNode>>> getNodesToProcess (
    const std::vector<std::reference_wrapper<LHCb::Track>>::iterator& processingTracksBegin,
    const std::vector<std::reference_wrapper<LHCb::Track>>::iterator& processingTracksEnd) const;

  void fitSmooth (
    const std::vector<std::reference_wrapper<LHCb::Track>>::iterator& fittingTracksBegin,
    const std::vector<std::reference_wrapper<LHCb::Track>>::iterator& fittingTracksEnd) const;

  void fitOutliers (
    const std::vector<std::reference_wrapper<LHCb::Track>>::iterator& fittingTracksBegin,
    const std::vector<std::reference_wrapper<LHCb::Track>>::iterator& fittingTracksEnd
  ) const;

  void scalarFitSmooth (
    std::vector<std::vector<std::reference_wrapper<LHCb::FitNode>>> nodes
  ) const;

  void compare (
    std::vector<std::vector<std::reference_wrapper<LHCb::FitNode>>>& nodes,
    std::vector<std::vector<std::reference_wrapper<LHCb::FitNode>>>& nodes_vec
  ) const;

  template<class T>
  bool tcompare (
    const T& v0,
    const T& v1,
    const unsigned N,
    const unsigned bitdiff
  ) const {
    auto equal = [&bitdiff] (const double& a, const double& b) {
      const long long* i1 = reinterpret_cast<const long long*>(&a);
      const long long* i2 = reinterpret_cast<const long long*>(&b);
      const long long epsilon = std::abs(*i1 - *i2);
      const long long max_epsilon = (long long) pow(2.0, ((double) bitdiff));
  
      return epsilon < max_epsilon;
    };

    auto print = [&N] (const T& a) {
      std::cout << " ";
      for (unsigned i=0; i<N; ++i) {
        std::cout << a.Array()[i] << " ";
      }
      std::cout << std::endl;
    };

    for (unsigned i=0; i<N; ++i) {
      if (not equal(v0.Array()[i], v1.Array()[i])) {
        std::cout << "Elements differ: (" << v0.Array()[i] << ", " << v1.Array()[i] << ")" << std::endl;
        print(v0);
        print(v1);
        std::cout << std::endl;

        return false;
      }
    }
    return true;
  }

  inline bool compare (
    const ROOT::Math::SVector<double, 5u>& v0,
    const ROOT::Math::SVector<double, 5u>& v1,
    const unsigned bitdiff
  ) const {
    unsigned N = 5;
    return tcompare (v0, v1, N, bitdiff);
  }

  inline bool compare (
    const ROOT::Math::SMatrix<double, 5u, 5u, ROOT::Math::MatRepSym<double, 5u>>& v0,
    const ROOT::Math::SMatrix<double, 5u, 5u, ROOT::Math::MatRepSym<double, 5u>>& v1,
    const unsigned bitdiff
  ) const {
    unsigned N = 15;
    return tcompare (v0, v1, N, bitdiff);
  }

  inline bool compare (
    const ROOT::Math::SMatrix<double, 1u, 5u>& v0,
    const ROOT::Math::SMatrix<double, 1u, 5u>& v1,
    const unsigned bitdiff
  ) const {
    unsigned N = 5;
    return tcompare (v0, v1, N, bitdiff);
  }
};
#endif // TRACKFITTER_TRACKKALMANFILTER_H
