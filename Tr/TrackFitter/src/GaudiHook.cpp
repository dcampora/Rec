#include "GaudiHook.h"

std::vector<VectorFit::Track, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>> convertToNTrack (
    std::vector<std::vector<std::reference_wrapper<LHCb::FitNode>>>::iterator& itBegin,
    std::vector<std::vector<std::reference_wrapper<LHCb::FitNode>>>::iterator& itEnd,
    VectorFit::GrowingMemManager& memManager,
    const bool& debug = false
    ) {

  if (debug) {
    std::cout << "Converting " << ((int) (itEnd - itBegin)) << " tracks into vectorfit tracks" << std::endl;
  }

  std::vector<VectorFit::Track, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>> ntracks;

  // Generate new tracks
  unsigned i=0;
  std::for_each(itBegin, itEnd, [&] (std::vector<std::reference_wrapper<LHCb::FitNode>>& t) {
    ntracks.push_back(VectorFit::Track(t, i++, memManager));
  });

  return ntracks;
}

void convertBack (
  std::vector<VectorFit::Track , aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>& ntracks,
  std::vector<std::vector<std::reference_wrapper<LHCb::FitNode>>>::iterator& itBegin,
  std::vector<std::vector<std::reference_wrapper<LHCb::FitNode>>>::iterator& itEnd,
  const bool& debug = false
  ) {
  if (debug) {
    std::cout << "Converting back " << ((int) (itEnd - itBegin)) << " tracks" << std::endl;
  }

  unsigned i=0;
  while (itBegin+i != itEnd) {
    std::vector<std::reference_wrapper<LHCb::FitNode>>& track = *(itBegin+i);
    
    assert(track.size() == ntracks[i].m_nodes.size());

    for (unsigned j=0; j<ntracks[i].m_nodes.size(); ++j) {
      if (ntracks[i].m_nodes[j].m_type != LHCb::Node::Outlier) {
        LHCb::FitNode& node = (LHCb::FitNode&) track[j];

        node.update(ntracks[i].m_nodes[j]);
        node.m_totalChi2[LHCb::FitNode::Forward] = LHCb::ChiSquare(ntracks[i].m_nodes[j].getChi2<VectorFit::Op::Forward>(), ntracks[i].m_nodes[j].m_ndof);
        node.m_totalChi2[LHCb::FitNode::Backward] = LHCb::ChiSquare(ntracks[i].m_nodes[j].getChi2<VectorFit::Op::Backward>(), ntracks[i].m_nodes[j].m_ndof_backward);
      }
    }

    ++i;
  }
}

void crosskalman (
    std::vector<std::vector<std::reference_wrapper<LHCb::FitNode>>>::iterator& itBegin,
    std::vector<std::vector<std::reference_wrapper<LHCb::FitNode>>>::iterator& itEnd,
    const bool& vectorised_opt,
    const bool& debug
    ) {
  // Convert all to ntracks
  VectorFit::GrowingMemManager memManager (41);
  std::vector<VectorFit::Track, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>> ntracks =
    convertToNTrack(itBegin, itEnd, memManager, debug);
  // main code // Prepare the memory manager
  long unsigned totalNodes = 0;
  size_t maxNodeLength = 0;

  std::for_each(itBegin, itEnd, [&] (std::vector<std::reference_wrapper<LHCb::FitNode>>& track) {
    totalNodes   += track.size();
    maxNodeLength = std::max(maxNodeLength, track.size());
  });
  const unsigned reservedMem = totalNodes + maxNodeLength * VECTOR_WIDTH;
  const unsigned reservedMemSeq = vectorised_opt ? reservedMem : reservedMem * 2;
  const unsigned reservedMemVec = vectorised_opt ? reservedMem : 1;

  const int numberOfThreads = 1;
  VectorFit::MemManager memseq (41, reservedMemSeq);
  VectorFit::MemManager memvecforward (41, reservedMem);
  VectorFit::MemManager memvecbackward (41, reservedMem);
  VectorFit::MemManager memvecpost (41, reservedMemVec);
  VectorFit::MemManager memvecsmoother (22, reservedMem);

  memseq.reset();
  memvecforward.reset();
  memvecbackward.reset();
  memvecsmoother.reset();
  memvecpost.reset();
  
  std::vector<uint16_t> schedulingMasks {0xFFFF};
  std::vector<std::tuple<uint16_t, uint16_t, uint16_t, std::array<VectorFit::SchItem, VECTOR_WIDTH>>> scheduler;
  const bool vectorised = (ntracks.size() >= VECTOR_WIDTH ? vectorised_opt : false);

#if VECTOR_WIDTH > 1
  // Dumb Simple Static Scheduler
  if(vectorised){
  scheduler = VectorFit::DumbStaticScheduler::generate(ntracks, debug);
  }
#endif

  // Forward fit
  // Initial iterations
  // Iterations until all tracks have info upstream
  std::for_each(ntracks.begin(), ntracks.end(), [&] (VectorFit::Track& ntrack) {
    auto& track = ntrack.m_nodes;
    VectorFit::FitNode& node = track.front();
    node.getFit<VectorFit::Op::Forward>().m_states.setBasePointer(memseq.getNextElement());

    VectorFit::predictInitialise<VectorFit::Op::Forward>(node, ntrack.m_initialForwardCovariance);
    VectorFit::update<VectorFit::Op::Forward>(node);

    // Before m_forwardUpstream
    for (int i=1; i<= (int) ntrack.m_forwardUpstream; ++i) {
    VectorFit::FitNode& node = track[i];
    VectorFit::FitNode& prevnode = track[i-1];
    node.getFit<VectorFit::Op::Forward>().m_states.setBasePointer(memseq.getNextElement());

    VectorFit::predict<VectorFit::Op::Forward, false>(node, prevnode);
    VectorFit::update<VectorFit::Op::Forward>(node);
    }
  });
#if VECTOR_WIDTH > 1
  if (!vectorised) {
#endif
    // Non-vectorised version
    std::for_each(ntracks.begin(), ntracks.end(), [&] (VectorFit::Track& ntrack) {
        auto& track = ntrack.m_nodes;
        // After m_forwardUpstream
        for (int i=ntrack.m_forwardUpstream+1; i< (int) track.size(); ++i) {
        VectorFit::FitNode& node = track[i];
        VectorFit::FitNode& prevnode = track[i-1];
        node.getFit<VectorFit::Op::Forward>().m_states.setBasePointer(memseq.getNextElement());

        VectorFit::predict<VectorFit::Op::Forward, true>(node, prevnode);
        VectorFit::update<VectorFit::Op::Forward>(node);
        }
        });
#if VECTOR_WIDTH > 1
  }else{
    memvecforward.getNewVector();
    std::for_each (scheduler.begin(), scheduler.end(), [&] (decltype(scheduler[0])& s) {
      const uint8_t schInput  = std::get<0>(s);
      const uint8_t schOutput = std::get<1>(s);
      const uint8_t schAction = std::get<2>(s);
      auto& processingTracks  = std::get<3>(s);

      // Feed the data we need in our vector
      const auto& lastVector = memvecforward.getLastVector();
      if (schInput > 0x00) {
        for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
          if (schInput & (1 << i)) {
            VectorFit::State memslot (lastVector + i);
            memslot.m_updatedState.copy(processingTracks[i].prevnode->getState<VectorFit::Op::Forward, VectorFit::Op::Update, VectorFit::Op::StateVector>());
            memslot.m_updatedCovariance.copy(processingTracks[i].prevnode->getState<VectorFit::Op::Forward, VectorFit::Op::Update, VectorFit::Op::Covariance>());
          }
        }
      }

      // Update the pointers requested
      const auto& vector = memvecforward.getNewVector();
      for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
        if (schAction & (1 << i)) {
          processingTracks[i].node->getFit<VectorFit::Op::Forward>().m_states.setBasePointer(vector + i);
        }
      }

      // predict and update
      VectorFit::State last (lastVector);
      VectorFit::State current (vector);

      VectorFit::predict_vec<VectorFit::Op::Forward>::op<VECTOR_WIDTH> (
        processingTracks,
        last.m_updatedState.m_basePointer,
        last.m_updatedCovariance.m_basePointer,
        current.m_predictedState.m_basePointer,
        current.m_predictedCovariance.m_basePointer
      );

      VectorFit::update_vec<VECTOR_WIDTH> (
        processingTracks,
        current.m_predictedState.m_basePointer,
        current.m_predictedCovariance.m_basePointer,
        current.m_updatedState.m_basePointer,
        current.m_updatedCovariance.m_basePointer,
        current.m_chi2
      );

      // Move requested data out
      if (schOutput > 0x0000) {
        for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
          if (schOutput & (1 << i)) {
            auto& item = processingTracks[i];

            VectorFit::State memslot (memseq.getNextElement());
            memslot.m_updatedCovariance.copy(item.node->getState<VectorFit::Op::Forward, VectorFit::Op::Update, VectorFit::Op::Covariance>());
            memslot.m_updatedState.copy(item.node->getState<VectorFit::Op::Forward, VectorFit::Op::Update, VectorFit::Op::StateVector>());
            item.node->getState<VectorFit::Op::Forward, VectorFit::Op::Update, VectorFit::Op::Covariance>().setBasePointer(memslot.m_updatedCovariance);
            item.node->getState<VectorFit::Op::Forward, VectorFit::Op::Update, VectorFit::Op::StateVector>().setBasePointer(memslot.m_updatedState);
          }
        }
      }
    });

    // Last iterations
    std::vector<unsigned> slots (VECTOR_WIDTH);
    std::iota(slots.begin(), slots.end(), 0);
    {
      // Vectorised version
      // Attempt to take the tracks in batches of vector_size
      std::array<VectorFit::SchItem, VECTOR_WIDTH> processingTracks;
      memvecpost.getNewVector();

      auto trackIterator = ntracks.begin();
      while (trackIterator != ntracks.end()) {
        // Grab a track more
        VectorFit::Track& ntrack = *trackIterator;
        auto& track = ntrack.m_nodes;
        const int slot = slots.back();
        slots.pop_back();

        processingTracks[slot] = VectorFit::SchItem (
          &track,
          ntrack.m_nodes.size() - ntrack.m_backwardUpstream - 2,
          ntrack.m_nodes.size() - ntrack.m_backwardUpstream - 1,
          ntrack.m_backwardUpstream,
          ntrack.m_index
        );

        // Copy starting status in an empty slot
        VectorFit::State newslot (memvecpost.getLastVector() + slot);
        newslot.m_updatedState.copy(processingTracks[slot].prevnode->getState<VectorFit::Op::Forward, VectorFit::Op::Update, VectorFit::Op::StateVector>());
        newslot.m_updatedCovariance.copy(processingTracks[slot].prevnode->getState<VectorFit::Op::Forward, VectorFit::Op::Update, VectorFit::Op::Covariance>());

        while (slots.size() == 0) {
          // Start processing the nodes in batches
          // Prepare the storage location
          const auto& lastVector = memvecpost.getLastVector();
          const auto& vector = memvecpost.getNewVector();
          for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
            processingTracks[i].node->getFit<VectorFit::Op::Forward>().m_states.setBasePointer(vector + i);
          }

          // predict and update
          VectorFit::State last (lastVector);
          VectorFit::State current (vector);

          VectorFit::predict_vec<VectorFit::Op::Forward>::op<VECTOR_WIDTH> (
            processingTracks,
            last.m_updatedState.m_basePointer,
            last.m_updatedCovariance.m_basePointer,
            current.m_predictedState.m_basePointer,
            current.m_predictedCovariance.m_basePointer
          );

          VectorFit::update_vec<VECTOR_WIDTH> (
            processingTracks,
            current.m_predictedState.m_basePointer,
            current.m_predictedCovariance.m_basePointer,
            current.m_updatedState.m_basePointer,
            current.m_updatedCovariance.m_basePointer,
            current.m_chi2
          );

          // Update all tracks information
          // and check for finished tracks
          for (unsigned k=0; k<VECTOR_WIDTH; ++k) {
            VectorFit::SchItem& s = processingTracks[k];

            if (s.node + 1 == s.track->end()) {
              // Move data out and update pointers
              VectorFit::State store (memseq.getNextElement());
              store.m_updatedState.copy(s.node->getState<VectorFit::Op::Forward, VectorFit::Op::Update, VectorFit::Op::StateVector>());
              store.m_updatedCovariance.copy(s.node->getState<VectorFit::Op::Forward, VectorFit::Op::Update, VectorFit::Op::Covariance>());
              s.node->getState<VectorFit::Op::Forward, VectorFit::Op::Update, VectorFit::Op::StateVector>().setBasePointer(store.m_updatedState);
              s.node->getState<VectorFit::Op::Forward, VectorFit::Op::Update, VectorFit::Op::Covariance>().setBasePointer(store.m_updatedCovariance);

              // Free slot and remove finished track
              slots.push_back(k);
            } else {
              // Advance to next node
              ++s.prevnode;
              ++s.node;
            }
          };
        }

        trackIterator++;
      }

      // We have a number of resting nodes
      if (slots.size() < VECTOR_WIDTH) {
        // Have an action mask, following our previous design
        uint8_t actionMask = VectorFit::ArrayGen::mask<VECTOR_WIDTH>();
        std::for_each (slots.begin(), slots.end(), [&actionMask] (decltype(slots[0])& i) {
            actionMask &= ~(1 << i);
            });

        // We will finish once all slots are used up
        while (actionMask > 0x00) {
          const auto& lastVector = memvecpost.getLastVector();
          const auto& vector = memvecpost.getNewVector();
          for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
            if (actionMask & (1 << i)) {
              processingTracks[i].node->getFit<VectorFit::Op::Forward>().m_states.setBasePointer(vector + i);
            }
          }

          // predict and update
          VectorFit::State last (lastVector);
          VectorFit::State current (vector);

          VectorFit::predict_vec<VectorFit::Op::Forward>::op<VECTOR_WIDTH> (
              processingTracks,
              last.m_updatedState.m_basePointer,
              last.m_updatedCovariance.m_basePointer,
              current.m_predictedState.m_basePointer,
              current.m_predictedCovariance.m_basePointer
              );

          VectorFit::update_vec<VECTOR_WIDTH> (
              processingTracks,
              current.m_predictedState.m_basePointer,
              current.m_predictedCovariance.m_basePointer,
              current.m_updatedState.m_basePointer,
              current.m_updatedCovariance.m_basePointer,
              current.m_chi2
              );

          // Check for finished tracks
          for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
            if (actionMask & (1 << i)) {
              VectorFit::SchItem& s = processingTracks[i];

              if (s.node + 1 == s.track->end()) {
                actionMask &= ~(1 << i);
              } else {
                ++s.prevnode;
                ++s.node;
              }
            }
          }
        }
      }
    }
  }
#endif


  std::for_each(ntracks.begin(), ntracks.end(), [&] (VectorFit::Track& ntrack) {
      auto& track = ntrack.m_nodes;
      VectorFit::FitNode& node = track.back();
      node.getFit<VectorFit::Op::Backward>().m_states.setBasePointer(memseq.getNextElement());

      VectorFit::predictInitialise<VectorFit::Op::Backward>(node, ntrack.m_initialBackwardCovariance);
      VectorFit::update<VectorFit::Op::Backward>(node);

      // Before m_backwardUpstream
      for (int i=1; i<= (int) ntrack.m_backwardUpstream; ++i) {
      const int element = ntrack.m_nodes.size() - i - 1;
      VectorFit::FitNode& prevnode = track[element + 1];
      VectorFit::FitNode& node = track[element];
      node.getFit<VectorFit::Op::Backward>().m_states.setBasePointer(memseq.getNextElement());

      VectorFit::predict<VectorFit::Op::Backward, false>(node, prevnode);
      VectorFit::update<VectorFit::Op::Backward>(node);
      }
      });



#if VECTOR_WIDTH > 1
  if (!vectorised) {
#endif
    // Non vectorised version
    std::for_each(ntracks.begin(), ntracks.end(), [&] (VectorFit::Track& ntrack) {
        auto& track = ntrack.m_nodes;
        // After m_backwardUpstream
        for (int i=ntrack.m_backwardUpstream+1; i< (int) track.size(); ++i) {
        const int element = ntrack.m_nodes.size() - i - 1;
        VectorFit::FitNode& node = track[element];
        VectorFit::FitNode& prevnode = track[element + 1];
        node.getFit<VectorFit::Op::Backward>().m_states.setBasePointer(memseq.getNextElement());

        VectorFit::predict<VectorFit::Op::Backward, true>(node, prevnode);
        VectorFit::update<VectorFit::Op::Backward>(node);
        }
        });
#if VECTOR_WIDTH > 1
  }
  else {
    // Vectorised version
    memvecbackward.getNewVector();

    // VectorFit::smoother
    VectorFit::State forward (memvecforward.getLastVector());

    std::for_each (scheduler.rbegin(), scheduler.rend(), [&] (decltype(scheduler[0])& s) {
        const uint16_t schOutput = std::get<0>(s);
        const uint16_t schInput  = std::get<1>(s);
        const uint16_t schAction = std::get<2>(s);
        auto& processingTracks  = std::get<3>(s);

        // TODO: Remove this... I still don't know how exactly
        for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
        processingTracks[i].prevnode = processingTracks[i].node + 1;
        }

        // Feed the data we need in our vector
        const auto& lastVector = memvecbackward.getLastVector();
        if (schInput > 0x00) {
        for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
        if (schInput & (1 << i)) {
        VectorFit::State memslot (lastVector + i);
        memslot.m_updatedState.copy(processingTracks[i].prevnode->getState<VectorFit::Op::Backward, VectorFit::Op::Update, VectorFit::Op::StateVector>());
        memslot.m_updatedCovariance.copy(processingTracks[i].prevnode->getState<VectorFit::Op::Backward, VectorFit::Op::Update, VectorFit::Op::Covariance>());
        }
        }
        }

        // VectorFit::update the pointers requested
        const auto& vector = memvecbackward.getNewVector();
        for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
          if (schAction & (1 << i)) {
            processingTracks[i].node->getFit<VectorFit::Op::Backward>().m_states.setBasePointer(vector + i);
          }
        }

        // VectorFit::predict and VectorFit::update
        VectorFit::State last (lastVector);
        VectorFit::State current (vector);

        VectorFit::predict_vec<VectorFit::Op::Backward>::op<VECTOR_WIDTH> (
            processingTracks,
            last.m_updatedState.m_basePointer,
            last.m_updatedCovariance.m_basePointer,
            current.m_predictedState.m_basePointer,
            current.m_predictedCovariance.m_basePointer
            );

        VectorFit::update_vec<VECTOR_WIDTH> (
            processingTracks,
            current.m_predictedState.m_basePointer,
            current.m_predictedCovariance.m_basePointer,
            current.m_updatedState.m_basePointer,
            current.m_updatedCovariance.m_basePointer,
            current.m_chi2
            );

        // Do the VectorFit::smoother *here*
        // This allows for cache benefits,
        // and removes the need of doing swapping to restore
        // the updated states

        // Fetch a new memvecsmoother vector
        const auto& smootherVector = memvecsmoother.getNewVector();
        VectorFit::SmoothState smooth (smootherVector);

        // VectorFit::update pointers
        for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
          if (schAction & (1 << i)) {
            processingTracks[i].node->m_smoothState.setBasePointer(smootherVector + i);
          }
        }

        // VectorFit::smoother
        uint16_t smoother_result = VectorFit::smoother_vec<VECTOR_WIDTH> (
            forward.m_predictedState.m_basePointer,
            forward.m_predictedCovariance.m_basePointer,
            current.m_updatedState.m_basePointer,
            current.m_updatedCovariance.m_basePointer,
            smooth.m_state.m_basePointer,
            smooth.m_covariance.m_basePointer
            );

        // VectorFit::update residuals
        VectorFit::updateResiduals_vec (
            processingTracks,
            smooth.m_state.m_basePointer,
            smooth.m_covariance.m_basePointer,
            smooth.m_residual,
            smooth.m_errResidual
            );

#ifdef DEBUG
        if (VectorFit::smoother_result != VectorFit::ArrayGen::mask<VECTOR_WIDTH>()) {
          std::cout << "VectorFit::smoother errors: ";
          print(VectorFit::smoother_result);
          std::cout << std::endl;
        }
#endif

        // Move to the next vector
        --forward;

        // Move requested data out
        if (schOutput > 0x0000) {
          for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
            if (schOutput & (1 << i)) {
              auto& item = processingTracks[i];

              VectorFit::State memslot (memseq.getNextElement());
              memslot.m_updatedState.copy(item.node->getState<VectorFit::Op::Backward, VectorFit::Op::Update, VectorFit::Op::StateVector>());
              memslot.m_updatedCovariance.copy(item.node->getState<VectorFit::Op::Backward, VectorFit::Op::Update, VectorFit::Op::Covariance>());
              item.node->getState<VectorFit::Op::Backward, VectorFit::Op::Update, VectorFit::Op::StateVector>().setBasePointer(memslot.m_updatedState);
              item.node->getState<VectorFit::Op::Backward, VectorFit::Op::Update, VectorFit::Op::Covariance>().setBasePointer(memslot.m_updatedCovariance);
            }
          }
        }
    });

    // Last iterations
    std::vector<unsigned> slots (VECTOR_WIDTH);
    std::iota(slots.begin(), slots.end(), 0);
    {
      std::array<VectorFit::SchItem, VECTOR_WIDTH> processingTracks;
      memvecpost.getNewVector();
      auto trackIterator = ntracks.begin();
      while (trackIterator != ntracks.end()) {
        VectorFit::Track& ntrack = *trackIterator;
        auto& track = ntrack.m_nodes;
        const int slot = slots.back();
        slots.pop_back();

        processingTracks[slot] = VectorFit::SchItem (
            &track,
            ntrack.m_forwardUpstream + 1,
            ntrack.m_forwardUpstream,
            ntrack.m_backwardUpstream,
            ntrack.m_index
            );

        VectorFit::State newslot (memvecpost.getLastVector() + slot);
        newslot.m_updatedState.copy(processingTracks[slot].prevnode->getState<VectorFit::Op::Backward, VectorFit::Op::Update, VectorFit::Op::StateVector>());
        newslot.m_updatedCovariance.copy(processingTracks[slot].prevnode->getState<VectorFit::Op::Backward, VectorFit::Op::Update, VectorFit::Op::Covariance>());

        while (slots.size() == 0) {
          const auto& lastVector = memvecpost.getLastVector();
          const auto& vector = memvecpost.getNewVector();
          for (int i=0; i< (int) VECTOR_WIDTH; ++i) {
            processingTracks[i].node->getFit<VectorFit::Op::Backward>().m_states.setBasePointer(vector + i);
          }

          VectorFit::State last (lastVector);
          VectorFit::State current (vector);

          VectorFit::predict_vec<VectorFit::Op::Backward>::op<VECTOR_WIDTH> (
              processingTracks,
              last.m_updatedState.m_basePointer,
              last.m_updatedCovariance.m_basePointer,
              current.m_predictedState.m_basePointer,
              current.m_predictedCovariance.m_basePointer
              );

          VectorFit::update_vec<VECTOR_WIDTH> (
              processingTracks,
              current.m_predictedState.m_basePointer,
              current.m_predictedCovariance.m_basePointer,
              current.m_updatedState.m_basePointer,
              current.m_updatedCovariance.m_basePointer,
              current.m_chi2
              );

          for (unsigned k=0; k<VECTOR_WIDTH; ++k) {
            VectorFit::SchItem& s = processingTracks[k];

            if (s.node == s.track->begin()) {
              VectorFit::State store (memseq.getNextElement());
              store.m_updatedState.copy(s.node->getState<VectorFit::Op::Backward, VectorFit::Op::Update, VectorFit::Op::StateVector>());
              store.m_updatedCovariance.copy(s.node->getState<VectorFit::Op::Backward, VectorFit::Op::Update, VectorFit::Op::Covariance>());
              s.node->getState<VectorFit::Op::Backward, VectorFit::Op::Update, VectorFit::Op::StateVector>().setBasePointer(store.m_updatedState);
              s.node->getState<VectorFit::Op::Backward, VectorFit::Op::Update, VectorFit::Op::Covariance>().setBasePointer(store.m_updatedCovariance);
              slots.push_back(k);
            } else {
              --s.prevnode;
              --s.node;
            }
          };
        }

        trackIterator++;
      }

      if (slots.size() < VECTOR_WIDTH) {
        uint8_t actionMask = VectorFit::ArrayGen::mask<VECTOR_WIDTH>();
        std::for_each (slots.begin(), slots.end(), [&actionMask] (decltype(slots[0])& i) {
            actionMask &= ~(1 << i);
            });

        while (actionMask > 0x0000) {
          const auto& lastVector = memvecpost.getLastVector();
          const auto& vector = memvecpost.getNewVector();
          for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
            if (actionMask & (1 << i)) {
              processingTracks[i].node->getFit<VectorFit::Op::Backward>().m_states.setBasePointer(vector + i);
            }
          }

          VectorFit::State last (lastVector);
          VectorFit::State current (vector);

          VectorFit::predict_vec<VectorFit::Op::Backward>::op<VECTOR_WIDTH> (
              processingTracks,
              last.m_updatedState.m_basePointer,
              last.m_updatedCovariance.m_basePointer,
              current.m_predictedState.m_basePointer,
              current.m_predictedCovariance.m_basePointer
              );

          // VectorFit::update
          VectorFit::update_vec<VECTOR_WIDTH> (
              processingTracks,
              current.m_predictedState.m_basePointer,
              current.m_predictedCovariance.m_basePointer,
              current.m_updatedState.m_basePointer,
              current.m_updatedCovariance.m_basePointer,
              current.m_chi2
              );

          for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
            if (actionMask & (1 << i)) {
              VectorFit::SchItem& s = processingTracks[i];

              if (s.node == s.track->begin()) {
                actionMask &= ~(1 << i);
              } else {
                --s.prevnode;
                --s.node;
              }
            }
          }
        }
      }
    }
  }
#endif

  // Calculate the chi2 a posteriori
  std::for_each(ntracks.begin(), ntracks.end(), [&] (VectorFit::Track& ntrack) {
    auto& track = ntrack.m_nodes;
    ntrack.m_ndof = -ntrack.m_parent_nTrackParameters;

    for (int k=0; k< (int) track.size(); ++k) {
      VectorFit::FitNode& node = track[k];
      if (node.m_type == LHCb::Node::HitOnTrack) {
        ++ntrack.m_ndof;
        ntrack.m_forwardFitChi2  += node.m_forwardFit.m_states.m_chi2[0];
      }
      node.getChi2<VectorFit::Op::Forward>() = ntrack.m_forwardFitChi2;
      node.m_ndof = ntrack.m_ndof;
    }

    int temp_ndof = -ntrack.m_parent_nTrackParameters;
    for (int k=0; k< (int) track.size(); ++k) {
      VectorFit::FitNode& node = track[track.size() - k - 1];
      if (node.m_type == LHCb::Node::HitOnTrack) {
        ++temp_ndof;
        ntrack.m_backwardFitChi2  += node.m_backwardFit.m_states.m_chi2[0];
      }
      node.getChi2<VectorFit::Op::Backward>() = ntrack.m_backwardFitChi2;
      node.m_ndof_backward = temp_ndof;
    }
  });

  // Not upstream iterations
  std::for_each(ntracks.begin(), ntracks.end(), [&] (VectorFit::Track& ntrack) {
      for (int j=0; j<= (int) ntrack.m_forwardUpstream; ++j) {
      ntrack.m_nodes[j].m_smoothState.setBasePointer(memvecsmoother.getNextElement());
      VectorFit::smoother<false, true>(ntrack.m_nodes[j]);
      updateResiduals(ntrack.m_nodes[j]);
      }

      for (int j=0; j<=(int) ntrack.m_backwardUpstream; ++j) {
      const unsigned element = ntrack.m_nodes.size() - j - 1;
      ntrack.m_nodes[element].m_smoothState.setBasePointer(memvecsmoother.getNextElement());
      VectorFit::smoother<true, false>(ntrack.m_nodes[element]);
      updateResiduals(ntrack.m_nodes[element]);
      }
      });


#if VECTOR_WIDTH > 1
  if (!vectorised) {
#endif
    std::for_each(ntracks.begin(), ntracks.end(), [&] (VectorFit::Track& ntrack) {
        for (int j = ntrack.m_forwardUpstream + 1; j < (int) (ntrack.m_nodes.size() - ntrack.m_backwardUpstream - 1); ++j) {
        ntrack.m_nodes[j].m_smoothState.setBasePointer(memvecsmoother.getNextElement());
        VectorFit::smoother<true, true>(ntrack.m_nodes[j]);
        VectorFit::updateResiduals(ntrack.m_nodes[j]);
        }
        });
#if VECTOR_WIDTH > 1
  }
#endif

  // Convert back
  convertBack(ntracks, itBegin, itEnd, debug);
}

void crosskalmanOutliers (
  std::vector<std::vector<std::reference_wrapper<LHCb::FitNode>>>::iterator& itBegin,
  std::vector<std::vector<std::reference_wrapper<LHCb::FitNode>>>::iterator& itEnd,
  const bool& debug
) {
  // Convert all to ntracks
  VectorFit::GrowingMemManager memManager (41);
  std::vector<VectorFit::Track, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>> ntracks =
    convertToNTrack(itBegin, itEnd, memManager, debug);

  for (auto& ntrack : ntracks) {
    ntrack.findOutliers();
  }

  // Prepare the memory manager
  long unsigned totalNodes = 0;
  size_t maxNodeLength = 0;

  std::for_each(itBegin, itEnd, [&] (std::vector<std::reference_wrapper<LHCb::FitNode>>& track) {
    totalNodes   += track.size();
    maxNodeLength = std::max(maxNodeLength, track.size());
  });
  const unsigned reservedMem = totalNodes + maxNodeLength * VECTOR_WIDTH;
  const unsigned reservedMemSeq = reservedMem * 2;
  const unsigned reservedMemVec = 1;

  const int numberOfThreads = 1;
  VectorFit::MemManager memseq (41, reservedMemSeq);
  VectorFit::MemManager memvecforward (41, reservedMem);
  VectorFit::MemManager memvecbackward (41, reservedMem);
  VectorFit::MemManager memvecpost (41, reservedMemVec);
  VectorFit::MemManager memvecsmoother (22, reservedMem);

  memseq.reset();
  memvecforward.reset();
  memvecbackward.reset();
  memvecsmoother.reset();
  memvecpost.reset();
  
  std::vector<uint16_t> schedulingMasks {0xFFFF};
  std::vector<std::tuple<uint16_t, uint16_t, uint16_t, std::array<VectorFit::SchItem, VECTOR_WIDTH>>> scheduler;

  // Forward fit
  // Initial iterations
  // Iterations until all tracks have info upstream
  unsigned current_outliers;
  std::for_each(ntracks.begin(), ntracks.end(), [&] (VectorFit::Track& ntrack) {
    assert(ntrack.m_outliers.size() > 0);
    auto& track = ntrack.m_nodes;
    
    // First outlier and first non outlier
    unsigned firstOutlier = ntrack.m_outliers.front();
    unsigned firstNonOutlier = firstOutlier;
    while (std::find(ntrack.m_outliers.begin(), ntrack.m_outliers.end(), firstNonOutlier) != ntrack.m_outliers.end()) {
      ++firstNonOutlier;
    }
    int starting_index = 1;
    // If it is the 0th element, life is easy
    if (firstOutlier == 0) {
      starting_index = firstNonOutlier+1;

      VectorFit::FitNode& node = track[firstNonOutlier];
      node.getFit<VectorFit::Op::Forward>().m_states.setBasePointer(memseq.getNextElement());    
      VectorFit::predictInitialise<VectorFit::Op::Forward>(node, ntrack.m_initialForwardCovariance);
      VectorFit::update<VectorFit::Op::Forward>(node);
    }
    else {
      VectorFit::FitNode& node = track.front();
      node.getFit<VectorFit::Op::Forward>().m_states.setBasePointer(memseq.getNextElement());

      VectorFit::predictInitialise<VectorFit::Op::Forward>(node, ntrack.m_initialForwardCovariance);
      VectorFit::update<VectorFit::Op::Forward>(node);
    }

    if (debug) {
      std::cout << "Forward - First outlier, firstNonOutlier: " << firstOutlier << ", " << firstNonOutlier << std::endl;
    }
    
    // Before m_forwardUpstream
    // current_outliers = (firstOutlier==0) ? firstNonOutlier : 0;
    current_outliers = 0;
    for (int i=starting_index; i<= (int) ntrack.m_forwardUpstream; ++i) {
      VectorFit::FitNode& node = track[i];

      if (node.m_type == LHCb::Node::Outlier) {
        ++current_outliers;
      }
      else {
        if (debug) {
          std::cout << "Forward - i, current_outliers: " << i << ", " << current_outliers << std::endl;
        }

        VectorFit::FitNode& prevnode = track[i-1-current_outliers];
        node.getFit<VectorFit::Op::Forward>().m_states.setBasePointer(memseq.getNextElement());

        VectorFit::predict<VectorFit::Op::Forward, true>(node, prevnode);
        VectorFit::update<VectorFit::Op::Forward>(node);

        current_outliers = 0;
      }
    }
  });

  // Non-vectorised version
  std::for_each(ntracks.begin(), ntracks.end(), [&] (VectorFit::Track& ntrack) {
    auto& track = ntrack.m_nodes;
    // After m_forwardUpstream
    current_outliers = 0;
    for (int i=ntrack.m_forwardUpstream+1; i< (int) track.size(); ++i) {
      VectorFit::FitNode& node = track[i];

      if (node.m_type == LHCb::Node::Outlier) {
        ++current_outliers;
      }
      else {
        VectorFit::FitNode& prevnode = track[i-1-current_outliers];
        node.getFit<VectorFit::Op::Forward>().m_states.setBasePointer(memseq.getNextElement());

        VectorFit::predict<VectorFit::Op::Forward, true>(node, prevnode);
        VectorFit::update<VectorFit::Op::Forward>(node);

        current_outliers = 0;
      }
    }
  });

  std::for_each(ntracks.begin(), ntracks.end(), [&] (VectorFit::Track& ntrack) {
    auto& track = ntrack.m_nodes;
    
    // First outlier and first non outlier
    int firstOutlier = ntrack.m_outliers.back();
    int firstNonOutlier = firstOutlier;
    while (std::find(ntrack.m_outliers.begin(), ntrack.m_outliers.end(), firstNonOutlier) != ntrack.m_outliers.end()) {
      --firstNonOutlier;
    }
    int starting_index = 1;
    // If it is the last element, life is easy
    if (firstOutlier == track.size() - 1) {
      if (firstNonOutlier == -1) {
        std::cout << "WARNING: VectorFit has encountered a track with only outlier hits." << std::endl;
        starting_index = -1;
      }
      else starting_index = track.size() - firstNonOutlier;

      VectorFit::FitNode& node = track[firstNonOutlier];
      node.getFit<VectorFit::Op::Backward>().m_states.setBasePointer(memseq.getNextElement());    
      VectorFit::predictInitialise<VectorFit::Op::Backward>(node, ntrack.m_initialBackwardCovariance);
      VectorFit::update<VectorFit::Op::Backward>(node);
    }
    else {
      VectorFit::FitNode& node = track.back();
      node.getFit<VectorFit::Op::Backward>().m_states.setBasePointer(memseq.getNextElement());

      VectorFit::predictInitialise<VectorFit::Op::Backward>(node, ntrack.m_initialBackwardCovariance);
      VectorFit::update<VectorFit::Op::Backward>(node);
    }

    if (debug) {
      std::cout << "Backward - First outlier, firstNonOutlier: " << firstOutlier << ", " << firstNonOutlier << std::endl;
    }
    
    // Before m_backwardUpstream
    // current_outliers = (firstOutlier==(track.size()-1)) ? (track.size()-firstNonOutlier-1) : 0;
    current_outliers = 0;
    if (starting_index >= 0) {
      for (int i=starting_index; i<= (int) ntrack.m_backwardUpstream; ++i) {
        const int element = ntrack.m_nodes.size() - i - 1;
        VectorFit::FitNode& node = track[element];
        
        if (node.m_type == LHCb::Node::Outlier) {
          ++current_outliers;
        } else {
          if (debug) {
            std::cout << "Backward - element, current_outliers, backwardUpstream, track size: "
              << element << ", " << current_outliers << ", " << ntrack.m_backwardUpstream << ", " << track.size() << std::endl;
          }

          VectorFit::FitNode& prevnode = track[element + 1 + current_outliers];
          node.getFit<VectorFit::Op::Backward>().m_states.setBasePointer(memseq.getNextElement());

          VectorFit::predict<VectorFit::Op::Backward, false>(node, prevnode);
          VectorFit::update<VectorFit::Op::Backward>(node);

          current_outliers = 0;
        }
      }
    }
  });

  // Non vectorised version
  std::for_each(ntracks.begin(), ntracks.end(), [&] (VectorFit::Track& ntrack) {
    auto& track = ntrack.m_nodes;
    // After m_backwardUpstream
    current_outliers = 0;
    for (int i=ntrack.m_backwardUpstream+1; i< (int) track.size(); ++i) {
      const int element = ntrack.m_nodes.size() - i - 1;
      VectorFit::FitNode& node = track[element];
      
      if (node.m_type == LHCb::Node::Outlier) {
        ++current_outliers;
      } else {
        if (debug) {
          std::cout << "Backward main - element, current_outliers, backwardUpstream, track size: "
            << element << ", " << current_outliers << ", " << ntrack.m_backwardUpstream << ", " << track.size() << std::endl
            << " Outliers:";
          
          for (auto& o : ntrack.m_outliers) {
            std::cout << " " << o;
          }

          std::cout << std::endl;
        }

        VectorFit::FitNode& prevnode = track[element + 1 + current_outliers];
        node.getFit<VectorFit::Op::Backward>().m_states.setBasePointer(memseq.getNextElement());

        VectorFit::predict<VectorFit::Op::Backward, true>(node, prevnode);
        VectorFit::update<VectorFit::Op::Backward>(node);

        current_outliers = 0;
      }
    }
  });

  // Calculate the chi2 a posteriori
  std::for_each(ntracks.begin(), ntracks.end(), [&] (VectorFit::Track& ntrack) {
    auto& track = ntrack.m_nodes;
    ntrack.m_ndof = -ntrack.m_parent_nTrackParameters;
    for (int k=0; k< (int) track.size(); ++k) {
      const VectorFit::FitNode& node = track[k];

      if (node.m_type == LHCb::Node::HitOnTrack) {
        ++ntrack.m_ndof;
        ntrack.m_forwardFitChi2  += node.getChi2<VectorFit::Op::Forward>();
        ntrack.m_backwardFitChi2 += node.getChi2<VectorFit::Op::Backward>();
      }
    }
  });

  // Not upstream iterations
  std::for_each(ntracks.begin(), ntracks.end(), [&] (VectorFit::Track& ntrack) {
    for (int j=0; j<= (int) ntrack.m_forwardUpstream; ++j) {
      if (ntrack.m_nodes[j].m_type != LHCb::Node::Outlier) {
        ntrack.m_nodes[j].m_smoothState.setBasePointer(memvecsmoother.getNextElement());
        VectorFit::smoother<false, true>(ntrack.m_nodes[j]);
        updateResiduals(ntrack.m_nodes[j]);
      }
    }

    for (int j=0; j<=(int) ntrack.m_backwardUpstream; ++j) {
      const unsigned element = ntrack.m_nodes.size() - j - 1;
      if (ntrack.m_nodes[element].m_type != LHCb::Node::Outlier) {
        ntrack.m_nodes[element].m_smoothState.setBasePointer(memvecsmoother.getNextElement());
        VectorFit::smoother<true, false>(ntrack.m_nodes[element]);
        updateResiduals(ntrack.m_nodes[element]);
      }
    }
  });

  std::for_each(ntracks.begin(), ntracks.end(), [&] (VectorFit::Track& ntrack) {
    for (int j = ntrack.m_forwardUpstream + 1; j < (int) (ntrack.m_nodes.size() - ntrack.m_backwardUpstream - 1); ++j) {
      if (ntrack.m_nodes[j].m_type != LHCb::Node::Outlier) {
        ntrack.m_nodes[j].m_smoothState.setBasePointer(memvecsmoother.getNextElement());
        VectorFit::smoother<true, true>(ntrack.m_nodes[j]);
        VectorFit::updateResiduals(ntrack.m_nodes[j]);
      }
    }
  });

  // Convert back
  convertBack(ntracks, itBegin, itEnd, debug);
}
