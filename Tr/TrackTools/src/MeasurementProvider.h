#ifndef TRACKTOOLS_MEASUREMENTPROVIDER_H
#define TRACKTOOLS_MEASUREMENTPROVIDER_H

// Include files
// -------------
#include <array>
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/ToolHandle.h"

// from LHCbKernel
#include "Kernel/LHCbID.h"

// from TrackInterfaces
#include "TrackInterfaces/IMeasurementProvider.h"
#include "TrackInterfaces/ISTClusterPosition.h"

// from TrackEvent
#include "Event/Track.h"

class MeasurementProvider : public GaudiTool,
				   virtual public IMeasurementProvider {
public:
  /** standard tool constructor */
  MeasurementProvider( const std::string& type, const std::string& name, const IInterface* parent);

  /** initialize tool */
  StatusCode initialize() override;

  /** finalize tool */
  StatusCode finalize() override;

  /** See interface class */
  StatusCode load( LHCb::Track& track ) const override;

  /** See interface class */
  virtual LHCb::Measurement* measurement( const LHCb::LHCbID& id, bool localY=false) const override;

  /** See interface class */
  virtual LHCb::Measurement* measurement( const LHCb::LHCbID& id, const LHCb::ZTrajectory& ref,
                                          bool localY=false) const override;

  /** See interface class */
  virtual void addToMeasurements( range_of_ids ids,
                                  std::vector<LHCb::Measurement*>& measurements,
                                  const LHCb::ZTrajectory& reftraj) const override;

private:
  // Handles to actual measurement providers
  ToolHandle<IMeasurementProvider> m_veloRProvider =   { "MeasurementProviderT<MeasurementProviderTypes::VeloR>/VeloRMeasurementProvider", this };
  ToolHandle<IMeasurementProvider> m_veloPhiProvider = { "MeasurementProviderT<MeasurementProviderTypes::VeloPhi>/VeloPhiMeasurementProvider", this };
  ToolHandle<IMeasurementProvider> m_vpProvider =      { "MeasurementProviderT<MeasurementProviderTypes::VP>/VPMeasurementProvider", this };
  ToolHandle<IMeasurementProvider> m_ttProvider =      { "MeasurementProviderT<MeasurementProviderTypes::TT>/TTMeasurementProvider", this };
  ToolHandle<IMeasurementProvider> m_utProvider =      { "MeasurementProviderT<MeasurementProviderTypes::UT>/UTMeasurementProvider", this };
  ToolHandle<IMeasurementProvider> m_itProvider =      { "MeasurementProviderT<MeasurementProviderTypes::IT>/ITMeasurementProvider", this };
  ToolHandle<IMeasurementProvider> m_otProvider =      { "OTMeasurementProvider", this };
  ToolHandle<IMeasurementProvider> m_ftProvider =      { "FTMeasurementProvider", this };
  ToolHandle<IMeasurementProvider> m_muonProvider =    { "MuonMeasurementProvider", this };

  bool m_ignoreVelo;    ///< Ignore Velo hits
  bool m_ignoreVP;      ///< Ignore VP hits
  bool m_ignoreTT;      ///< Ignore TT hits
  bool m_ignoreUT;      ///< Ignore UT hits
  bool m_ignoreIT;      ///< Ignore IT hits
  bool m_ignoreOT;      ///< Ignore OT hits
  bool m_ignoreFT;      ///< Ignore FT hits
  bool m_ignoreMuon;    ///< Ignore Muon hits
  bool m_initializeReference; ///< Initialize measurement reference vector with closest state on track

  std::array<const IMeasurementProvider*,LHCb::Measurement::UTLite+1> m_providermap ;
};
#endif // TRACKTOOLS_MEASUREMENTPROVIDER_H
