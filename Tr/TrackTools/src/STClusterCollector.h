#ifndef _STClusterCollector_H
#define _STClusterCollector_H 1

#include <map>
#include <string>

// ST tool base class
#include "Kernel/STToolBase.h"

// LHCbKernel
#include "TrackInterfaces/ISTClusterCollector.h"

/** @class STClusterCollector STClusterCollector.h
 *
 *  Class for collecting STClusters around a track
 *  @author M.Needham
 *  @date   28/3/2009
 */

#include "GaudiKernel/IIncidentListener.h"

#include "Event/STCluster.h"

#include <memory>


namespace LHCb{
  class Track;
  class STChannelID;
}

class ITrackExtrapolator;
class ITrajPoca;
struct ISTChannelIDSelector;
class IMagneticFieldSvc;

class STClusterCollector final : public ST::ToolBase, 
                                 virtual public ISTClusterCollector,
                                 virtual public IIncidentListener  {

 public: 
   
  /// constructor
  STClusterCollector( const std::string& type,
                         const std::string& name,
                         const IInterface* parent );

  /// initialize method
  StatusCode initialize() override;

  /// collect the hits
  virtual StatusCode execute(const LHCb::Track& track,
                             ISTClusterCollector::Hits& outputCont) const override; 


  /** Implement the handle method for the Incident service.
  *  This is used to inform the tool of software incidents.
  *
  *  @param incident The incident identifier
  */
  virtual void handle( const Incident& incident ) override;

  
  typedef std::pair<LHCb::STCluster*, std::shared_ptr<LHCb::Trajectory> >
    STClusterTrajectory;
  typedef std::vector<STClusterTrajectory> STClusterTrajectories;

 
 private:

  void initEvent() const;

  bool select(const LHCb::STChannelID& chan) const;
 
  double m_xTol;
  double m_yTol;
  double m_refZ;
  double m_windowSize; 
  std::string m_dataLocation;
  mutable STClusterTrajectories  m_dataCont;
  mutable bool m_configured = false;
  bool m_ignoreHitsOnTrack;

  std::string m_extrapolatorName;
  ITrackExtrapolator* m_extrapolator = nullptr;

  ISTChannelIDSelector* m_selector = nullptr;
  std::string m_selectorType;
  std::string m_selectorName;

  ITrajPoca * m_trajPoca = nullptr;

  IMagneticFieldSvc *m_magFieldSvc = nullptr;
  bool m_magneticField;
};

#endif // STClusterCollector_H
