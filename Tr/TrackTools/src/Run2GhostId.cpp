// Include files
#include "Kernel/HitPattern.h"
#include "Event/Particle.h"
#include "Event/Track.h"
#include "Event/KalmanFitResult.h"
#include "Event/STCluster.h"
#include "Event/VeloCluster.h"
#include "Event/GhostTrackInfo.h"
#include "TMVA/FlattenDownstream.C"
#include "TMVA/FlattenLong.C"
#include "TMVA/FlattenTtrack.C"
#include "TMVA/FlattenUpstream.C"
#include "TMVA/FlattenVelo.C"

#include <map>
namespace {
  static const std::map<LHCb::Track::Types, std::vector<std::string> > s_varsMap = {
              { LHCb::Track::Velo, { "tracks_OldImplementation_obsVELO",
                                     "tracks_TrackExtraInfo_FitVeloChi2",
                                     "tracks_TrackExtraInfo_FitVeloNDoF",
                                     "tracks_OldImplementation_veloHits",
                                     "tracks_OldImplementation_ttHits",
                                     "tracks_OldImplementation_itHits",
                                     "tracks_OldImplementation_otHits",
                                     "tracks_PT",
                                     "tracks_TRACK_CHI2",
                                     "tracks_TRACK_NDOF",
                                     "tracks_TRACK_CHI2NDOF" } },
              { LHCb::Track::Long, { "tracks_OldImplementation_obsVELO",
                                     "tracks_TrackExtraInfo_FitVeloChi2",
                                     "tracks_TrackExtraInfo_FitVeloNDoF",
                                     "tracks_OldImplementation_obsTT",
                                     "tracks_OldImplementation_TToutlier",
                                     "tracks_OldImplementation_obsIT",
                                     "tracks_OldImplementation_obsOT",
                                     "tracks_OldImplementation_IToutlier",
                                     "tracks_OldImplementation_OTunused",
                                     "tracks_TrackExtraInfo_FitTChi2",
                                     "tracks_TrackExtraInfo_FitTNDoF",
                                     "tracks_OldImplementation_veloHits",
                                     "tracks_OldImplementation_ttHits",
                                     "tracks_OldImplementation_itHits",
                                     "tracks_OldImplementation_otHits",
                                     "tracks_PT",
                                     "tracks_TRACK_CHI2",
                                     "tracks_TRACK_NDOF",
                                     "tracks_TRACK_CHI2NDOF",
                                     "tracks_TrackExtraInfo_NCandCommonHits",
                                     "tracks_TrackExtraInfo_FitMatchChi2" } },
              { LHCb::Track::Upstream, { "tracks_OldImplementation_obsVELO",
                                         "tracks_TrackExtraInfo_FitVeloChi2",
                                         "tracks_TrackExtraInfo_FitVeloNDoF",
                                         "tracks_OldImplementation_obsTT",
                                         "tracks_OldImplementation_TToutlier",
                                         "tracks_OldImplementation_veloHits",
                                         "tracks_OldImplementation_ttHits",
                                         "tracks_OldImplementation_itHits",
                                         "tracks_OldImplementation_otHits",
                                         "tracks_PT",
                                         "tracks_TRACK_CHI2",
                                         "tracks_TRACK_NDOF",
                                         "tracks_TRACK_CHI2NDOF" } },
              { LHCb::Track::Downstream, { "tracks_OldImplementation_obsTT",
                                           "tracks_OldImplementation_TToutlier",
                                           "tracks_OldImplementation_obsIT",
                                           "tracks_OldImplementation_obsOT",
                                           "tracks_OldImplementation_IToutlier",
                                           "tracks_OldImplementation_OTunused",
                                           "tracks_TrackExtraInfo_FitTChi2",
                                           "tracks_TrackExtraInfo_FitTNDoF",
                                           "tracks_OldImplementation_veloHits",
                                           "tracks_OldImplementation_ttHits",
                                           "tracks_OldImplementation_itHits",
                                           "tracks_OldImplementation_otHits",
                                           "tracks_PT",
                                           "tracks_TRACK_CHI2",
                                           "tracks_TRACK_CHI2NDOF" } },
              { LHCb::Track::Ttrack, { "tracks_OldImplementation_obsIT",
                                       "tracks_OldImplementation_obsOT",
                                       "tracks_OldImplementation_IToutlier",
                                       "tracks_OldImplementation_OTunused",
                                       "tracks_TrackExtraInfo_FitTChi2",
                                       "tracks_TrackExtraInfo_FitTNDoF",
                                       "tracks_OldImplementation_veloHits",
                                       "tracks_OldImplementation_ttHits",
                                       "tracks_OldImplementation_itHits",
                                       "tracks_OldImplementation_otHits",
                                       "tracks_PT",
                                       "tracks_TRACK_CHI2",
                                       "tracks_TRACK_NDOF",
                                       "tracks_TRACK_CHI2NDOF" } } };
}

#include "Event/RecSummary.h"
class IClassifierReader {

 public:

   // constructor
   IClassifierReader() : fStatusIsClean( true ) {}
   virtual ~IClassifierReader() = default;

   // return classifier response
   virtual float GetMvaValue( std::vector<float>& inputValues ) const = 0;

   // returns classifier status
   bool IsStatusClean() const { return fStatusIsClean; }

 protected:

   bool fStatusIsClean;
};


namespace VeloGhostProb {
#include "TMVA/TMVA_1_25nsLL_1d_MLP.class.C"
}
namespace LongGhostProb {
#include "TMVA/TMVA_3_25nsLL_1d_MLP.class.C"
}
namespace UpGhostProb {
#include "TMVA/TMVA_4_25nsLL_1d_MLP.class.C"
}
namespace DownGhostProb {
#include "TMVA/TMVA_5_25nsLL_1d_MLP.class.C"
}
namespace TGhostProb {
#include "TMVA/TMVA_6_25nsLL_1d_MLP.class.C"
}


#include "TMath.h"

// local
#include "Run2GhostId.h"

//-----------------------------------------------------------------------------
// Implementation file for class : Run2GhostId
//
// 2015-02-06 : Paul Seyfert
// following an earlier version by Angelo Di Canto
//-----------------------------------------------------------------------------

DECLARE_TOOL_FACTORY( Run2GhostId )

namespace {
 static const int largestChannelIDType = 1+std::max(LHCb::LHCbID::OT,std::max(LHCb::LHCbID::TT,std::max(LHCb::LHCbID::Velo,LHCb::LHCbID::IT)));
 static constexpr int largestTrackTypes = 1+LHCb::Track::Ttrack;}

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Run2GhostId::Run2GhostId( const std::string& type,
                                const std::string& name,
                                const IInterface* parent)
: base_class ( type, name, parent )
{
  //declareProperty( "Expectors" , m_expectorNames=std::vector<std::string>{"UTHitExpectation","FTHitExpectation"} );
  //declareProperty( "Expectors" , m_expectorNames={"TTHitExpectation","ITHitExpectation","OTHitExpectation"} );
  declareProperty( "InDaVinci" , m_DaVinci = false );
  declareInterface<IGhostProbability>(this);
}

//=============================================================================
//

StatusCode Run2GhostId::finalize()
{
  m_vectorsizes.clear();
  m_readers.clear();
  m_flatters.clear();
  return GaudiTool::finalize();
}

StatusCode Run2GhostId::initialize()
{
  if( !GaudiTool::initialize() ) return StatusCode::FAILURE;

  if (largestTrackTypes<=std::max(LHCb::Track::Ttrack,std::max(
     std::max(LHCb::Track::Velo,LHCb::Track::Upstream),
     std::max(LHCb::Track::Ttrack,LHCb::Track::Downstream))))
    return Warning("ARRAY SIZE SET WRONG (largestTrackTypes is smaller than enum LHCb::Track::Types",StatusCode::FAILURE);

  m_otHitCreator = tool<Tf::IOTHitCreator>("Tf::OTHitCreator", "OTHitCreator");
  // m_veloExpectation = tool<IVeloExpectation>("VeloExpectation");

  //for (auto entry : m_expectorNames) {
    //m_Expectations.push_back(tool<IHitExpectation>(entry));
  //}

  m_readers.clear();
  m_readers.resize(  largestTrackTypes  );
  m_readers[LHCb::Track::Velo].reset(     new VeloGhostProb::ReadMLP(variableNames(LHCb::Track::Velo)) );
  m_readers[LHCb::Track::Long].reset(     new LongGhostProb::ReadMLP(variableNames(LHCb::Track::Long)) );
  m_readers[LHCb::Track::Upstream].reset( new UpGhostProb::ReadMLP(variableNames(LHCb::Track::Upstream)) );
  m_readers[LHCb::Track::Downstream].reset( new DownGhostProb::ReadMLP(variableNames(LHCb::Track::Downstream)) );
  m_readers[LHCb::Track::Ttrack].reset(   new TGhostProb::ReadMLP(variableNames(LHCb::Track::Ttrack)) );
  m_flatters.clear();
  m_flatters.resize( largestTrackTypes );
  m_flatters[LHCb::Track::Velo].reset( VeloTable() );
  m_flatters[LHCb::Track::Long].reset( LongTable() );
  m_flatters[LHCb::Track::Upstream].reset(  UpstreamTable() );
  m_flatters[LHCb::Track::Downstream].reset(  DownstreamTable() );
  m_flatters[LHCb::Track::Ttrack].reset(  TtrackTable() );

  m_vectorsizes.clear();
  m_vectorsizes.resize( largestTrackTypes, 0 );
  m_vectorsizes[LHCb::Track::Velo] = 11 + 1;
  m_vectorsizes[LHCb::Track::Long] = 21 + 1;
  m_vectorsizes[LHCb::Track::Upstream] = 13 + 1;
  m_vectorsizes[LHCb::Track::Downstream] = 15 + 1;
  m_vectorsizes[LHCb::Track::Ttrack] = 14 + 1;
  return StatusCode::SUCCESS;
}

StatusCode Run2GhostId::beginEvent() { 
  // countHits() 
  /// only for qmtest
  if (UNLIKELY(m_DaVinci)) {
    const LHCb::RecSummary * rS =
      getIfExists<LHCb::RecSummary>(evtSvc(),LHCb::RecSummaryLocation::Default);
    if ( !rS ) {
      rS = getIfExists<LHCb::RecSummary>(evtSvc(),LHCb::RecSummaryLocation::Default,false);
    }
    if ( !rS ) {
      rS = getIfExists<LHCb::RecSummary>(evtSvc(),"/Event/Turbo/Rec/Summary",false);
    }
    if ( !rS ) {
      return StatusCode::FAILURE;
    }

    m_otHits = rS->info(LHCb::RecSummary::nOTClusters,-1);
    m_veloHits = rS->info(LHCb::RecSummary::nVeloClusters,-1);
    m_itHits = rS->info(LHCb::RecSummary::nITClusters,-1);
    m_ttHits = rS->info(LHCb::RecSummary::nTTClusters,-1);
  } else {
    auto veloCont = getIfExists<LHCb::VeloLiteCluster::VeloLiteClusters>(LHCb::VeloLiteClusterLocation::Default);
    auto ttCont = getIfExists<LHCb::STLiteCluster::STLiteClusters>(LHCb::STLiteClusterLocation::TTClusters);
    auto itCont = getIfExists<LHCb::STLiteCluster::STLiteClusters>(LHCb::STLiteClusterLocation::ITClusters);
    if (veloCont) m_veloHits = veloCont->size();
    if (ttCont) m_ttHits = ttCont->size();;
    if (itCont) m_itHits = itCont->size();;
    m_otHits = m_otHitCreator->hits().size();
    if (!(veloCont&&itCont&&ttCont)) return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;
}

namespace {
  inline std::vector<float> subdetectorhits(const LHCb::Track& aTrack) {
    std::vector<float> returnvalue( largestChannelIDType, 0. );
    for (auto lhcbid : aTrack.lhcbIDs()) {
      if (lhcbid.detectorType()>=returnvalue.size()) {continue;} // may be a hit in a non-tracking detector
      returnvalue[lhcbid.detectorType()] += 1.;
    }
    return returnvalue;
  }
}


//=============================================================================
StatusCode Run2GhostId::execute(LHCb::Track& aTrack) const
{
  auto variables = netInputs(aTrack);
  //float netresponse = m_readers[aTrack.type()]->GetRarity(variables); // TODO rarity would be nice, see https://sft.its.cern.ch/jira/browse/ROOT-7050
  float netresponse = m_readers[aTrack.type()]->GetMvaValue(variables);
  netresponse = m_flatters[aTrack.type()]->value(0.5*(1.-netresponse));
  aTrack.setGhostProbability(netresponse);
  return StatusCode::SUCCESS;
}


std::vector<float> Run2GhostId::netInputs(LHCb::Track& aTrack) const
{
  /// code for debuging:
  ///if (true) {
  ///  int veloHits;
  ///  const LHCb::VPClusters* vpCont = NULL;
  ///  vpCont = getIfExists<LHCb::VPClusters>(LHCb::VPClusterLocation::Default);
  ///  if (vpCont) veloHits = vpCont->size();
  ///  if (veloHits != m_veloHits) return Error("This is very bad!!!",StatusCode::FAILURE,10);
  ///}

  std::vector<float> obsarray = subdetectorhits(aTrack);
  const LHCb::KalmanFitResult* fit = static_cast<const LHCb::KalmanFitResult*>(aTrack.fitResult());

  std::vector<float> variables;
  variables.reserve(m_vectorsizes[aTrack.type()]);
  //if (LHCb::Track::Velo == tracktype || LHCb::Track::Long == tracktype || LHCb::Track::Upstream == tracktype) {
  if (aTrack.hasVelo()) {
    variables.push_back(obsarray[LHCb::LHCbID::Velo] );
    //variables.push_back(m_veloExpectation->nExpected(aTrack));
    variables.push_back(aTrack.info(LHCb::Track::FitVeloChi2, -999));
    variables.push_back(aTrack.info(LHCb::Track::FitVeloNDoF, -999));
  }
  //if (LHCb::Track::Downstream == tracktype || LHCb::Track::Long == tracktype || LHCb::Track::Upstream == tracktype) {
  if (aTrack.hasTT()) { // includes longtracks w/o ut hits
    variables.push_back(obsarray[LHCb::LHCbID::TT] );
    //variables.push_back(m_Expectations[0]->nExpected(aTrack));
    variables.push_back((fit->nMeasurements(LHCb::Measurement::TTLite) - fit->nActiveMeasurements(LHCb::Measurement::TTLite)));
  }
  //if (LHCb::Track::Downstream == tracktype || LHCb::Track::Long == tracktype || LHCb::Track::Ttrack == tracktype) {
  if (aTrack.hasT()) {
    variables.push_back(obsarray[LHCb::LHCbID::IT] );
    variables.push_back(obsarray[LHCb::LHCbID::OT] );
    //variables.push_back(m_Expectations[1]->nExpected(aTrack)); // IT
    //variables.push_back(m_Expectations[2]->nExpected(aTrack)); // OT
    variables.push_back((fit->nMeasurements(LHCb::Measurement::ITLite) - fit->nActiveMeasurements(LHCb::Measurement::ITLite)));
    float nMeas = fit->nMeasurements( LHCb::Measurement::OT );
    float nOTBad = nMeas-nMeas*aTrack.info(25 , 0 );// info 25 : FitFracUsedOTTimes
    variables.push_back(nOTBad);
    variables.push_back(aTrack.info(LHCb::Track::FitTChi2, -999));
    variables.push_back(aTrack.info(LHCb::Track::FitTNDoF, -999));
  }
  variables.push_back(m_veloHits);
  variables.push_back(  m_ttHits);
  variables.push_back(  m_itHits);
  variables.push_back(  m_otHits);
    variables.push_back(aTrack.pt());
  //if (LHCb::Track::Long != aTrack.type() && LHCb::Track::Downstream != aTrack.type()) {
    variables.push_back(aTrack.chi2());
    if (LHCb::Track::Downstream != aTrack.type()) {
      variables.push_back(aTrack.nDoF());
    }
    variables.push_back(aTrack.chi2PerDoF());
  //}

  if (LHCb::Track::Long == aTrack.type()) {
    variables.push_back(aTrack.info(LHCb::Track::NCandCommonHits, -999));
    variables.push_back(aTrack.info(LHCb::Track::FitMatchChi2, -999));
  }
  variables.push_back(1.f);
  return variables;
}

std::vector<std::string> Run2GhostId::variableNames(LHCb::Track::Types type) const {
  return  s_varsMap.at(type);
}
