#pragma once

/**
 * @file swap_if_partitioned.h
 *
 * @author Daniel Campora
 * @date 2016-07-12
 */

/**
 * @brief      Partitions an iterable object in two parts,
 *             according to some criterion established by the caller.
 *             
 *             The iterable object will keep its length (begin and end are const),
 *             but the contents will change. The return value is the point
 *             where the object was partitioned. From begin to partitionEnd, the
 *             original order is kept. Order is not guaranteed from partitionEnd to end.
 *
 * @param[in]  begin            The begin
 * @param[in]  end              The end
 * @param[in]  p                The predicate
 *
 * @tparam     ForwardIterator  
 * @tparam     Predicate  
 *
 * @return     The new partition ending
 */
template <class ForwardIterator, class Predicate>
ForwardIterator swap_if_partitioned (
    const ForwardIterator begin,
    const ForwardIterator end,
    const Predicate p
) {
  auto partitionEnd = begin;
  for (auto it = begin; end != it; ++it) {
    if (p(*it, partitionEnd)) continue;
    if (partitionEnd != it) std::swap(*partitionEnd, *it);
    ++partitionEnd;
  }
  return partitionEnd;
}
