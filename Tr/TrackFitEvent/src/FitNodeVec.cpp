#include "Event/FitNodeVec.h"

namespace VectorFit {

  StateVector::StateVector (const StateVectorAOS& state) {
    m_parameters = state.parameters();
    m_z = state.z();
  }

  FitParameters::FitParameters (const FitNodeAOS& node, const int& direction) {
    m_transportMatrix = (direction==LHCb::FitNode::Forward ? node.transportMatrix() : node.invertTransportMatrix());
  }

  Node::Node (const LHCb::FitNode& node) {
    m_type = node.type();
    m_refVector = node.refVector();
    m_errMeasure = node.errMeasure();
    m_projectionMatrix = node.projectionMatrix();
    m_measurement = (LHCb::Measurement*) &(node.measurement());
  }

  FitNode::FitNode (const FitNodeAOS& node, const unsigned& index)
    : Node(node), m_forwardFit(node, LHCb::FitNode::Forward), m_backwardFit(node, LHCb::FitNode::Backward),
    m_noiseMatrix(node.noiseMatrix()), m_transportVector(node.transportVector()),
    m_refResidual(node.refResidual()),
    m_index(index) {}

  FitNode::FitNode (const FitNodeAOS& node)
    : Node(node), m_forwardFit(node, LHCb::FitNode::Forward), m_backwardFit(node, LHCb::FitNode::Backward),
    m_noiseMatrix(node.noiseMatrix()), m_transportVector(node.transportVector()),
    m_refResidual(node.refResidual()) {}

  Track::Track (
    const std::vector<std::reference_wrapper<LHCb::FitNode>>& tracks,
    const unsigned& trackNumber,
    GrowingMemManager& memManager
  ) : m_index(trackNumber) {
    // Generate the nodes
    int i=0;
    for (const auto& node : tracks) {
      m_nodes.push_back(FitNode(node, i++));
    }

    // Copy the initial forward predicted and backward predicted covariance
    m_initialForwardCovariance  = tracks.front().get().m_predictedState[LHCb::FitNode::Forward].covariance();
    m_initialBackwardCovariance = tracks.back().get().m_predictedState[LHCb::FitNode::Backward].covariance();

    // Copy the initial m_parent_trackParameters
    // FitNodeAOS nctrack = tracks.front().get();
    LHCb::FitNode& n = tracks.front();
    if (n.m_parent != nullptr) {
      m_parent_nTrackParameters = n.m_parent->nTrackParameters();
    } else {
      m_parent_nTrackParameters = 0;
    }

    m_forwardUpstream = 0;
    m_backwardUpstream = tracks.size() - 1;

    bool foundForward = false;
    bool foundBackward = false;
    for (int i=0; i<(int)tracks.size(); ++i) {
      const int reverse_i = tracks.size() - i - 1;

      if (!foundForward && tracks[i].get().type() == LHCb::Node::HitOnTrack) {
        foundForward = true;
        m_forwardUpstream = i;
      }

      if (!foundBackward && tracks[reverse_i].get().type() == LHCb::Node::HitOnTrack) {
        foundBackward = true;
        m_backwardUpstream = i;
      }
    }
  }

  void Track::findOutliers () {
    m_outliers.clear();

    for (unsigned i=0; i<m_nodes.size(); ++i) {
      if (m_nodes[i].m_type == LHCb::Node::Outlier) {
        m_outliers.push_back(i);
      }
    }
  }
}
