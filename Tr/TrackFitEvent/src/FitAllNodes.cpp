// local
#include "Event/FitAllNodes.h"
#include "Event/KalmanFitResult.h"
#include "LHCbMath/Similarity.h"

namespace LHCb {

void BatchFit::initialize (
  std::vector<std::reference_wrapper<LHCb::FitNode>>::iterator allNodesBegin,
  std::vector<std::reference_wrapper<LHCb::FitNode>>::iterator allNodesEnd) {
  
  // Initialize the hasInfoUpstream(prevnode, direction) in both directions
  std::for_each(allNodesBegin, allNodesEnd, [&] (LHCb::FitNode& node)
  {
    node.setInfoUpstream(LHCb::FitNode::Forward);
    node.setInfoUpstream(LHCb::FitNode::Backward);
    node.setParentInformation();
  });
}

void BatchFit::predict(
  std::vector<std::reference_wrapper<LHCb::FitNode>>::iterator allNodesBegin,
  std::vector<std::reference_wrapper<LHCb::FitNode>>::iterator allNodesEnd,
  int direction) {
  // Discard the ones that don't require refitting
  auto nodesEnd = std::remove_if(allNodesBegin, allNodesEnd, [&direction] (LHCb::FitNode& node)
  {
    return not node.requirePredictedState(direction);
  });

  // Do the actual fit
  std::for_each(allNodesBegin, nodesEnd, [&direction] (LHCb::FitNode& node)
  {
    node.stagedComputePredict(direction);
  });
}

void BatchFit::filter(
  std::vector<std::reference_wrapper<LHCb::FitNode>>::iterator allNodesBegin,
  std::vector<std::reference_wrapper<LHCb::FitNode>>::iterator allNodesEnd,
  int direction) {
  // Discard the ones that don't require fitting
  auto nodesEnd = std::remove_if(allNodesBegin, allNodesEnd, [&direction] (LHCb::FitNode& node) {
    return not node.requireFilteredState(direction);
  });
  
  // Update predicted with filtered
  std::for_each(allNodesBegin, nodesEnd, [&direction] (LHCb::FitNode& node) {
    node.updatePredictedWithFiltered(direction);
  });
  
  // Reorder with all the nodes that require filtering first and filter them
  auto filterNodesEnd = swap_if(allNodesBegin, nodesEnd, [&direction] (LHCb::FitNode& node) {
    return not node.requireFilter();
  });
  std::for_each(allNodesBegin, filterNodesEnd, [&direction] (LHCb::FitNode& node) {
    node.stagedComputeFilter(direction);
  });

  // Post fit for all
  std::for_each(allNodesBegin, nodesEnd, [&direction] (LHCb::FitNode& node) {
    node.stagedComputeFilterPost(direction);
  });
}

void BatchFit::smoother(
  std::vector<std::reference_wrapper<LHCb::FitNode>>::iterator allNodesBegin,
  std::vector<std::reference_wrapper<LHCb::FitNode>>::iterator allNodesEnd) {

  std::for_each(allNodesBegin, allNodesEnd, [] (LHCb::FitNode& node) {
    node.stagedComputeBiSmooth();
  });
}

}
