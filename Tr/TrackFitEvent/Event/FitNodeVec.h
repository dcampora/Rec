#ifndef TRACKFITEVENT_FITNODEVEC_H
#define TRACKFITEVENT_FITNODEVEC_H 1


#include <vector>
#include <functional>
#include "Event/StoreVec.h"
//#include "Cpp14Compat.h"
#include "Event/VectorConfiguration.h"
#include "Event/MatrixTypeVec.h"
#include "Event/FitNodeVecAux.h"
// Include files
#include "Event/State.h"
#include "Event/Node.h"
#include "Event/FitNode.h"
#include "Event/Measurement.h"
#include "Event/TrackParameters.h"
#include "Event/TrackTypes.h"
#include "LHCbMath/ValueWithError.h"
#include "GaudiKernel/VectorMap.h"
#include "Math/SMatrix.h"
 // from TrackEvent
#include "Event/Node.h"
#include "Event/Measurement.h"
#include "Event/ChiSquare.h"
#include "LHCbMath/ValueWithError.h"
#include "GaudiKernel/boost_allocator.h"
// From LHCbMath
#include "LHCbMath/MatrixManip.h"



// Names for templates
namespace VectorFit {

  namespace Op {
    class Backward;
    class Forward;
    class Predict;
    class Update;
    class Smooth;
    class StateVector;
    class Covariance;
  }

  struct SmoothState {
    PRECISION* m_basePointer = 0x0;
    TrackVector m_state;
    TrackSymMatrix m_covariance;
    PRECISION* m_residual;
    PRECISION* m_errResidual;

    SmoothState () = default;
    SmoothState (PRECISION* m_basePointer) {
      setBasePointer(m_basePointer);
    }

    void setBasePointer (PRECISION* p) {
      m_basePointer = p;
      m_state = TrackVector(p); p += 5 * VECTOR_WIDTH;
      m_covariance = TrackSymMatrix(p); p += 15 * VECTOR_WIDTH;
      m_residual = p; p += VECTOR_WIDTH;
      m_errResidual = p;
    }

    void setBasePointer (const SmoothState& s) {
      setBasePointer(s.m_basePointer);
    }

    void copy (const SmoothState& s) {
      m_state.copy(s.m_state);
      m_covariance.copy(s.m_covariance);
      *m_residual = *s.m_residual;
      *m_errResidual = *s.m_errResidual;
    }
  };

  /**
   * @brief      Our flamant AOSOA
   */
  struct State {
    PRECISION* m_basePointer = 0x0;
    TrackVector m_predictedState;
    TrackVector m_updatedState;
    TrackSymMatrix m_predictedCovariance;
    TrackSymMatrix m_updatedCovariance;
    PRECISION* m_chi2;

    State () = default;
    State (PRECISION* m_basePointer) {
      setBasePointer(m_basePointer);
    }

    void setBasePointer (PRECISION* p) {
      m_basePointer = p;
      m_predictedState = TrackVector(p); p += 5 * VECTOR_WIDTH;
      m_updatedState   = TrackVector(p); p += 5 * VECTOR_WIDTH;
      m_predictedCovariance = TrackSymMatrix(p); p += 15 * VECTOR_WIDTH;
      m_updatedCovariance   = TrackSymMatrix(p); p += 15 * VECTOR_WIDTH;
      m_chi2 = p;
    }

    void setBasePointer (const State& s) {
      setBasePointer(s.m_basePointer);
    }

    void copy (const State& s) {
      m_predictedState.copy(s.m_predictedState);
      m_updatedState.copy(s.m_updatedState);
      m_predictedCovariance.copy(s.m_predictedCovariance);
      m_updatedCovariance.copy(s.m_updatedCovariance);
      *m_chi2 = *(s.m_chi2);
    }

    State operator++ () {
      setBasePointer(m_basePointer + 41 * VECTOR_WIDTH);
      return *this;
    }

    State operator-- () {
      setBasePointer(m_basePointer - 41 * VECTOR_WIDTH);
      return *this;
    }
  };

  struct StateVector {
    _aligned TrackVectorContiguous m_parameters;
    PRECISION m_z ;

    StateVector () = default;
    StateVector (const StateVectorAOS& state);
  };

  struct FitParameters {
    _aligned TrackMatrixContiguous m_transportMatrix;
    State m_states;

    FitParameters (const FitParameters& node) = default;
    FitParameters (const FitNodeAOS& node, const int& direction);
  };

  struct Node {
    _aligned TrackVectorContiguous m_projectionMatrix;
    LHCb::Node::Type m_type;
    StateVector m_refVector;
    PRECISION m_errMeasure;
    LHCb::Measurement* m_measurement;

    Node () = default;
    Node (const Node& node) = default;
    Node (const FitNodeAOS& node);
  };

  struct FitNode : public Node {
    FitParameters m_forwardFit;
    FitParameters m_backwardFit;
    _aligned TrackSymMatrixContiguous m_noiseMatrix;
    _aligned TrackVectorContiguous m_transportVector;
    PRECISION m_refResidual;
    SmoothState m_smoothState;
    unsigned m_index;
    int m_ndof = 0;
    int m_ndof_backward = 0;

    FitNode () = default;
    FitNode (const FitNode& node) = default;
    FitNode (const FitNodeAOS& node);
    FitNode (const FitNodeAOS& node, const unsigned& index);

    template<class R, class S, class T,
      class U = typename std::conditional<std::is_same<T, Op::Covariance>::value, TrackSymMatrix, TrackVector>::type>
        inline const U& getState () const;
    template<class R, class S, class T,
      class U = typename std::conditional<std::is_same<T, Op::Covariance>::value, TrackSymMatrix, TrackVector>::type>
        inline U& getState ();

    template<class R,
      class U = typename std::conditional<std::is_same<R, Op::Covariance>::value, TrackSymMatrix, TrackVector>::type>
        const U& getSmooth () const;
    template<class R,
      class U = typename std::conditional<std::is_same<R, Op::Covariance>::value, TrackSymMatrix, TrackVector>::type>
        U& getSmooth ();

    template<class R>
      inline const FitParameters& getFit () const;
    template<class R>
      inline FitParameters& getFit ();

    template<class R>
      inline const PRECISION& getChi2 () const {
        return *(getFit<R>().m_states.m_chi2);
      }
    template<class R>
      inline PRECISION& getChi2 () {
        return *(getFit<R>().m_states.m_chi2);
      }
  };

  struct Track {
    std::vector<VectorFit::FitNode, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>> m_nodes;

    std::vector<unsigned> m_outliers;

    // Active measurements from this node
    unsigned m_forwardUpstream;
    unsigned m_backwardUpstream;

    // Chi square of track
    PRECISION m_forwardFitChi2 = 0.0;
    PRECISION m_backwardFitChi2 = 0.0;

    // Some other things
    unsigned m_index;
    int m_ndof = 0;
    int m_parent_nTrackParameters;

    // Initial covariances
    TrackSymMatrixContiguous m_initialForwardCovariance;
    TrackSymMatrixContiguous m_initialBackwardCovariance;

    Track () = default;
    Track (const Track& track) = default;
    Track (
      const std::vector<std::reference_wrapper<LHCb::FitNode>>& track,
      const unsigned& trackNumber,
      GrowingMemManager& memManager
    );
    Track (
      const std::vector<std::reference_wrapper<LHCb::FitNode>>& track,
      const unsigned& trackNumber,
      GrowingMemManager& memManager,
      const bool& dummy
    );

    void findOutliers ();
  };

}

#include "FitNodeVecGetters.h"


#endif
