#ifndef TRACKFITEVENT_FITNODEVECGETTERS_H
#define TRACKFITEVENT_FITNODEVECGETTERS_H 1


namespace VectorFit {

  // ---------------
  // get methods
  // ---------------

  template<>
    inline const FitParameters& VectorFit::FitNode::getFit<Op::Backward> () const {
      return m_backwardFit;
    }

  template<>
    inline const FitParameters& VectorFit::FitNode::getFit<Op::Forward> () const {
      return m_forwardFit;
    }

  template<>
    inline const TrackVector& VectorFit::FitNode::getState<Op::Forward, Op::Predict, Op::StateVector> () const {
      return m_forwardFit.m_states.m_predictedState;
    }

  template<>
    inline const TrackSymMatrix& VectorFit::FitNode::getState<Op::Forward, Op::Predict, Op::Covariance> () const {
      return m_forwardFit.m_states.m_predictedCovariance;
    }

  template<>
    inline const TrackVector& VectorFit::FitNode::getState<Op::Forward, Op::Update, Op::StateVector> () const {
      return m_forwardFit.m_states.m_updatedState;
    }

  template<>
    inline const TrackSymMatrix& VectorFit::FitNode::getState<Op::Forward, Op::Update, Op::Covariance> () const {
      return m_forwardFit.m_states.m_updatedCovariance;
    }

  template<>
    inline const TrackVector& VectorFit::FitNode::getState<Op::Backward, Op::Predict, Op::StateVector> () const {
      return m_backwardFit.m_states.m_predictedState;
    }

  template<>
    inline const TrackSymMatrix& VectorFit::FitNode::getState<Op::Backward, Op::Predict, Op::Covariance> () const {
      return m_backwardFit.m_states.m_predictedCovariance;
    }

  template<>
    inline const TrackVector& VectorFit::FitNode::getState<Op::Backward, Op::Update, Op::StateVector> () const {
      return m_backwardFit.m_states.m_updatedState;
    }

  template<>
    inline const TrackSymMatrix& VectorFit::FitNode::getState<Op::Backward, Op::Update, Op::Covariance> () const {
      return m_backwardFit.m_states.m_updatedCovariance;
    }

  template<>
    inline const TrackVector& VectorFit::FitNode::getSmooth<Op::StateVector> () const {
      return m_smoothState.m_state;
    }

  template<>
    inline const TrackSymMatrix& VectorFit::FitNode::getSmooth<Op::Covariance> () const {
      return m_smoothState.m_covariance;
    }

  // -------------------------
  // ref getters for the above
  // -------------------------

  template<>
    inline FitParameters& VectorFit::FitNode::getFit<Op::Forward> () {
      return m_forwardFit;
    }

  template<>
    inline FitParameters& VectorFit::FitNode::getFit<Op::Backward> () {
      return m_backwardFit;
    }

  template<>
    inline TrackVector& VectorFit::FitNode::getState<Op::Forward, Op::Predict, Op::StateVector> () {
      return m_forwardFit.m_states.m_predictedState;
    }

  template<>
    inline TrackSymMatrix& VectorFit::FitNode::getState<Op::Forward, Op::Predict, Op::Covariance> () {
      return m_forwardFit.m_states.m_predictedCovariance;
    }

  template<>
    inline TrackVector& VectorFit::FitNode::getState<Op::Forward, Op::Update, Op::StateVector> () {
      return m_forwardFit.m_states.m_updatedState;
    }

  template<>
    inline TrackSymMatrix& VectorFit::FitNode::getState<Op::Forward, Op::Update, Op::Covariance> () {
      return m_forwardFit.m_states.m_updatedCovariance;
    }

  template<>
    inline TrackVector& VectorFit::FitNode::getState<Op::Backward, Op::Predict, Op::StateVector> () {
      return m_backwardFit.m_states.m_predictedState;
    }

  template<>
    inline TrackSymMatrix& VectorFit::FitNode::getState<Op::Backward, Op::Predict, Op::Covariance> () {
      return m_backwardFit.m_states.m_predictedCovariance;
    }

  template<>
    inline TrackVector& VectorFit::FitNode::getState<Op::Backward, Op::Update, Op::StateVector> () {
      return m_backwardFit.m_states.m_updatedState;
    }

  template<>
    inline TrackSymMatrix& VectorFit::FitNode::getState<Op::Backward, Op::Update, Op::Covariance> () {
      return m_backwardFit.m_states.m_updatedCovariance;
    }

  template<>
    inline TrackVector& VectorFit::FitNode::getSmooth<Op::StateVector> () {
      return m_smoothState.m_state;
    }

  template<>
    inline TrackSymMatrix& VectorFit::FitNode::getSmooth<Op::Covariance> () {
      return m_smoothState.m_covariance;
    }

}

#endif
