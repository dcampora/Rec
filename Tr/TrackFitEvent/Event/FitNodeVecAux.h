#ifndef TRACKFITEVENT_FITNODEVECAUX_H
#define TRACKFITEVENT_FITNODEVECAUX_H 1

#ifdef SP
#define PRECISION float
#else
#define PRECISION double
#endif
// from TrackEvent
#include "Event/KalmanFitResult.h"
#include "Event/FitNode.h"
#include "Event/StateVector.h"


// Oldfit types, just to do the conversion to the new types

typedef LHCb::FitNode FitNodeAOS;
typedef Gaudi::TrackVector TrackVectorAOS;
typedef Gaudi::TrackMatrix TrackMatrixAOS;
typedef LHCb::StateVector StateVectorAOS;

// Some align definitions
#define _aligned alignas(sizeof(PRECISION)*VECTOR_WIDTH)
#ifdef __INTEL_COMPILER
#define _type_aligned __attribute__((align_value(sizeof(PRECISION)*VECTOR_WIDTH)))
#else
#define _type_aligned __attribute__((aligned(sizeof(PRECISION)*VECTOR_WIDTH)))
#endif

typedef _type_aligned PRECISION * const __restrict__ fp_ptr_64;
typedef _type_aligned const PRECISION * const __restrict__ fp_ptr_64_const;

#endif
