/**
 * @brief      Class to process all fits horizontally in parallel
 */

#ifndef TRACKFITEVENT_FITALLNODES_H
#define TRACKFITEVENT_FITALLNODES_H 1

#include "Event/FitNode.h"

namespace LHCb
{

// Swaps with the last items if the predicate is true
template <class ForwardIterator, class UnaryPredicate>
ForwardIterator swap_if (ForwardIterator first, ForwardIterator last, UnaryPredicate pred)
{
  while (first!=last) {
    if (pred(*first)) std::swap(*first, *(--last));
    else              ++first;
  }
  return last;
}

class BatchFit {
public:
  static void initialize (
    std::vector<std::reference_wrapper<LHCb::FitNode>>::iterator allNodesBegin,
    std::vector<std::reference_wrapper<LHCb::FitNode>>::iterator allNodesEnd
  );

  static void predict (
    std::vector<std::reference_wrapper<LHCb::FitNode>>::iterator allNodesBegin,
    std::vector<std::reference_wrapper<LHCb::FitNode>>::iterator allNodesEnd,
    int direction
  );

  static void filter (
    std::vector<std::reference_wrapper<LHCb::FitNode>>::iterator allNodesBegin,
    std::vector<std::reference_wrapper<LHCb::FitNode>>::iterator allNodesEnd,
    int direction
  );

  static void smoother (
    std::vector<std::reference_wrapper<LHCb::FitNode>>::iterator allNodesBegin,
    std::vector<std::reference_wrapper<LHCb::FitNode>>::iterator allNodesEnd
  );
};

}

#endif // TRACKFITEVENT_FITALLNODES_H
