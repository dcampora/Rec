/**
 *      Timer
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      December, 2013
 *      CERN
 */

#ifndef CUSTOM_TIMER 
#define CUSTOM_TIMER 1

#include <ctime>

#define GET_TIME(__TIMER__, __FOO__) __TIMER__.start(); __FOO__; __TIMER__.stop()

namespace LHCb {

class Timer {
public:
    bool _append;
    long long int timeDiff;
    struct timespec tstart, tend; 

    Timer();
    
    void start();
    void stop();
    void flush();
    
    /**
     * Gets the elapsed time since tstart
     * @return
     */
    float getElapsedTime();
    long long int getElapsedTimeLong();
    
    /**
     * Gets the accumulated time in timeDiff
     * @return
     */
    float get();
    long long int getLong();
};

}

#endif
