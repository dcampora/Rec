// Include files 

#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/ToStream.h"

#include "Event/ODIN.h"
#include "Event/Track.h"
#include "Event/State.h"
#include "Event/TrackTypes.h"
#include "Event/VeloCluster.h"
#include "Event/VeloLiteCluster.h"
#include "Event/VeloPhiMeasurement.h"
#include "Event/VeloRMeasurement.h"

#include "VeloDet/DeVelo.h"

#include "Kernel/LHCbID.h"

#include "VeloCCEScanMonitor.h"

namespace {
//========================================================
// Linear extrapolator from a track to a given z position.
//========================================================
Gaudi::XYZPoint extrapolateToZ(const LHCb::Track& track, double toZ)
{
    // get state parameters
    const LHCb::State state = track.closestState(toZ);
    Gaudi::XYZPoint coord = state.position();
    Gaudi::XYZVector slope = state.slopes();
    double deltaZ=toZ-coord.z();
    return { coord.x()+slope.x()*deltaZ, coord.y()+slope.y()*deltaZ, toZ };
}
}

typedef IVeloClusterPosition::Direction Direction;

DECLARE_NAMESPACE_ALGORITHM_FACTORY(Velo, VeloCCEScanMonitor )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Velo::VeloCCEScanMonitor::VeloCCEScanMonitor(const std::string& name,
          ISvcLocator* pSvcLocator ) :
      GaudiTupleAlg( name , pSvcLocator ),
      m_recordLoc(LHCb::VeloTELL1DataLocation::CMSuppressedADCs)
{
    declareProperty( "TracksInContainer", m_tracksInContainer );
    declareProperty( "CCEscan", m_CCEscan ) ;
    declareProperty( "TestSensor", m_testsensor );
    declareProperty( "NumstripsCCE", m_numstripsCCE );
    declareProperty( "NumstripsCFE", m_numstripsCFE );
    declareProperty( "KillSensorList", m_killSensorList ); 
    declareProperty( "ClusThres", m_ClusThres );
}
//=============================================================================
// Destructor
//=============================================================================
Velo::VeloCCEScanMonitor::~VeloCCEScanMonitor() {} 

//=============================================================================
// Initialization. Check parameters
//=============================================================================
StatusCode Velo::VeloCCEScanMonitor::initialize()
{
    // Mandatory initialization of GaudiAlgorithm
    StatusCode sc = GaudiTupleAlg::initialize();
    if ( sc.isFailure() ) { return sc; }

    m_clusterTool = tool<IVeloClusterPosition>("VeloClusterPosition");
    m_veloDet = getDet<DeVelo>(  DeVeloLocation::Default ) ;

    m_linExtrapolator = tool<ITrackExtrapolator>("TrackLinearExtrapolator",this);
    if(!m_linExtrapolator) {
        err() << "Unable to retrieve the TrackLinearExtrapolator" << endmsg;
        return  StatusCode::FAILURE;
    }

    m_cceTool = tool<IVeloCCEConfigTool>("VeloCCEConfigTool");
    if(!m_cceTool) {
        err() << "Unable to retrieve the VeloCCEConfigTool" << endmsg;
        return  StatusCode::FAILURE;
    }

    return StatusCode::SUCCESS;
}

//=============================================================================
// Execute
//=============================================================================
StatusCode Velo::VeloCCEScanMonitor::execute()
{

    ++m_evt;
    // get the input data
    m_tracks = get<LHCb::Tracks>(m_tracksInContainer);
    // For finding clusters:
    if(!exist<LHCb::VeloClusters>(LHCb::VeloClusterLocation::Default)){
        return Error( " ==> There is no VeloClusters in TES " );
    }else{
        m_rawClusters=get<LHCb::VeloClusters>(LHCb::VeloClusterLocation::Default);
    }

    if(!exist<LHCb::VeloTELL1Datas>(m_recordLoc)){
        error()<<"There is no recorded ADC samples in TES!"
            <<"at location: LHCb::VeloTell1DataLocation::CMSuppressedADCs"
            <<endmsg;
        return ( StatusCode::FAILURE );
    }else{
        m_recorded=get<LHCb::VeloTELL1Datas>(m_recordLoc);
    }

    //////////// loop over tracks if collision
    bool fillTuple = 0;
    
    if ( m_CCEscan ){ 
       // Check for ODIN banks:
       LHCb::ODIN *odin = getIfExists<LHCb::ODIN>(LHCb::ODINLocation::Default);
       if ( odin ){
          m_CCEstep = odin->calibrationStep();
          if ( m_CCEstep>=0 ){
	     if (m_killSensorList.empty())
	     {
                m_Voltage = m_cceTool->findKilledSensors(m_CCEstep,m_killSensorListVec);
	    	for (unsigned int i=0; i<m_killSensorListVec.size();++i)
	    	{
                   if((m_testsensor==(unsigned)m_killSensorListVec[i]) || 
                      (m_testsensor==(unsigned)m_killSensorListVec[i]+64)) fillTuple=1;
            	}
             }
	     else
	     {
                std::stringstream ss(m_killSensorList);
                while( ss.good() )
                {
                   std::string substr;
                   getline(ss, substr, ',');
                   m_killSensorListVec.push_back(std::stoi(substr));
                }
	    	for (unsigned int i=0; i<m_killSensorListVec.size();++i)
	    	{
                   if((m_testsensor==(unsigned)m_killSensorListVec[i]) || 
                      (m_testsensor==(unsigned)m_killSensorListVec[i]+64)) fillTuple=1;
            	}
            	m_Voltage = m_cceTool->findKilledSensors(m_CCEstep,m_killSensorListVec);
	     }
          }
          else {
             fatal() << "There is no ODIN bank" << endmsg;
             return StatusCode::FAILURE;
          }
       }
    }
    else {
       fillTuple = 1;
    }
    for (LHCb::Tracks::const_iterator iterT = m_tracks->begin(); 
         iterT != m_tracks->end(); ++iterT){
        if(((*iterT)->checkType(LHCb::Track::Velo) == true) || 
           ((*iterT)->checkType(LHCb::Track::Long) == true)){
            if (fillTuple==1) fillCFEtuple(**iterT);
        }
    }

    return StatusCode::SUCCESS;
}

//=============================================================================

void Velo::VeloCCEScanMonitor::fillCFEtuple(const LHCb::Track& track)   
{
    Tuple tuple=nTuple("VeloNtuple", "Clusters",CLID_ColumnWiseTuple );
    double Pt = -1;
    double P = -1;
    if (track.type()==3){ 
        Pt = track.pt();
        P = track.p();
    } 

    ///Reguire hits on 2 modules either side of test (8 hits on 8 sensors) ///
    // Make cluster used later:
    LHCb::VeloCluster* currentCluster = new LHCb::VeloCluster();
    int largestADC = 0;
    int clusterADC = -1;
    double clusterRadiusL = -1;
    double clusterRadiusG = -1;
    unsigned int mainStrip = 999999;

    int SENSHITS = 0,SENSHITS2 = 0,SENSHITS3 = 0,SENSHITS4 = 0,SENSHITS5 = 0;
    std::vector<int> SensCheck,SensCheck2,SensCheck3,SensCheck4,SensCheck5;
    std::vector<int> SensTrackHits;
    const DeVeloSensor* sensor = m_veloDet->sensor(m_testsensor);

    ///////// 1in5
    SensCheck.push_back(m_testsensor+2); SensCheck.push_back(m_testsensor+4); 
    SensCheck.push_back(m_testsensor+6); SensCheck.push_back(m_testsensor+8);
    SensCheck2.push_back(m_testsensor+2); SensCheck2.push_back(m_testsensor+4); 
    SensCheck2.push_back(m_testsensor+6); SensCheck2.push_back(m_testsensor-2);
    SensCheck3.push_back(m_testsensor+2); SensCheck3.push_back(m_testsensor+4); 
    SensCheck3.push_back(m_testsensor-2); SensCheck3.push_back(m_testsensor-4);
    SensCheck4.push_back(m_testsensor+2); SensCheck4.push_back(m_testsensor-2); 
    SensCheck4.push_back(m_testsensor-4); SensCheck4.push_back(m_testsensor-6);
    SensCheck5.push_back(m_testsensor-2); SensCheck5.push_back(m_testsensor-4); 
    SensCheck5.push_back(m_testsensor-6); SensCheck5.push_back(m_testsensor-8);

    const LHCb::Track::LHCbIDContainer& lhcbids = track.lhcbIDs() ;
    for ( LHCb::Track::LHCbIDContainer::const_iterator it = lhcbids.begin() ; 
            it != lhcbids.end(); ++it ) {
        LHCb::LHCbID id = (*it);
        if (id.isVelo()!=1) continue;
        LHCb::VeloChannelID vcID = id.veloID();
        unsigned int sensorID = vcID.sensor(); 
        SensTrackHits.push_back(sensorID);
    }

    for(unsigned int j = 0; j!=SensCheck.size();j++){
        int SenCh = SensCheck[j];
        int SenCh2 = SensCheck2[j];
        int SenCh3 = SensCheck3[j];
        int SenCh4 = SensCheck4[j];
        int SenCh5 = SensCheck5[j];
        for(unsigned int jj = 0; jj!=SensTrackHits.size();jj++){
            int SenTrackHit = SensTrackHits[jj];
            if(SenTrackHit==SenCh)SENSHITS++;
            if(SenTrackHit==SenCh2)SENSHITS2++;
            if(SenTrackHit==SenCh3)SENSHITS3++;
            if(SenTrackHit==SenCh4)SENSHITS4++;
            if(SenTrackHit==SenCh5)SENSHITS5++;
        }
    }
    SensTrackHits.clear();
    SensCheck.clear();
    SensCheck2.clear();
    SensCheck3.clear();
    SensCheck4.clear();
    SensCheck5.clear();

    if(SENSHITS>3||SENSHITS2>3||SENSHITS3>3||SENSHITS4>3||SENSHITS5>3){

        /// Look at charge collection on track intercept with test sensor ///

        // Which set of conditions were passed?:
        bool isSet1 = 0;
        bool isSet2 = 0;
        bool isSet3 = 0;
        bool isSet4 = 0;
        bool isSet5 = 0;
        if (SENSHITS>3) isSet1=1;
        if (SENSHITS2>3) isSet2=1;
        if (SENSHITS3>3) isSet3=1;
        if (SENSHITS4>3) isSet4=1;
        if (SENSHITS5>3) isSet5=1;

        const Gaudi::XYZPoint trInterceptGlob= extrapolateToZ(track, sensor->z());
        // ITrackExtrapolator to get error on extrapolation:
        Gaudi::XYZPoint testSensorTrackPos;
        Gaudi::SymMatrix3x3 trackErrorOnPos;
        StatusCode scextrap = m_linExtrapolator->position (track, sensor->z(), 
			      testSensorTrackPos, trackErrorOnPos);

        LHCb::VeloChannelID vIDtest; 
        double dRL = -1; 
        double dS = -1; 

        if (sensor->isR())
        {
            const DeVeloRType* rDet = sensor->rType();
            StatusCode distToM2 = rDet->distToM2Line(trInterceptGlob, vIDtest, dRL, dS); 
            distToM2.ignore();
        }

        Gaudi::XYZPoint trInterceptLocal(0,0,0) ;  
        trInterceptLocal  = sensor->globalToLocal(trInterceptGlob);

        double RLocal = sqrt((trInterceptLocal.x()*trInterceptLocal.x())+
			     (trInterceptLocal.y()*trInterceptLocal.y()));

        bool SenRegion = 0;
        LHCb::VeloChannelID chan; 
        double local,pitch;  
        StatusCode sc  = sensor->pointToChannel(trInterceptGlob,chan,local,pitch);
        if(sc)SenRegion=1;

        int Stripbad = 0;
        int StripBCbad = 0;   

        unsigned int CenStrip = chan.strip();
        unsigned int CenChan = chan.channelID();

        std::vector<unsigned int> stripIDs;
        bool Foundchan = 0;

        if(SenRegion==1){
            Foundchan = 1;
            int strip = chan.strip();
            m_CenStripOK = sensor->OKStrip(strip);
            stripIDs.push_back(strip);
            for(int i = 1; i!=m_numstripsCCE+1;i++){             
                if((0<=(strip+i)) && ((strip+i)<2048))stripIDs.push_back(strip+i);
                if((0<=(strip-i)) && ((strip-i)<2048))stripIDs.push_back(strip-i);
            }
        }        

        unsigned int tell1Id = 0;

        LHCb::VeloTELL1Data* SenADCData = m_recorded->object(tell1Id);
        int stripsADC = 0;        
        int stripsADC2 = 0;        
        double ADCXStrip = 0;        
        // _2 is for second cluster made when there is a gap
        int stripsADC2_2 = 0;        
        double ADCXStrip_2 = 0;        
        // _3 is for third cluster made when there is a second gap
        int stripsADC2_3 = 0;        
        double ADCXStrip_3 = 0;        

        int mainStripManual = -1;
        double interStripFracManual = -1;
        int noiseADC = -999;

        // Construct cluster
        LHCb::VeloCluster*  partialCluster = new LHCb::VeloCluster();
        LHCb::VeloCluster::ADCVector  adcVec;
        LHCb::VeloCluster::ADCVector  adcVec_2;
        LHCb::VeloCluster::ADCVector  adcVec_3;
        bool clusterMade = 0;
        bool gap = 0; 
        bool gap2 = 0; 
        bool secondCluster = 0; 
        if(SenADCData){
            int counter = 0;
            // Look at ADC of strip 20 away from extrapolation:
            int noiseStrip = chan.strip() - 20;
            if (noiseStrip>1) noiseADC = SenADCData->stripADC(noiseStrip);
            else{
                noiseStrip = chan.strip() + 20;
                if (noiseStrip<2048) noiseADC = SenADCData->stripADC(noiseStrip);
            } 

            sort(stripIDs.begin(), stripIDs.end());
            gap = 0; 
            gap2 = 0; 
            secondCluster = 0; 
            for(std::vector<unsigned int>::const_iterator stripIt=stripIDs.begin(); 
                    stripIt!=stripIDs.end();stripIt++){
                ++counter;
                if(sensor->OKStrip(*stripIt)==0)Stripbad++;
                if(checkBCList(*stripIt)==0)StripBCbad++;
                int currentADC = SenADCData->stripADC(*stripIt);
                unsigned int currentStrip = *stripIt;  
                stripsADC = stripsADC + currentADC;
                std::pair < int, unsigned int > stripPair (currentStrip, currentADC);

                if(currentADC>largestADC){ 
                    largestADC = currentADC;
                    mainStrip = currentStrip;
                }

                if ((clusterMade == 1) && (currentADC < m_ClusThres)) gap = 1;
                if ((secondCluster == 1) && (currentADC < m_ClusThres)) gap2 = 1;

                if ((currentADC >= m_ClusThres) && (gap!=1)){
                    adcVec.push_back(stripPair);
                    clusterMade = 1;

                    stripsADC2 = stripsADC2 + currentADC;
                    ADCXStrip = ADCXStrip + (currentADC * currentStrip);
                }
                // make the second cluster if there is a gap
                else if ((currentADC >= m_ClusThres) && (gap==1) && (gap2!=1)){
                    secondCluster = 1;
                    adcVec_2.push_back(stripPair);
                    stripsADC2_2 = stripsADC2_2 + currentADC;
                    ADCXStrip_2 = ADCXStrip_2 + (currentADC * currentStrip);
                }
                // make the third cluster if there is a second gap
                else if ( (currentADC >= m_ClusThres) && (gap2==1) ){
                    adcVec_3.push_back(stripPair);
                    stripsADC2_3 = stripsADC2_3 + currentADC;
                    ADCXStrip_3 = ADCXStrip_3 + (currentADC * currentStrip);
                }
            }// loop over strips

            // Pick the best cluster:
            if (gap==1){
                if(stripsADC2_2 > stripsADC2){
                    adcVec = adcVec_2;
                    stripsADC2 = stripsADC2_2;
                    ADCXStrip = ADCXStrip_2;
                }
            }
            if (gap2==1){
                if(stripsADC2_3 > stripsADC2){
                    adcVec = adcVec_3;
                    stripsADC2 = stripsADC2_3;
                    ADCXStrip = ADCXStrip_3;
                }
            }

            // Safety for position tool bug
            if (clusterMade==1){
                for(unsigned int i=0; i<(adcVec.size()-1); ++i){
                    if ((adcVec[i+1].first-adcVec[i].first)!=1){
                        clusterMade=0;
                        break;
                    }
                }
		if (stripsADC2 > 0)
		{
                	mainStripManual = (int)(ADCXStrip/stripsADC2);
                	interStripFracManual = ((ADCXStrip/stripsADC2) - mainStripManual);
            	}
	    }

            //Fill Cluster
            partialCluster->setStripValues(adcVec);

            m_sizeCluster = partialCluster->size(); 

            LHCb::SiPositionInfo<LHCb::VeloChannelID> toolInfo, toolInfoFull;
            if (clusterMade==1){
                toolInfo=m_clusterTool->position(partialCluster);
                m_interStripFr = toolInfo.fractionalPosition;

                // Make litecluster:
                LHCb::VeloChannelID mainChan = LHCb::VeloChannelID(m_testsensor,mainStrip); 
                LHCb::VeloChannelID mainChanManual = 
			      LHCb::VeloChannelID(m_testsensor,mainStripManual); 

                // Get the radius of the cluster if it's a R sensor:
                bool isRSen = mainChan.isRType();
                if (isRSen == 1){
                    const DeVeloRType* rDet = sensor->rType();
                    clusterRadiusL = rDet->rOfStrip(mainStrip);
                    clusterRadiusG = rDet->globalROfStrip(mainStrip);
                }

                bool largeADC = 0;
                if (largestADC >= 30){ largeADC = 1;}
                LHCb::VeloLiteCluster currentLiteCluster = 
				LHCb::VeloLiteCluster(  mainChanManual, 
				interStripFracManual, m_sizeCluster, largeADC);
                currentCluster = new LHCb::VeloCluster( currentLiteCluster, adcVec);
                toolInfoFull=m_clusterTool->position(currentCluster);
                clusterADC = currentCluster->totalCharge();

                std::vector<LHCb::VeloChannelID> chanCont=currentCluster->channels();
                for(unsigned int i=0; i<(currentCluster->size()); i++){
                }

                LHCb::VeloChannelID clustChan = toolInfoFull.strip; 

                // Resolution code:
                StatusCode sc4 = sensor->residual(trInterceptGlob ,  track.slopes() ,
                        clustChan,     interStripFracManual,
                        m_unbiasedResid, m_chi2_unbiasedres );
            }
        }

        unsigned int chan1ID,chan2ID;
        std::vector<unsigned int> chanIDs;

        if(SenRegion==1){
            chanIDs.push_back(CenChan);
            for(unsigned int i = 1; i!=m_numstripsCFE+1;i++){
                unsigned int Cu = CenStrip+i; unsigned int Cd = CenStrip-i;           
                if(Cu<2048){
                    chan1ID = LHCb::VeloChannelID(m_testsensor,Cu); 
                    chanIDs.push_back(chan1ID);
                }
                if(Cd<2048){
                    chan2ID = LHCb::VeloChannelID(m_testsensor,Cd);
                    chanIDs.push_back(chan2ID);
                }           
            }
        }

        unsigned int SearchArea = m_numstripsCFE;
        if(chanIDs.size()!=0)Foundchan = 1;  

        // Tool for cluster position tool:
        LHCb::SiPositionInfo<LHCb::VeloChannelID> toolInfo;

        LHCb::VeloCluster *cluster;
        std::vector<LHCb::VeloCluster*> clusters;
        // Get Projected angles:
        double slx = track.firstState().tx();
        double sly = track.firstState().ty();
        Gaudi::XYZVector global3dDir=Gaudi::XYZVector(slx, sly, 1.);
        Direction localDirection; 
        // for R sensors:
        if (sensor->isR()){
            const DeVeloRType* rDet = sensor->rType();
            localDirection=localTrackDirection(global3dDir, rDet);
            m_projAngle=projAngleR(localDirection, trInterceptLocal);
        }   
        // for phi sensors:
        if ((sensor->isPhi()) && (clusterMade==1)){
            const DeVeloPhiType* phiDet = sensor->phiType();
            localDirection=localTrackDirection(global3dDir, phiDet);
            unsigned int centreStrip=currentCluster->channelID().strip();
            m_projAngle=projAnglePhi(localDirection, phiDet, centreStrip);
        }  

        ///// find clusters which contain channels of interest ////////

        for(std::vector<unsigned int>::const_iterator chanIt=chanIDs.begin(); 
                chanIt!=chanIDs.end();chanIt++){
            cluster = (LHCb::VeloCluster*)m_rawClusters->containedObject(*chanIt);
            if(cluster){
                clusters.push_back(cluster);
                // If the cluster is the closest one to the central strip:
                auto distToCentral = CenChan > *chanIt ? CenChan - *chanIt : *chanIt - CenChan;
                if ( distToCentral <= SearchArea ){
                    clusterMade = 1;
                    SearchArea = distToCentral;

            }// if this is the closest cluster. 
        } // if cluster found.
    } // loop over channel ids


    //////// Check a number of channels around central channel ////////////////

    bool Foundclu = 0;
    if(clusters.size()!=0)Foundclu = 1;

    /////////////// Check distance to strip limit    //////////

    double IntercptToStripLim = -1;
    if(SenRegion==1){
        std::pair< Gaudi::XYZPoint, Gaudi::XYZPoint> P = 
					sensor->localStripLimits(CenStrip);
        IntercptToStripLim = sqrt(((trInterceptLocal.x()-P.first.x())*
		(trInterceptLocal.x()-P.first.x()))+
		((trInterceptLocal.y()-P.first.y())*
		(trInterceptLocal.y()-P.first.y())));    
        double diff2 = sqrt(((trInterceptLocal.x()-P.second.x())*
		(trInterceptLocal.x()-P.second.x()))+
		((trInterceptLocal.y()-P.second.y())*
		(trInterceptLocal.y()-P.second.y())));
        if(diff2<IntercptToStripLim)IntercptToStripLim = diff2;
    }

    ///////////// Check distance to nearest Track intercept ////////////////

    double IntercptToNearestTr = -1;

    for (LHCb::Tracks::const_iterator iterTr = m_tracks->begin(); 
		iterTr != m_tracks->end(); ++iterTr){
        if(((*iterTr)->checkType(LHCb::Track::Velo) == true) && ((*iterTr)!= &track)){

            const Gaudi::XYZPoint TrIn = extrapolateToZ(**iterTr, sensor->z());
            Gaudi::XYZPoint TrInL(0,0,0) ;      
            TrInL  = sensor->globalToLocal(TrIn);
            double IntercptToTr = sqrt(((trInterceptLocal.x()-TrInL.x())*
		(trInterceptLocal.x()-TrInL.x()))+
		((trInterceptLocal.y()-TrInL.y())*
		(trInterceptLocal.y()-TrInL.y())));
            if((IntercptToTr<IntercptToNearestTr)|(IntercptToNearestTr==-1))
		IntercptToNearestTr=IntercptToTr;

        }
    }
    ///////////////////////////////////////

    if((stripIDs.size()==0)||(!SenADCData))stripsADC = -1000;


    ///////// Search +/-512 strips away incase track near boundary region on R sensor ////////

    bool FoundcluR = 0;

    if(Foundchan==1 && Foundclu==0 && sensor->isR() && 
	IntercptToStripLim<2 && IntercptToStripLim>0){
        std::vector<unsigned int> chanIDRs;
        unsigned int C1 = CenStrip+512;
        unsigned int C2 = CenStrip-512;
        if(C1<2048)
	{
	    unsigned int chan1IDR = LHCb::VeloChannelID(m_testsensor,C1); 
	    chanIDRs.push_back(chan1IDR);
	}
        if(C2<2048)
	{
	    unsigned int chan2IDR = LHCb::VeloChannelID(m_testsensor,C2); 
	    chanIDRs.push_back(chan2IDR);
	}
        for(unsigned int j = 1; j!=m_numstripsCFE+1;j++){

            unsigned int C3 = C1+j;
	    unsigned int C4 = C1-j;
            unsigned int C5 = C2+j;
            unsigned int C6 = C2-j;

            if(C3<2048)
	    {
		unsigned int chan3IDR = LHCb::VeloChannelID(m_testsensor,C3);
		chanIDRs.push_back(chan3IDR);
	    }
            if(C4<2048)
	    {
		unsigned int chan4IDR = LHCb::VeloChannelID(m_testsensor,C4);
		chanIDRs.push_back(chan4IDR);
	    }
            if(C5<2048)
	    {
		unsigned int chan5IDR = LHCb::VeloChannelID(m_testsensor,C5);
		chanIDRs.push_back(chan5IDR);
	    }
            if(C6<2048)
	    {
		unsigned int chan6IDR = LHCb::VeloChannelID(m_testsensor,C6);
		chanIDRs.push_back(chan6IDR);
	    }
        }

        LHCb::VeloCluster *clusterR;
        std::vector<LHCb::VeloCluster*> clustersR;

        for(std::vector<unsigned int>::const_iterator chanIT=chanIDRs.begin(); 
                chanIT!=chanIDRs.end();chanIT++){
            clusterR = (LHCb::VeloCluster*)m_rawClusters->containedObject(*chanIT);
            if(clusterR){
                clustersR.push_back(clusterR);
            }
        }

        if(clustersR.size()!=0)FoundcluR = 1;
    }

    /// Search strips in adjacent region in case /// 
    /// track near boundary region on Phi sensor ///

    bool FoundcluPhi = 0;
    double interceptR = sqrt((trInterceptGlob.x()*trInterceptGlob.x())+
			(trInterceptGlob.y()*trInterceptGlob.y()));

    if(Foundchan==1 && Foundclu==0 && sensor->isPhi() && IntercptToStripLim<2 && 
		IntercptToStripLim>0 && interceptR>14.0 && interceptR<20.0 ){
        std::vector<unsigned int> chanIDPhis;
        unsigned int CP1;

        if(CenStrip<683){
            CP1 = 683+2*CenStrip;
            if(CP1<2048)
	    {
		unsigned int chan1IDPhi = LHCb::VeloChannelID(m_testsensor,CP1);
		chanIDPhis.push_back(chan1IDPhi);
	    }
        }
        if(CenStrip>682){
            CP1 = int(round(float(CenStrip-683)/2));
            if(CP1<2048)
	    {
		unsigned int chan1IDPhi = LHCb::VeloChannelID(m_testsensor,CP1);
		chanIDPhis.push_back(chan1IDPhi);
	    }
        }

        for(unsigned int jp = 1; jp!=m_numstripsCFE+1;jp++){

            unsigned int CP2 = CP1+jp;unsigned int CP3 = CP1-jp; 
            if(CP2<2048)
	    {
		unsigned int chan2IDPhi = LHCb::VeloChannelID(m_testsensor,CP2);
		chanIDPhis.push_back(chan2IDPhi);
	    }
            if(CP3<2048)
	    {
		unsigned int chan3IDPhi = LHCb::VeloChannelID(m_testsensor,CP3);
		chanIDPhis.push_back(chan3IDPhi);
	    }
        }

        LHCb::VeloCluster *clusterPhi;
        std::vector<LHCb::VeloCluster*> clustersPhi;         

        for(std::vector<unsigned int>::const_iterator chanI=chanIDPhis.begin(); 
                chanI!=chanIDPhis.end();chanI++){
            clusterPhi = (LHCb::VeloCluster*)m_rawClusters->containedObject(*chanI);
            if(clusterPhi){
                clustersPhi.push_back(clusterPhi);
            }
        }

        if(clustersPhi.size()!=0)FoundcluPhi = 1;
    }

    tuple->column("evt",m_evt);
    tuple->column("interceptToNearestTr",IntercptToNearestTr);
    tuple->column("StripBCbad",StripBCbad);
    tuple->column("Stripbad",Stripbad);
    tuple->column("CenStripOK",m_CenStripOK);
    tuple->column("slope0",track.slopes());
    tuple->column("X0",track.position().X());
    tuple->column("Y0",track.position().Y());
    tuple->column("trphi0",track.phi());
    tuple->column("interceptXErr",trackErrorOnPos(0,0));
    tuple->column("interceptYErr",trackErrorOnPos(1,1));
    tuple->column("interceptXLocal",trInterceptLocal.x());
    tuple->column("interceptYLocal",trInterceptLocal.y());
    tuple->column("sensornum",sensor->sensorNumber());
    tuple->column("Foundchan",Foundchan);
    tuple->column("stripsADC",stripsADC);
    tuple->column("UnbiasedResolution",m_unbiasedResid);
    tuple->column("UnbiasedResolutionChi2",m_chi2_unbiasedres);
    tuple->column("sizeCluster",m_sizeCluster);
    tuple->column("projAngle",m_projAngle);
    tuple->column("clusterMade",clusterMade);
    tuple->column("ClusThreshold",m_ClusThres);
    tuple->column("Voltage",m_Voltage);
    tuple->column("cceStep",m_CCEstep);
    tuple->column("Type",track.type());
    tuple->column("clusterADC",clusterADC);
    tuple->column("interStripFr",m_interStripFr);
    tuple->column("trackPt", Pt);
    tuple->column("trackP", P);
    tuple->column("noiseADC", noiseADC);
    tuple->column("clusterRadiusL", clusterRadiusL);
    tuple->column("clusterRadiusG", clusterRadiusG);
    tuple->column("isSet1", isSet1);
    tuple->column("isSet2", isSet2);
    tuple->column("isSet3", isSet3);
    tuple->column("isSet4", isSet4);
    tuple->column("isSet5", isSet5);
    tuple->column("SENSHITS",SENSHITS);
    tuple->column("SENSHITS2",SENSHITS2);
    tuple->column("SENSHITS3",SENSHITS3);
    tuple->column("SENSHITS4",SENSHITS4);
    tuple->column("SENSHITS5",SENSHITS5);
    tuple->column("interceptToStripLim",IntercptToStripLim);
    tuple->column("trChi2",track.chi2());
    tuple->column("trNDoF",track.nDoF());
    tuple->column("interceptR",interceptR);
    tuple->column("interceptRLocal",RLocal);
    tuple->column("Foundclu",Foundclu);
    tuple->column("FoundcluR",FoundcluR);
    tuple->column("FoundcluPhi",FoundcluPhi);
    tuple->column("Censtripid",CenStrip);
    tuple->column("distanceToRL", dRL);
    tuple->column("distanceToStrip", dS);

    tuple->write();
  }
}


//===================================================
///////////////// Check BC List ///////////////////
//===================================================

bool Velo::VeloCCEScanMonitor::checkBCList(unsigned int CheckStrip){

    m_cceTool->findBadStrips(m_testsensor,m_badStripList);
    bool stripOK = 1;

    for(unsigned int j = 0; j < m_badStripList.size(); j++){
      if((unsigned)m_badStripList[j] == CheckStrip){
        stripOK = 0;
      }
    }
    
    return stripOK;

}

double Velo::VeloCCEScanMonitor::projAngleR(const Direction& locDirection,
        const Gaudi::XYZPoint& aLocPoint)
{
    if( msgLevel(MSG::DEBUG) ) debug()<< " --> projAngleR() " <<endmsg;
    //  
    Gaudi::XYZVector parallel2Track;
    //Direction locDir=localTrackDirection(m_trackDir, sensor);
    //-- track angle
    double alphaOfTrack=angleOfInsertion(locDirection, parallel2Track);
    //-- vector normal to the strip - sensor type specific
    Gaudi::XYZVector perp2RStrip(aLocPoint.x(), aLocPoint.y(), 0.);
    double cosTrackOnNormal=parallel2Track.Dot(perp2RStrip.Unit());
    //-- projection of track on normal to local strip
    double trackOnNormal=fabs(cosTrackOnNormal);
    //-- projection of track on Z axis
    double trackOnZ=cos(alphaOfTrack);
    double projectedAngle=atan(trackOnNormal/trackOnZ);
    //  
    return ( projectedAngle );
}

double Velo::VeloCCEScanMonitor::projAnglePhi(const Direction& locDirection,
        const DeVeloPhiType* phiSensor,
        unsigned int centreStrip)
{
    if( msgLevel(MSG::DEBUG) ) debug()<< " --> projAnglePhi() " <<endmsg;
    //  
    Gaudi::XYZVector parallel2Track;
    std::pair<Gaudi::XYZPoint, Gaudi::XYZPoint> strip;
    strip=phiSensor->localStripLimits(centreStrip);
    Gaudi::XYZVector perp2PhiStrip((strip.first.y()-strip.second.y()),
            (strip.second.x()-strip.first.x()), 0.);
    double alphaOfTrack=angleOfInsertion(locDirection, parallel2Track);
    double cosTrackOnNormal=parallel2Track.Dot(perp2PhiStrip.Unit());
    double trackOnNormal=fabs(cosTrackOnNormal);
    double trackOnZ=cos(alphaOfTrack);
    double projectedAngle=atan(trackOnNormal/trackOnZ);
    //  
    return ( projectedAngle );
}

double Velo::VeloCCEScanMonitor::angleOfInsertion(const Direction& localSlopes,
        Gaudi::XYZVector& parallel2Track) const
{
    if( msgLevel(MSG::DEBUG) ) debug()<< " --> angleOfInsertion() " <<endmsg;
    //  
    const Gaudi::XYZVector ZVersor(0., 0., 1.);
    Gaudi::XYZVector perpPi1(1., 0., -(localSlopes.first));
    Gaudi::XYZVector perpPi2(0., 1., -(localSlopes.second));
    // vector parallel to the track
    Gaudi::XYZVector parallel=perpPi1.Cross(perpPi2);
    double modParallel=sqrt(parallel.Mag2());
    // and normalized parallel to track
    Gaudi::XYZVector normParallel(parallel.x()/modParallel,
            parallel.y()/modParallel,
            parallel.z()/modParallel
            );  
    parallel2Track=normParallel;
    double cosOfInsertion=parallel2Track.Dot(ZVersor);
    double alphaOfInsertion=acos(cosOfInsertion);
    //  
    return ( alphaOfInsertion );

}

Direction Velo::VeloCCEScanMonitor::localTrackDirection(const Gaudi::XYZVector& gloTrackDir,
        const DeVeloSensor* sensor) const
{
    if( msgLevel(MSG::DEBUG) ) debug()<< " --> localTrackDirection() " <<endmsg;
    //-- translate global slopes into local
    using namespace std;
    Direction locTrackDir;
    if(sensor->isLeft()&&(!sensor->isDownstream())){
        locTrackDir=make_pair(gloTrackDir.x(), gloTrackDir.y());
    }else if(sensor->isLeft()&&sensor->isDownstream()){
        locTrackDir=make_pair((-1)*gloTrackDir.x(), gloTrackDir.y());
    }else if(sensor->isRight()&&sensor->isDownstream()){
        locTrackDir=make_pair(gloTrackDir.x(), (-1)*gloTrackDir.y());
    }else if(sensor->isRight()&&(!sensor->isDownstream())){
        locTrackDir=make_pair((-1)*gloTrackDir.x(), (-1)*gloTrackDir.y());
    }
    //  
    return ( locTrackDir );
}
