from Gaudi.Configuration import GaudiSequencer
from TrackSys.Configuration import *
from GaudiKernel.SystemOfUnits import mm

def PrUpgradeChecking(defTracks = {}):
   ### match hits and tracks
   log.warning("Run upgrade checkers.")
   from Configurables import UnpackMCParticle, UnpackMCVertex, PrLHCbID2MCParticle
   # Check if VP is part of the list of detectors.
   from Configurables import LHCbApp, VPClusterLinker
   withVP = False
   if hasattr(LHCbApp(), "Detectors"):
      if LHCbApp().isPropertySet("Detectors"):
         if 'VP' in LHCbApp().upgradeDetectors(): withVP = True
   trackTypes = TrackSys().getProp("TrackTypes")
   if "Truth" in trackTypes :
      truthSeq = GaudiSequencer("RecoTruthSeq")
      truthSeq.Members = [UnpackMCParticle(), UnpackMCVertex()]
      if withVP: truthSeq.Members += [VPClusterLinker()]
      truthSeq.Members += [PrLHCbID2MCParticle()]
   else:
      if withVP:
        GaudiSequencer("MCLinksTrSeq").Members = [VPClusterLinker(), PrLHCbID2MCParticle()]
      else:
        GaudiSequencer("MCLinksTrSeq").Members = [PrLHCbID2MCParticle()]
   
   from Configurables import PrTrackAssociator
   GaudiSequencer("MCLinksTrSeq").Members += [ PrTrackAssociator() ]

   from Configurables import PrChecker
   #PrCheckerFast: Tracks which are outdated after BestStage
   GaudiSequencer("CheckPatSeq").Members += [ PrChecker("PrCheckerFast",ForwardTracks=defTracks["ForwardFast"]["Location"], 
                                                        BestTracks=defTracks["ForwardFastFitted"]["Location"],
   VeloTracks = "", MatchTracks ="", SeedTracks="",DownTracks="",UpTracks="",WriteForwardHistos = 2, TriggerNumbers = True, Eta25Cut = True) ]
   #PrChecker: Tracks available after Best stage
   GaudiSequencer("CheckPatSeq").Members += [ PrChecker("PrChecker", ForwardTracks=defTracks["ForwardBest"]["Location"], UpTracks=defTracks["Upstream"]["Location"] , WriteForwardHistos = 2, Eta25Cut = True, GhostProbCut = 0.9)]

